<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/21/2015
 * Time: 12:54 PM
 */

namespace Rasen\NineGagBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PostViewEvent
 * @package Rasen\NineGagBundle\Event
 */
class PostViewEvent extends Event
{
    /**
     * @var array $posts
     */
    protected $posts;

    /**
     * @var Request $request
     */
    protected $request;

    /**
     * @param array $posts
     * @param Request $request
     */
    public function __construct(
        $posts,
        Request $request
    )
    {
        $this->posts = $posts;
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}