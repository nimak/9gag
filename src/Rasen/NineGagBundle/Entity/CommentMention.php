<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CommentMention
 *
 * Stores user mentions made in comments (@ mentions)
 *
 * @ORM\Table(name="comments_mentions", uniqueConstraints={@ORM\UniqueConstraint(name="comment_id_user_id_UNIQUE", columns={"comment_id", "user_id"})}, indexes={@ORM\Index(name="comments_mentions_comment_id_idx", columns={"comment_id"}), @ORM\Index(name="comments_mentions_user_id_idx", columns={"user_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"comment", "user"},
 *     errorPath="user",
 *     message="comment.mention.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class CommentMention
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Rasen\NineGagBundle\Entity\Comment
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Comment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comment_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $comment;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @Assert\Valid
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param \Rasen\NineGagBundle\Entity\Comment $comment
     * @return CommentMention
     */
    public function setComment(\Rasen\NineGagBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \Rasen\NineGagBundle\Entity\Comment 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return CommentMention
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
