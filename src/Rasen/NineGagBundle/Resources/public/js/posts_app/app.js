'use strict';

/* App Module */

var postsListApp = angular.module('postsListApp', [
    'postsAppServices',
    'postsAppControllers',
    'postsManagerApp',
    'userConfigApp'
]);

postsListApp
    .config(['$interpolateProvider',
        function($interpolateProvider) {
            $interpolateProvider.startSymbol('[[').endSymbol(']]');
        }]);

angular.element().ready(function() {
    angular.bootstrap('.main-wrapper', ['postsListApp']);
});

