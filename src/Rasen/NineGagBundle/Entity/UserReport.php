<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UserReport
 *
 * Stores inappropriate reports about users.
 *
 * @ORM\Table(name="users_reports", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_reporter_id_UNIQUE", columns={"user_id", "reporter_id"})}, indexes={@ORM\Index(name="users_reports_user_id_idx", columns={"user_id"}), @ORM\Index(name="users_reports_reporter_id_idx", columns={"reporter_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"user", "reporter"},
 *     errorPath="user",
 *     message="user.report.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class UserReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="reported_time", type="datetime", nullable=false)
     */
    private $reportedTime;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reporter_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $reporter;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportedTime
     *
     * @param \DateTime $reportedTime
     * @return UserReport
     */
    public function setReportedTime($reportedTime)
    {
        $this->reportedTime = $reportedTime;

        return $this;
    }

    /**
     * Get reportedTime
     *
     * @return \DateTime 
     */
    public function getReportedTime()
    {
        return $this->reportedTime;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return UserReport
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set reporter
     *
     * @param \Rasen\NineGagBundle\Entity\User $reporter
     * @return UserReport
     */
    public function setReporter(\Rasen\NineGagBundle\Entity\User $reporter = null)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }
}
