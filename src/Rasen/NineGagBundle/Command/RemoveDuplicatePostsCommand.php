<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 6:15 PM
 */

namespace Rasen\NineGagBundle\Command;

use Doctrine\ORM\EntityManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Lib\PostImageProcessor;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class GenerateHashIdsCommand
 * @package Rasen\NineGagBundle\Command
 */
class RemoveDuplicatePostsCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:post:remove-duplicates')
			->setDescription('Removes duplicate posts.')
            ->addOption(
                'destroy',
                null,
                InputOption::VALUE_NONE,
                'Skips the recycle-bin and completely removes images from storage.'
            )->setHelp(
                <<<EOT
                    The <info>%command.name%</info> command removes duplicate posts from database and storage

<info>php %command.full_name% [--destroy]</info>

EOT
            )
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
        if ($input->hasOption('destroy') && $input->getOption('destroy')) {
            $this->removeDuplicates(true, $output);
        } else {
            $this->removeDuplicates(false, $output);
        }
	}

    private function getPostImagePath(PostImage $post)
    {
        $uploadPath = $this->getContainer()->getParameter('kernel.root_dir').PostImageProcessor::POST_UPLOAD_DIR;
        return realpath(PostImageProcessor::join_paths($uploadPath, $post->getImageUrl()));
    }

    private function removeDuplicates($destroy, $output)
    {
        /**
         * $container ContainerInterface
         */
        $container = $this->getContainer();


        /**
         * @var EntityManager $em
         */
        $em = $container->get('doctrine')->getManager();

        $selectDQL = "SELECT p FROM RasenNineGagBundle:PostImage p";
        $selectQ = $em->createQuery($selectDQL);
        $results = $selectQ->iterate();
        $i = 1;
        $batchSize = 20;
        $toDelete = array();
        $hashes = array();
        $didDeleteSome = false;

        $output->writeln('Searching for duplicates...');
        foreach ($results as $row) {
            $post = $row[0];
            if ($post instanceof PostImage)
            {
                if (empty($post->getImageUrl())) {
                    $toDelete[] = $post->getId();
                    continue;
                }

                $postImagePath = $this->getPostImagePath($post);

                $sha1 = sha1_file($postImagePath);
                if (in_array($sha1, $hashes)) {
                    $toDelete[] = $post->getId();
                    $this->removeImages($post);
                }
                $hashes[$post->getId()] = $sha1;

                if (($i % $batchSize) === 0) {
                    if (empty($toDelete)) {
                        continue;
                    }
                    $didDeleteSome = true;
                    $output->writeln('Removing batch #'.$i/$batchSize.'...');
                    $this->remove($toDelete, $destroy, $output);
                    $toDelete = array();
                    $em->flush(); // Executes all updates.
                    $em->clear(); // Detaches all objects from Doctrine!
                }
            }
            ++$i;
        }

        if (empty($toDelete)) {
            if (!$didDeleteSome) $output->writeln('No duplicates found :)');
        } else {
            $didDeleteSome = true;
            $output->writeln('Removing final batch...');
            $this->remove($toDelete, $destroy, $output);
            $em->flush(); // Executes all updates.
        }

        if ($didDeleteSome) {
            // Update counters if anything got deleted (workaround for listeners not getting called for some reason)
            $command = $this->getApplication()->find('rasen:counters:update');
            $arguments = array(
                'command' => 'rasen:counters:update'
            );
            $input = new ArrayInput($arguments);
            $returnCode = $command->run($input, $output);
        }
        ;
    }

    private function removeImages(PostImage $post)
    {
        $this->getContainer()->get('rasen_ninegag.post_image_processor')->purge($post);

        $path = $this->getContainer()->get('vich_uploader.templating.helper.uploader_helper')->asset($post, 'posts');
        $this->getContainer()->get('liip_imagine.cache.manager')->remove($path);

        $postImageFilePath = $this->getPostImagePath($post);
        if (file_exists($postImageFilePath)) {
            unlink($postImageFilePath);
        }
    }

    private function remove($postIds, $destroy, $output)
    {

        $output->writeln('Removing => ' . implode(',', $postIds));

        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($destroy)
        {
            $output->writeln('Destroying... ');
            $sql = "DELETE FROM posts WHERE id IN (?1)";
            $em->getConnection()->executeUpdate($sql, $postIds);
        }
        else
        {
            $output->writeln('Removing... ');
            $dql = "DELETE FROM RasenNineGagBundle:Post p WHERE p.id IN (?1)";
            $q = $em->createQuery($dql)
            ->setParameter(1, $postIds);
            $q->execute();
        }
    }
} 