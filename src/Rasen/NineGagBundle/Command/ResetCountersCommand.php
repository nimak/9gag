<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 6:15 PM
 */

namespace Rasen\NineGagBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ResetCountersCommand
 * @package Rasen\NineGagBundle\Command
 */
class ResetCountersCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:counters:reset')
			->setDescription('Reset all counters in all entities')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/**
		 * $container ContainerInterface
		 */
		$container = $this->getContainer();

		/**
		 * $em ObjectManager
		 */
		$em = $container->get('doctrine')->getManager();

		$output->writeln('Resetting all users\' counters...');
		$userRepo = $em->getRepository('RasenNineGagBundle:User');
		$q1 = $userRepo->createQueryBuilder('u')
			->update('RasenNineGagBundle:User', 'u')
			->set('u.followersNo', 0)
			->set('u.followingNo', 0)
			->set('u.postsNo', 0)
			->set('u.approvedPostsNo', 0)
			->set('u.commentsNo', 0)
			->set('u.approvedCommentsNo', 0)
			->set('u.pointsNo', 0)
			->set('u.reportsNo', 0)
			->set('u.upvotesNo', 0)
			->getQuery();
			;
		$p1 = $q1->execute();

		$output->writeln('Resetting all posts\' counters...');
		$postRepo = $em->getRepository('RasenNineGagBundle:Post');
		$q2 = $postRepo->createQueryBuilder('p')
			->update('RasenNineGagBundle:Post', 'p')
			->set('p.commentsNo', 0)
			->set('p.approvedCommentsNo', 0)
			->set('p.viewsNo', 0)
			->set('p.upvotesNo', 0)
			->set('p.downvotesNo', 0)
			->set('p.reportsNo', 0)
			->getQuery();
			;
		$p2 = $q2->execute();

		$output->writeln('Resetting all comments\' counters...');
		$commentRepo = $em->getRepository('RasenNineGagBundle:Comment');
		$q3 = $commentRepo->createQueryBuilder('c')
			->update('RasenNineGagBundle:Comment', 'c')
			->set('c.upvotesNo', 0)
			->set('c.downvotesNo', 0)
			->set('c.reportsNo', 0)
			->set('c.repliesNo', 0)
			->getQuery();
			;
		$p3 = $q3->execute();

		$output->writeln('Done! ;)');
	}
} 