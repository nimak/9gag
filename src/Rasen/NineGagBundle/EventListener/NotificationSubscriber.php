<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/29/2014
 * Time: 1:20 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\CommentMention;
use Rasen\NineGagBundle\Entity\UserBadge;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\Notification;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Lib\NotificationUtility;
use Rasen\NineGagBundle\Model\VotableReportable;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Class NotificationSubscriber
 *
 * @DI\Service("event_listener.notification_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class NotificationSubscriber implements EventSubscriber
{
	private $container;

	/**
	 * @DI\InjectParams({
	 *     "container" = @DI\Inject("service_container")
	 * })
	 *
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'postPersist',
			'onFlush'
		);
	}

	/**
	 * @param OnFlushEventArgs $args
	 */
	public function onFlush(OnFlushEventArgs $args) {
		$em  = $args->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach ( $uow->getScheduledEntityUpdates() as $entity ) {
			$changeSet = $uow->getEntityChangeSet( $entity );
			if ( $entity instanceof Comment ) {
				if ( array_key_exists( 'approved', $changeSet ) ) {
					$valueBefore = $changeSet['approved'][0];
					$valueAfter  = $changeSet['approved'][1];
					if (!$valueBefore && $valueAfter) {
						$mentions = $entity->getMentions();
						foreach ($mentions as $mention) {
							$mentionNotifs = $this->addNotification($mention);
                            if (!empty($mentionNotifs)) {
                                /**
                                 * @var Notification $notification
                                 */
                                foreach ($mentionNotifs as $mentionNotif) {
                                    if ($mentionNotif && $mentionNotif->getUser() !== $mentionNotif->getActor()) {
                                        $em->persist($mentionNotif);
                                        $this->container->get('rasen_ninegag.notification_utility')->publishNotification($mentionNotif);
                                    }
                                }
                                $uow->computeChangeSets();
                            }
						}
						$notifications = $this->addNotification($entity);
                        if (!empty($notifications)) {
                            /**
                             * @var Notification $notification
                             */
                            foreach ($notifications as $notification) {
                                if ($notification && $notification->getUser() !== $notification->getActor()) {
                                    $em->persist($notification);
                                    $this->container->get('rasen_ninegag.notification_utility')->publishNotification($notification);
                                }
                            }
                            $uow->computeChangeSets();
                        }
					}
				}
			} elseif ( $entity instanceof PostVote || $entity instanceof CommentVote ) {
				if ( array_key_exists( 'vote', $changeSet ) ) {
					$valueBefore = $changeSet['vote'][0];
					$valueAfter  = $changeSet['vote'][1];
					if ($valueBefore != $valueAfter) {
						$notifications = $this->addNotification($entity);
                        if (!empty($notifications)) {
                            /**
                             * @var Notification $notification
                             */
                            foreach ($notifications as $notification) {
                                if ($notification && $notification->getUser() !== $notification->getActor()) {
                                    $em->persist($notification);
                                    $this->container->get('rasen_ninegag.notification_utility')->publishNotification($notification);
                                }
                            }
                            $uow->computeChangeSets();
                        }
					}
				}
			}
		}
	}

	/**
	 * @param LifecycleEventArgs $args
	 */
	public function postPersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();

		$notifications = $this->addNotification($entity);
        if (!empty($notifications)) {
            $em = $args->getObjectManager();
            /**
             * @var Notification $notification
             */
            foreach ($notifications as $notification) {
                if ($notification && $notification->getUser() !== $notification->getActor()) {
                    $em->persist($notification);
                    $this->container->get('rasen_ninegag.notification_utility')->publishNotification($notification);
                }
            }
            $em->flush();
        }

	}

	/**
	 * @param $entity
	 *
	 * @return bool|Notification
	 */
	public function addNotification($entity)
	{
        $notifications = array();
		if ($entity instanceof PostVote)
		{
			if (!$entity->getVote()) {
				return false; //DO NOT send notifications for down votes. (down votes are better remained anonymous)
			}
			$verb = Notification::VERB_UPVOTED;
            $notifications[] = $entity->getPost()->getCreatedBy()->newNotification()
			                       ->setActor($entity->getVotedBy())
			                       ->setObjectType(Notification::OBJECT_TYPE_POST)
			                       ->setObjectId($entity->getPost()->getId())
			                       ->setVerb($verb);
			return $notifications;
		} elseif ($entity instanceof Comment)
		{
			if (!$entity->getApproved()) return false;

            $sendPostCommentNotif = true;
            if ($parent = $entity->getParent()) {
                if ($entity->getPost()->getCreatedBy() === $parent->getCreatedBy()) {
                    $sendPostCommentNotif = false;
                }
                $commentReplyNotif = $parent->getCreatedBy()->newNotification()
                    ->setActor($entity->getCreatedBy())
                    ->setObjectType(Notification::OBJECT_TYPE_COMMENT)
                    ->setObjectId($entity->getParent()->getId())
                    ->setVerb(Notification::VERB_REPLIED);
                $replyId = $this->container->get('hashids')->encode($entity->getId());
                $commentReplyNotif->addMeta('reply_id', $replyId);
                $notifications[] = $commentReplyNotif;
            }

            if($sendPostCommentNotif) {
                $postCommentNotif = $entity->getPost()->getCreatedBy()->newNotification()
                    ->setActor($entity->getCreatedBy())
                    ->setObjectType(Notification::OBJECT_TYPE_POST)
                    ->setObjectId($entity->getPost()->getId())
                    ->setVerb(Notification::VERB_COMMENTED);
                $hashId = $this->container->get('hashids')->encode($entity->getId());
                $postCommentNotif->addMeta('comment_id', $hashId);
                $notifications[] = $postCommentNotif;
            }

			return $notifications;
		} elseif ($entity instanceof CommentVote)
		{
			if (!$entity->getVote()) {
				return false; //DO NOT send notifications for down votes. (down votes are better remained anonymous)
			}
			$verb = Notification::VERB_UPVOTED;
            $notifications[] = $entity->getComment()->getCreatedBy()->newNotification()
			                       ->setActor($entity->getVotedBy())
			                       ->setObjectType(Notification::OBJECT_TYPE_COMMENT)
			                       ->setObjectId($entity->getComment()->getId())
			                       ->setVerb($verb);
			//$notification->addMeta('comment_id', $entity->getId());
			return $notifications;
		} elseif ($entity instanceof CommentMention)
		{
			if (!$entity->getComment()->getApproved()) return false;

            $userMentionNotif = $entity->getUser()->newNotification()
			                       ->setActor($entity->getComment()->getCreatedBy())
			                       ->setObjectType(Notification::OBJECT_TYPE_USER)
			                       ->setObjectId($entity->getUser()->getId())
			                       ->setVerb(Notification::VERB_MENTIONED);
            $userMentionNotif->addMeta('comment_id', $entity->getComment()->getId());
            $notifications[] = $userMentionNotif;
			return $notifications;
		} elseif ($entity instanceof UserFollower)
		{
            $notifications[] = $entity->getUser()->newNotification()
			                       ->setActor($entity->getFollower())
			                       ->setObjectType(Notification::OBJECT_TYPE_USER)
			                       ->setObjectId($entity->getFollower()->getId())
			                       ->setVerb(Notification::VERB_FOLLOWED);
			return $notifications;
		} elseif ($entity instanceof UserPoint)
		{
            $notifications[] = $entity->getUser()->newNotification()
			                       ->setActor(null)
			                       ->setObjectType(Notification::OBJECT_TYPE_POINT)
			                       ->setObjectId($entity->getId())
			                       ->setVerb(Notification::VERB_RECEIVED_POINT);
			return $notifications;
		} elseif ($entity instanceof UserBadge)
		{
            $notifications[] = $entity->getUser()->newNotification()
			                       ->setActor(null)
			                       ->setObjectType(Notification::OBJECT_TYPE_BADGE)
			                       ->setObjectId($entity->getId())
			                       ->setVerb(Notification::VERB_RECEIVED_BADGE);
			return $notifications;
		}
		return false;
	}
} 