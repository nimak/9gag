<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/18/2014
 * Time: 2:50 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Hashids\Hashids;
use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PostImageNamer
 *
 * @DI\Service("rasen_ninegag.file_namer.post_image")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class PostImageNamer implements NamerInterface
{

	/**
	 * @var Hashids
	 */
	private $hashids;


	/**
	 * @DI\InjectParams({
	 *     "hashids" = @DI\Inject("hashids")
	 * })
	 *
	 * @param Hashids $hashids
	 */
	public function __construct(
		Hashids $hashids)
	{
		$this->hashids = $hashids;
	}

	/**
	 * { @inheritdoc }
	 */
	public function name($obj, PropertyMapping $mapping) {

		$extension = ($obj->getImageFile()->guessExtension() != null) ?  $obj->getImageFile()->guessExtension() : 'jpg' ;
		$ownerId= $obj->getCreatedBy()->getId();
		$ownerHashId = $this->hashids->encode($ownerId);
		$finalFileName = $ownerHashId . '/' . str_replace('.','_',uniqid('', true)) . '.' . $extension;
		return $name = $finalFileName ;
	}
}