9GAG
========================

### Prerequisites:
* [JRE] (required to run [Google Closure Compiler])
* [RabbitMQ Server]
* [Redis] >= v2.8.9 (needs HyperLogLog)
* [phpredis] (Windows? Download the dll from [Pecl Snaps])
* [NodeJS]
* [FFmpeg] (Windows? [Here][1]
* [ImageMagick]

[JRE]:https://docs.oracle.com/javase/7/docs/webnotes/install/
[Google Closure Compiler]:https://developers.google.com/closure/compiler/
[RabbitMQ Server]:http://www.rabbitmq.com/download.html
[Redis]:http://redis.io/download
[phpredis]:https://github.com/nicolasff/phpredis#installingconfiguring
[Pecl Snaps]:http://windows.php.net/downloads/pecl/snaps/redis/
[NodeJS]:http://nodejs.org/
[ffmpeg]:https://www.ffmpeg.org
[ImageMagick]:http://www.imagemagick.org/script/download.php
[1]:http://ffmpeg.zeranoe.com/builds/