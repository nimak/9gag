php:
  ng:
    fpm:
      config:
        ini:
          settings:
            PHP:
              cgi.fix_pathinfo: 0
              expose_php: 'Off'
              post_max_size: 8M
              upload_max_filesize: 8M
    ini:
      defaults:
        PHP:
          cgi.fix_pathinfo: 0
          expose_php: 'Off'
          post_max_size: 8M
          upload_max_filesize: 8M
        Date:
          date.timezone: Asia/Tehran
        opcache:
          opcache.enable: 1
          opcache.memory_consumption: 128
          opcache.max_accelerated_files: 11000
          opcache_revalidate_freq: 240
          opcache.fast_shutdown: 1
          opcache.interned_strings_buffer: 16