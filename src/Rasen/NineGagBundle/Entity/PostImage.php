<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use JMS\Serializer\Annotation as Serializer;

/**
 * PostImage
 *
 * @ORM\Table(name="posts_image", indexes={@ORM\Index(name="posts_image_post_id_idx", columns={"post_id"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\PostRepository")
 *
 * @Vich\Uploadable
 *
 * @Serializer\ExclusionPolicy("none")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class PostImage extends Post
{

    /**
     * @var string
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     *
     */
	protected $imageUrl;


	/**
	 * @var string
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @Assert\Length(
	 *      min = 1,
	 *      max = 100,
	 *      minMessage = "post_image.title.length_min",
	 *      maxMessage = "post_image.title.length_max"
	 * )
	 *
	 * @ORM\Column(name="post_title", type="string", length=100, nullable=true)
	 *
	 */
	protected $postTitle;

	/**
	 * @Vich\UploadableField(mapping="posts", fileNameProperty="imageUrl")
	 *
	 * This is not a mapped field of entity metadata, just a simple property.
	 *
	 * @Assert\File(
	 *     maxSize = "5M",
	 *     mimeTypes = {"image/jpeg","image/gif","image/png"},
	 *     mimeTypesMessage = "post_image.image_file.type"
	 * )
	 *
	 * @var File $imageFile
	 */
	protected $imageFile;

	/**
     * Whether the image is an animated gif
     *
     * @var boolean
     *
	 * @Serializer\Groups({"default", "own"})
	 *
     * @ORM\Column(name="is_animated", type="boolean", nullable=false)
     */
	protected $isAnimated;

    /**
     * Image Height in pixels
     *
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="height", type="integer", nullable=true, options={"unsigned":true})
     */
	protected $height;

    /**
     * Image width in pixels
     *
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="width", type="integer", nullable=true, options={"unsigned":true})
     */
	protected $width;

    /**
     * The image size in KiloBytes
     *
     * @var float
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="size", type="float", nullable=true, options={"unsigned":true})
     */
	protected $size;

    /**
     * File checksum
     *
     * @var string
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="checksum", type="string", length=40, nullable=true)
     */
	protected $checksum;

	/**
	 * Whether the image has been processed (thumbnails created, gif converted, etc.)
	 *
	 * @var boolean
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @ORM\Column(name="is_processed", type="boolean", nullable=true)
	 */
	protected $isProcessed;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="change", field={"category", "nsfw", "imageUrl", "postTitle", "isAnimated", "height", "width", "size"})
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=true)
     */
    protected $lastModifiedTime;

    /**
     * TODO: Fix blamable and timestampable to "on Change" instead of "on Update"
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="change", field={"category", "nsfw", "imageUrl", "postTitle", "isAnimated", "height", "width", "size"})
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_modified_by", referencedColumnName="id", nullable=true)
     * })
     *
     * @Serializer\Exclude
     *
     */
    protected $lastModifiedBy;


	/**
	 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
	 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
	 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
	 * must be able to accept an instance of 'File' as the bundle will inject one here
	 * during Doctrine hydration.
	 *
	 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
	 */
	public function setImageFile(File $image)
	{
		$this->imageFile = $image;

		if ($image) {
			// It is required that at least one field changes if you are using doctrine
			// otherwise the event listeners won't be called and the file is lost
			$this->lastModifiedTime = new \DateTime('now');
		}
	}

	/**
	 * @return File
	 */
	public function getImageFile()
	{
		return $this->imageFile;
	}

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     * @return PostImage
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string 
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set postTitle
     *
     * @param string $postTitle
     * @return PostImage
     */
    public function setPostTitle($postTitle)
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    /**
     * Get postTitle
     *
     * @return string
     */
    public function getPostTitle()
    {
        return $this->postTitle;
    }

    /**
     * Set isAnimated
     *
     * @param boolean $isAnimated
     * @return PostImage
     */
    public function setIsAnimated($isAnimated)
    {
        $this->isAnimated = $isAnimated;

        return $this;
    }

    /**
     * Get isAnimated
     *
     * @return boolean 
     */
    public function getIsAnimated()
    {
        return $this->isAnimated;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return PostImage
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return PostImage
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return PostImage
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set checksum
     *
     * @param string $checksum
     * @return PostImage
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Get checksum
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

	/**
	 * Set isProcessed
	 *
	 * @param boolean $isProcessed
	 * @return PostImage
	 */
	public function setIsProcessed($isProcessed)
	{
		$this->isProcessed = $isProcessed;

		return $this;
	}

	/**
	 * Get isProcessed
	 *
	 * @return boolean
	 */
	public function getIsProcessed()
	{
		return $this->isProcessed;
	}

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return PostImage
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }

    /**
     * Set lastModifiedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $lastModifiedBy
     * @return PostImage
     */
    public function setLastModifiedBy(\Rasen\NineGagBundle\Entity\User $lastModifiedBy = null)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }
}
