var http = require('http');
var url = require('url');
var sockjs = require('sockjs');
var redis = require('redis');
var jwt = require('jwt-simple');
var context = require('rabbit.js').createContext('amqp://localhost:5672');

var port = process.argv[2] || 8080;

var PHPSESSION_PREFIX = 'JSON_PHPSESSION:';
var JWT_SECRET = "%VtMZ@5w$PPe3RYwT#$ev9TsSnDeqYQX";

var httpserver = http.createServer();// Listen for SockJS connections

var sockjs_opts = {
  sockjs_url: "http://cdn.jsdelivr.net/sockjs/0.3.4/sockjs.min.js"
};
var sjs = sockjs.createServer(sockjs_opts);

sjs.installHandlers(httpserver, {prefix: '[/]socks'});

context.on('ready', function() {

    var redisClient = redis.createClient();

    sjs.on('connection', function(connection) {
		//connection.pub = context.socket('PUB', {routing: 'topic'});
		connection.sub = context.socket('SUB', {routing: 'topic'});
		connection.sub.setEncoding('utf8');

    	console.log('connected');

		connection.on('close', function() {
			connection.sub.close();
		});

		connection.sub.on('data', function(data) {
			var message = JSON.parse(data);
			if (connection.hasSession && message.username == connection.username) {
				connection.write(data);
				console.log(connection.username + ' ==> ' + message.link);
			}
		});

    	connection.hasSession = false;
    	connection.isFirstData = true;

	    connection.on('data', function (data) {
	    	console.log('received data!');
	    	if (connection.isFirstData) {
	    		connection.isFirstData = false;
	    		var data = JSON.parse(data);
				if (data.token) {
					var sessionId = jwt.decode(data.token, JWT_SECRET);
					connection.sessionKeyName = PHPSESSION_PREFIX + sessionId;
					redisClient.get(connection.sessionKeyName, function (error, result) {
						if(error){
				            console.log("error : "+error);
				        }
				        if(result && result.toString() != ""){
							connection.session = JSON.parse(result);
							if (connection.session.username) {
								connection.hasSession = true;
								connection.username = connection.session.username;
							} else {
								connection.close(403, 'Forbidden');
							}

							connection.sub.connect('notification','notification.' + connection.username, function() {
								//sub.pipe(connection);
								var msg = 'connected to ' + 'notification.' + connection.username + ' topic!';
								connection.write(JSON.stringify({content: msg, data_type: 'server_msg'}));
								console.log(msg);
						  	});
				        }else{
				            console.log("session does not exist");
				            connection.close(403, 'Forbidden');
				        }
					});
				}
	    	} else {
	    		console.log('not the first data!')
	    	}
	    	
	    });
  	});

	httpserver.listen(port, '0.0.0.0');
});