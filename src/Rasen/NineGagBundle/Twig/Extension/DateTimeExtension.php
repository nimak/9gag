<?php

/*
 * This file is part of the Sonata project.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rasen\NineGagBundle\Twig\Extension;

use Rasen\NineGagBundle\Templating\Helper\DateTimeHelper;
use Sonata\IntlBundle\Twig\Extension\DateTimeExtension as SonataDateTimeExtension;

/**
 * DateTimeExtension extends Twig with localized date/time capabilities.
 *
 * @author Thomas Rabaix <thomas.rabaix@ekino.com>
 */
class DateTimeExtension extends SonataDateTimeExtension
{

    protected $helper;

    public function __construct(DateTimeHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'rasen_nine_gag_intl_datetime';
    }
}
