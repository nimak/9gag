<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/11/2014
 * Time: 4:14 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\ORM\EntityManager;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostCategory;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Lib\UrlSlugGenerator;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Hashids\Hashids;
use Rasen\NineGagBundle\Lib\PostImageProcessor;
use Rasen\NineGagBundle\Entity\UserBadge;
use Rasen\NineGagBundle\Entity\Badge;

use \Doctrine\ORM\Query\ResultSetMappingBuilder;
/**
 * Class SerializerSubscriber
 *
 * @DI\Service("event_listener.serializer_subscriber")
 * @DI\Tag("jms_serializer.event_subscriber")
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class SerializerSubscriber implements EventSubscriberInterface
{
    const POST_THUMB_WIDTH = 500;
    const POST_THUMB_BIG_WIDTH = 700;

	private $uploaderHelper;

	private $liipImagine;

	private $router;

	private $securityContext;

	private $em;

	/**
	 * @var Hashids $hashids
	 */
	private $hashids;

	/**
	 * @var PostImageProcessor $postImageProcessor
	 */
	private $postImageProcessor;

	/**
	 * @var AuthorizationChecker $authorizationChecker
	 */
	private $authorizationChecker;

	/**
	 * @var UrlSlugGenerator $urlSlugGenerator
	 */
	private $urlSlugGenerator;

	/**
	 * @DI\InjectParams({
	 *     "uploaderHelper" = @DI\Inject("vich_uploader.templating.helper.uploader_helper"),
	 *     "liipImagine" = @DI\Inject("liip_imagine.cache.manager"),
	 *     "router" = @DI\Inject("router"),
	 *     "securityContext" = @DI\Inject("security.context"),
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
	 *     "hashids" = @DI\Inject("hashids"),
	 *     "postImageProcessor" = @DI\Inject("rasen_ninegag.post_image_processor"),
	 *     "authorizationChecker" = @DI\Inject("security.authorization_checker"),
	 *     "urlSlugGenerator" = @DI\Inject("rasen_ninegag.url_slug_generator")
	 * })
	 *
	 * @param UploaderHelper $uploaderHelper
	 * @param CacheManager $liipImagine
	 * @param Router $router
	 * @param SecurityContext $securityContext
	 * @param EntityManager $em
	 * @param Hashids $hashids
	 * @param PostImageProcessor $postImageProcessor
	 * @param AuthorizationChecker $authorizationChecker
	 * @param UrlSlugGenerator $urlSlugGenerator
	 */
	public function __construct(UploaderHelper $uploaderHelper,
		CacheManager $liipImagine,
		Router $router,
		SecurityContext $securityContext,
		EntityManager $em,
		Hashids $hashids,
		PostImageProcessor $postImageProcessor,
		AuthorizationChecker $authorizationChecker,
		UrlSlugGenerator $urlSlugGenerator)
	{
		$this->uploaderHelper = $uploaderHelper;
		$this->liipImagine = $liipImagine;
		$this->router = $router;
		$this->securityContext = $securityContext;
		$this->em = $em;
		$this->hashids = $hashids;
		$this->postImageProcessor = $postImageProcessor;
		$this->authorizationChecker = $authorizationChecker;
		$this->urlSlugGenerator = $urlSlugGenerator;
	}

	public static function getSubscribedEvents()
	{
		return array(
			array('event' => 'serializer.post_serialize', 'method' => 'onPostSerialize'),
		);
	}

	public function onPostSerialize(ObjectEvent $event)
	{
		$object = $event->getObject();
		$currentUser = $this->securityContext->getToken()->getUser();
		if ($object instanceof Post) {
			$hashid = $this->hashids->encode($object->getId());

			$event->getVisitor()->addData('id', $hashid);

			//Check If the user's logged in and owns the post
			if ($object->getCreatedBy() === $currentUser) {
				$event->getVisitor()->addData('own', true);

				//Add owner specific data
				$event->getVisitor()->addData('is_approved', $object->getIsApproved());
			} else {
				$event->getVisitor()->addData('own', false);
			}

			//Get Post's single view url
			$postUrl = $this->urlSlugGenerator->generateUrl($object);
			$event->getVisitor()->addData('href', $postUrl);

			//Get Current User's Vote
			$vote = $this->em->getRepository('RasenNineGagBundle:PostVote')->findBy(array(
				'post' => $object,
				'votedBy' => $currentUser
			));
			if (!empty($vote) && $vote[0] instanceof PostVote) {
				$event->getVisitor()->addData('vote', $vote[0]->getVote());
			} else {
				$event->getVisitor()->addData('vote', null);
			}

			//Get Current User's Report
			$report = $this->em->getRepository('RasenNineGagBundle:PostReport')->findBy(array(
				'post' => $object,
				'reporter' => $currentUser
			));
			if (!empty($report) && $report[0] instanceof PostReport) {
				$event->getVisitor()->addData('reported', true);
			} else {
				$event->getVisitor()->addData('reported', false);
			}

			//Set image url to cached one
			if ($object instanceof PostImage) {
				$url = $this->uploaderHelper->asset($object, 'posts');

                $imageBigWidth = self::POST_THUMB_BIG_WIDTH;
                if ($object->getWidth() && $object->getHeight()) {
                    $imageBigHeight = floor((self::POST_THUMB_BIG_WIDTH/$object->getWidth()) * $object->getHeight());
                } else {
                    $imageBigHeight = '';
                }
				$cachedBigUrl = $this->liipImagine->getBrowserPath($url, 'posts_full');
				$event->getVisitor()->addData('image_big', array('url'=>$cachedBigUrl, 'width'=>$imageBigWidth, 'height'=>$imageBigHeight));

                /*
                $imageWidth = self::POST_THUMB_WIDTH;
                $imageHeight = floor((self::POST_THUMB_WIDTH/$object->getWidth()) * $object->getHeight());
				$cachedUrl = $this->liipImagine->getBrowserPath($url, 'posts_image');
                */
				$event->getVisitor()->addData('image', array('url'=>$cachedBigUrl, 'width'=>$imageBigWidth, 'height'=>$imageBigHeight));

				if ($object->getIsAnimated()) {
					$gifUrl = $this->uploaderHelper->asset($object, 'posts');
					$event->getVisitor()->addData('gif_url', $gifUrl);

					$mp4Url = $this->postImageProcessor->getProcessedImage($object, 'mp4');
					$event->getVisitor()->addData('mp4_url', $mp4Url);
					$webmUrl = $this->postImageProcessor->getProcessedImage($object, 'webm');
					$event->getVisitor()->addData('webm_url', $webmUrl);
				}
			}

		}
		elseif ($object instanceof Comment)
		{
			$hashid = $this->hashids->encode($object->getId());
			$event->getVisitor()->addData('id',  $hashid);

			if ($object->isDeleted()) {
				$event->getVisitor()->addData('deleted', $object->isDeleted());
				return false;
			}

			//Check If the user's logged in and owns the comment
			if ($object->getCreatedBy() === $currentUser) {
				$event->getVisitor()->addData('own', true);

				//Add owner specific data
				$event->getVisitor()->addData('approved', $object->getApproved());
			} else {
				$event->getVisitor()->addData('own', false);
			}

			//Get Current User's Vote
			$vote = $this->em->getRepository('RasenNineGagBundle:CommentVote')->findBy(array(
				'comment' => $object,
				'votedBy' => $currentUser
			));
			if (!empty($vote) && $vote[0] instanceof CommentVote) {
				$event->getVisitor()->addData('vote', $vote[0]->getVote());
			} else {
				$event->getVisitor()->addData('vote', null);
			}

			//Get Current User's Report
			$report = $this->em->getRepository('RasenNineGagBundle:CommentReport')->findBy(array(
				'comment' => $object,
				'reporter' => $currentUser
			));
			if (!empty($report) && $report[0] instanceof CommentReport) {
				$event->getVisitor()->addData('reported', true);
			} else {
				$event->getVisitor()->addData('reported', false);
			}


			//$rq = $this->em->getRepository('RasenNineGagBundle:CommentMention')->createQueryBuilder('cr');
/*
			$rsm = new ResultSetMappingBuilder($this->em);
			$rsm->addRootEntityFromClassMetadata('RasenNineGagBundle:CommentMention', 'cm');
			$rsm->addJoinedEntityFromClassMetadata('RasenNineGagBundle:User', 'u', 'cm', 'user', array('id' => 'user'));
			$selectClause = $rsm->generateSelectClause(array(
				'cm' => 'cm',
				'c' => 'c'
			));
			$sql = 'SELECT '.$selectClause.' '.
			       'FROM comments_mentions cm '.
			       'INNER JOIN users u ON cm.user_id = u.id '.
			       'LEFT JOIN comments c ON cm.comment_id = c.id '.
			       'WHERE cm.comment_id = :commentId '.
			       'ORDER BY c.created_time '
			;
			$unionSql = '('.$sql.'DESC LIMIT 2 '.')'.
			            'UNION '.
			            '('.$sql.'ASC LIMIT 2'.')'
			;
			$q = $this->em->createNativeQuery($unionSql, $rsm);
			$q->setParameter('commentId', $object->getId());

			if(!empty($q->getArrayResult()))
				die(var_dump($q->getArrayResult()));


			$reps = $q->getResult();
			$event->getVisitor()->addData('mentions', $reps);
*/
			/*$mentions = $this->em->getRepository('RasenNineGagBundle:CommentMention')->findBy(array(
				'comment' => $object
			));*/
		}
		elseif ($object instanceof User)
		{
			//Get User's profile view url
			$postUrl = $this->router->generate('rasen_ninegag_user_profile', array('username' => $object->getUsername()));
			$event->getVisitor()->addData('href', $postUrl);

			//Set image url to cached one
			if ($this->uploaderHelper->asset($object, 'profile_pics')) {
				$url = $this->uploaderHelper->asset($object, 'profile_pics');
			} else {
				$url = '/bundles/rasenninegag/images/empty_user.png';
			}
			$cachedUrl = $this->liipImagine->getBrowserPath($url, 'square_48');
			$event->getVisitor()->addData('image_url', $cachedUrl);

			//set follow state
			$userFollower = $this->em->getRepository('RasenNineGagBundle:UserFollower')->findBy(array(
				'user' => $object,
				'follower' => $currentUser
			));
			if (!empty($userFollower) && $userFollower[0] instanceof UserFollower) {
				$event->getVisitor()->addData('followed', true);
			} else {
				$event->getVisitor()->addData('followed', false);
			}
		}
		elseif ($object instanceof PostCategory)
		{
			//Get category view url
			$categoryUrl = $this->router->generate('rasen_ninegag_postcategory_view', array('slug' => $object->getSlug()));
			$event->getVisitor()->addData('href', $categoryUrl);
		}
		if (true === $this->authorizationChecker->isGranted('edit', $object)) {
			$event->getVisitor()->addData('can_edit', true);
		}
		if (true === $this->authorizationChecker->isGranted('delete', $object)) {
			$event->getVisitor()->addData('can_delete', true);
		}
        elseif ($object instanceof Badge)
        {
            if ($this->uploaderHelper->asset($object, 'badge_images')) {
                $url = $this->uploaderHelper->asset($object, 'badge_images');
                $cachedUrl = $this->liipImagine->getBrowserPath($url, 'square_24');
                $event->getVisitor()->addData('image_url', $cachedUrl);
            }
        }
	}
} 