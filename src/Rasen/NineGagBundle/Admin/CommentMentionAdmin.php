<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/15/2014
 * Time: 11:43 AM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class CommentMentionAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class CommentMentionAdmin extends Admin
{
	protected $parentAssociationMapping = 'comment';

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'comment.createdTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'comment-mention';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('comment_mention',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('user', 'sonata_type_model_list', array(), array('placeholder'=>'comment_mention'))
			->add('comment', 'sonata_type_model_list', array(), array('placeholder'=>'comment'))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('user.username')
			->add('comment.id')
			->add('comment.createdTime')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('user')
			->add('comment')
			->add('comment.createdTime')
		;
	}
} 