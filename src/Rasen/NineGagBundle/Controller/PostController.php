<?php

namespace Rasen\NineGagBundle\Controller;

use JMS\Serializer\SerializationContext;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Event\PostViewEvent;
use Rasen\NineGagBundle\Form\Type\PostImageType;
use Rasen\NineGagBundle\Lib\PostEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class PostController
 * @package Rasen\NineGagBundle\Controller
 */
class PostController extends Controller
{
	/**
	 * @param $name
	 *
	 * @return array
	 */
    public function indexAction($name)
    {
        return array('name' => $name);
    }


	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 *
	 * @Route("/upload")
	 * @return array
	 */
	public function uploadAction(Request $request)
	{
		if ($request->request->has('_src') && $request->request->has('_src') == 'modal') {
			$response = $this->uploadModal($request);
			return $response;
		}
		$response = $this->uploadPage($request);
		return $response;
	}

	private function uploadModal(Request $request) {

		$csrfValid = $this->get('form.csrf_provider')->isCsrfTokenValid('upload',$request->request->get('_token'));
		if(!$csrfValid){
			throw $this->createAccessDeniedException();
		}

        if (
            !$request->files->has('image') ||
            !$request->request->has('slug') ||
            ($request->files->get('image') == null)
        ) {
            throw new BadRequestHttpException();
        }
		$title = $request->request->get('title');
		$image = $request->files->get('image');
        $slug = $request->request->get('slug');

		$nsfw = $request->request->get('nsfw', false);
		if ($nsfw == 'on') $nsfw = true;

		$newPost = new PostImage();
		$newPost->setNsfw($nsfw);
		$newPost->setPostTitle($title);
		$newPost->setImageFile($image);
		$newPost->setCreatedBy($this->getUser());

        $uploadCategory=$this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(array('slug' => $slug));
        $newPost->setCategory($uploadCategory);

		// Approve all by default
		//$newPost->setIsApproved(true);

		$errors = $this->get('validator')->validate($newPost);
		if (count ($errors)>0){
			return $this->uploadPage($request, array(
				'errors'  => $errors
			));
			//return new Response(var_dump($errors));
		}

        /**
         * @var UploadedFile $image
         */
        $checksum = sha1_file($image->getRealPath());
        if ($duplicatePost = $this->checkDuplicatePost($checksum)){
            return $this->redirect($this->container->get('rasen_ninegag.url_slug_generator')->generateUrl($duplicatePost));
        }
        $newPost->setChecksum($checksum);

		$em = $this->getDoctrine()->getManager();
		$em->persist($newPost);
		$em->flush();

        $this->container->get('rasen_ninegag.duplicate_post_checker')->markAsUploaded($newPost);

		return $this->redirect($this->container->get('rasen_ninegag.url_slug_generator')->generateUrl($newPost));
	}

	/**
	 *
	 * @param Request $request
	 * @param $errors
	 *
	 * @return array
	 */
    private function uploadPage(Request $request, $errors = null)
    {
	    $post = new PostImage();
	    $form = $this->createForm(new PostImageType(), $post);
	    $form->handleRequest($request);
	    if ($form->isSubmitted()) {
		    if ($form->isValid()) {
                if (
                !$request->files->has('post_image') || empty($request->files->get('post_image')['imageFile'])
                ) {
                    throw new BadRequestHttpException();
                }

                /**
                 * @var UploadedFile $file
                 */
                $file = $request->files->get('post_image')['imageFile'];
                $checksum = sha1_file($file->getRealPath());
                if ($duplicatePost = $this->checkDuplicatePost($checksum)){
                    return $this->redirect($this->container->get('rasen_ninegag.url_slug_generator')->generateUrl($duplicatePost));
                }
                $post->setChecksum($checksum);

                $em = $this->getDoctrine()->getManager();
			    $em->persist($post);
			    $em->flush();

                $this->container->get('rasen_ninegag.duplicate_post_checker')->markAsUploaded($post);

			    return $this->redirect($this->container->get('rasen_ninegag.url_slug_generator')->generateUrl($post));
		    }
	    }
        return $this->render('RasenNineGagBundle:Post:new.html.twig',array(
			'form' => $form->createView(),
			'errors' => $errors
		));
    }

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @Route("/new/text")
	 * @return array
	 */
    public function newTextAction()
    {
	    $post = new PostText();

        return new Response();
    }

	public function viewPost(Request $request, Post $post)
	{
		$nextPost = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post")->findNextPostByPost($post);
		if (empty($nextPost) || !$nextPost[0] instanceof Post) {
			$nextPost = null;
		} else {
			$nextPost = $nextPost[0];
		}

		$post->setEmbeddedCommentsLimit(10);

		$serializer = $this->container->get('jms_serializer');

        $this->disableSoftDeleteFilter();
		$postJson = $serializer->serialize($post, 'json');
        $this->enableSoftDeleteFilter();

        /**
         * @var EventDispatcher $dispatcher
         */
        $dispatcher = $this->get('event_dispatcher');
        $postViewEvent = new PostViewEvent(array($post), $request);
        $dispatcher->dispatch(PostEvents::POST_VIEW, $postViewEvent);

		$response = $this->render('RasenNineGagBundle:Post:singlePost.html.twig', array(
			'post' =>$post,
			'postJson' => $postJson,
			'nextPost' => $nextPost
		));

		return $response;
	}

	/**
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Route("/{hashId}/{slug}")
	 * @return array
	 * @throws NotFoundHttpException
	 */
	public function viewSlugAction(Request $request, $hashId, $slug)
	{
		$id = $this->container->get('hashids')->decode($hashId);
		$post = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post")->findOneBy(array("id" => $id));
		if (!$post) {
			throw new NotFoundHttpException();
		}
		if ($post instanceof PostImage) {
			$postSlug = $this->container->get('rasen_ninegag.url_slug_generator')->slugify($post->getPostTitle());
			if ($postSlug != ''){
				if ($postSlug != $slug) {
					return $this->redirect($this->generateUrl('rasen_ninegag_post_viewslug', array('hashId' => $hashId, 'slug' => $postSlug)), 301);
				}
			} else {
				return $this->redirect($this->generateUrl('rasen_ninegag_post_view', array('hashId'=>$hashId)), 301);
			}
		} else {
			return $this->redirect($this->generateUrl('rasen_ninegag_post_view', array('hashId'=>$hashId)), 301);
		}

		return $this->viewPost($request, $post);
	}
	/**
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Route("/{hashId}")
	 * @return array
	 * @throws NotFoundHttpException
	 */
	public function viewAction(Request $request, $hashId)
	{
		$id = $this->container->get('hashids')->decode($hashId);
		$post = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post")->findOneBy(array("id" => $id));
		if (!$post) {
			throw new NotFoundHttpException();
		}

		if ($post instanceof PostImage) {
			$slug = $this->container->get('rasen_ninegag.url_slug_generator')->slugify($post->getPostTitle());
			if ($slug != '') {
				return $this->redirect($this->generateUrl('rasen_ninegag_post_viewslug', array('hashId'=>$hashId, 'slug'=>$slug)), 301);
			}
		}

		return $this->viewPost($request, $post);
	}

    private function checkDuplicatePost($checksum)
    {
        if ($duplicatePostId = $this->container->get('rasen_ninegag.duplicate_post_checker')->isDuplicate($checksum, $this->getUser()))
        {
            $duplicatePost = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostImage')->findOneById($duplicatePostId);
            if ($duplicatePost instanceof PostImage) {
                return $duplicatePost;
            }
        }
        return false;
    }

    private function disableSoftDeleteFilter()
    {
        if (array_key_exists('softdeleteable',$this->getDoctrine()->getManager()->getFilters()->getEnabledFilters())) {
            $this->getDoctrine()->getManager()->getFilters()->disable('softdeleteable');
        }
    }

    private function enableSoftDeleteFilter()
    {
        if (!array_key_exists('softdeleteable',$this->getDoctrine()->getManager()->getFilters()->getEnabledFilters())) {
            $this->getDoctrine()->getManager()->getFilters()->enable('softdeleteable');
        }
    }
}
