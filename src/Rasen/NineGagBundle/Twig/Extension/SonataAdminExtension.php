<?php

namespace Rasen\NineGagBundle\Twig\Extension;

use Doctrine\Common\Util\ClassUtils;
use Sonata\AdminBundle\Admin\FieldDescriptionInterface;
use Sonata\AdminBundle\Exception\NoValueException;
use Sonata\AdminBundle\Admin\Pool;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Psr\Log\LoggerInterface;
use Sonata\AdminBundle\Twig\Extension\SonataAdminExtension as BaseSonataAdminExtension;

/**
 * Class SonataAdminExtension
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class SonataAdminExtension extends BaseSonataAdminExtension
{
    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($model)
    {
        $admin = $this->pool->getAdminByClass(
            ClassUtils::getClass($model)
        );

        return $admin->getUrlsafeIdentifier($model);
    }
}
