<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * UserBadge
 *
 * Stores the badges users have taken.
 *
 * @ORM\Table(name="users_badges", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_badge_code_UNIQUE", columns={"user_id", "badge_code"})}, indexes={@ORM\Index(name="users_badges_user_id_idx", columns={"user_id"}), @ORM\Index(name="users_badges_badge_id_idx", columns={"badge_id"}), @ORM\Index(name="users_badges_given_by_idx", columns={"given_by"}), @ORM\Index(name="users_badges_code_idx", columns={"badge_code"})})
 * @ORM\Entity @ORM\HasLifecycleCallbacks
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @UniqueEntity(
 *     fields={"user", "badgeCode"},
 *     errorPath="badgeCode",
 *     message="user.badge.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class UserBadge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="given_time", type="datetime", nullable=false)
     */
    private $givenTime;

    /**
     * The badge's unique code
     *
     * Each badge has an unique code assigned to it, which will be sent to users and they can claim it using that code.
     *
     * @var string
     *
     * @ORM\Column(name="badge_code", type="string", length=255, nullable=false)
     */
    private $badgeCode;

    /**
     * Whether the user has claimed the badge
     *
     * A badge must be claimed before it appears on the user's profile page
     *
     * @var boolean
     *
     * @ORM\Column(name="is_claimed", type="boolean", nullable=false)
     */
    private $isClaimed;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \Rasen\NineGagBundle\Entity\Badge
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\Badge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="badge_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $badge;

    /**
     * Who gave the user the badge
     *
     * If the badge has been given to the user by an administrator, it will be stored here.
     *
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="given_by", referencedColumnName="id")
     * })
     */
    private $givenBy;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set givenTime
     *
     * @param \DateTime $givenTime
     * @return UserBadge
     */
    public function setGivenTime($givenTime)
    {
        $this->givenTime = $givenTime;

        return $this;
    }

    /**
     * Get givenTime
     *
     * @return \DateTime 
     */
    public function getGivenTime()
    {
        return $this->givenTime;
    }

    /**
     * Set badgeCode
     *
     * @param string $badgeCode
     * @return UserBadge
     */
    public function setBadgeCode($badgeCode)
    {
        $this->badgeCode = $badgeCode;

        return $this;
    }

    /**
     * Get badgeCode
     *
     * @return string 
     */
    public function getBadgeCode()
    {
        return $this->badgeCode;
    }

    /**
     * Set isClaimed
     *
     * @param boolean $isClaimed
     * @return UserBadge
     */
    public function setIsClaimed($isClaimed)
    {
        $this->isClaimed = $isClaimed;

        return $this;
    }

    /**
     * Get isClaimed
     *
     * @return boolean 
     */
    public function getIsClaimed()
    {
        return $this->isClaimed;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return UserBadge
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set badge
     *
     * @param \Rasen\NineGagBundle\Entity\Badge $badge
     * @return UserBadge
     */
    public function setBadge(\Rasen\NineGagBundle\Entity\Badge $badge = null)
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * Get badge
     *
     * @return \Rasen\NineGagBundle\Entity\Badge 
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set givenBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $givenBy
     * @return UserBadge
     */
    public function setGivenBy(\Rasen\NineGagBundle\Entity\User $givenBy = null)
    {
        $this->givenBy = $givenBy;

        return $this;
    }

    /**
     * Get givenBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getGivenBy()
    {
        return $this->givenBy;
    }

	/**
	 * @ORM\PrePersist
	 */
	public function updateBadgeCode()
	{
		$slug = $this->badge->getCode() . $this->user->getId() . uniqid();
		if (function_exists('mb_strtoupper')) {
			$slug = mb_strtoupper($slug);
		} else {
			$slug = strtoupper($slug);
		}
		$this->badgeCode = $slug;
	}
}
