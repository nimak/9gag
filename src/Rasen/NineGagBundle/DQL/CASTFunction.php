<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/20/2014
 * Time: 1:36 AM
 */

namespace Rasen\NineGagBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\Lexer;

/**
 * Class CASTFunction
 * @package Rasen\NineGagBundle\DQL
 */
class CASTFunction extends FunctionNode
{
	public $firstDateExpression = null;
	public $unit = null;

	/**
	 * {@inheritdoc}
	 */
	public function parse(\Doctrine\ORM\Query\Parser $parser)
	{
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->firstDateExpression = $parser->StringPrimary();

		$parser->match(Lexer::T_AS);

		$parser->match(Lexer::T_IDENTIFIER);
		$lexer = $parser->getLexer();
		$this->unit = $lexer->token['value'];

		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
	{
		return sprintf('CAST(%s AS %s)',  $this->firstDateExpression->dispatch($sqlWalker), $this->unit);
	}
} 