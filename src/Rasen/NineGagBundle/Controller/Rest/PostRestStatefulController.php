<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use Doctrine\ORM\Query;
use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\PaginatedRepresentation;
use JMS\Serializer\SerializationContext;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostCategory;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;

use Rasen\NineGagBundle\Event\PostViewEvent;
use Rasen\NineGagBundle\Hateoas\Representation\CursorPaginatedRepresentation;
use Rasen\NineGagBundle\Lib\CursorPagination\Cursor;
use Rasen\NineGagBundle\Lib\CursorPagination\CursorPaginator;
use Rasen\NineGagBundle\Lib\PostEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
/**
 * Class PostRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class PostRestStatefulController extends FOSRestController
{
	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Delete("/posts/{hashId}")
	 * @Rest\RequestParam(name="hashId", description="Post hash id")
	 *
	 * @return Response
	 */
	public function postDeleteAction(Request $request, $hashId)
	{
		$post = $this->getPostObject($hashId);
		if (false === $this->get('security.authorization_checker')->isGranted('delete', $post)) {
			throw new AccessDeniedException('Unauthorized access!');
		}

        $em = $this->getDoctrine()->getManager();

        $em->remove($post);
        $em->flush();

		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Put("/posts/{hashId}")
	 * @Rest\RequestParam(name="hashId", description="Post hash id")
	 *
	 * @return Response
	 */
	public function postEditAction(Request $request, $hashId)
	{
		$post = $this->getPostObject($hashId);
		if (false === $this->get('security.authorization_checker')->isGranted('edit', $post)) {
			throw new AccessDeniedException('Unauthorized access!');
		}

		$categorySlug = $request->request->get('category');
		/**
		 * @var PostCategory $category
		 */
		$category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(array(
			'slug' => $categorySlug
		));
		if (!$category) {
			$categoryNotFoundView = $this->view(array('error'=>'Category not found!'), 404);
			return $this->handleView($categoryNotFoundView);
		}

		$post->setCategory($category);
		if ($post instanceof PostImage) {
			if ($request->request->has('title')) {
				$title = $request->request->get('title');
				$post->setPostTitle($title);
			}
		}
		$errors = $this->get('validator')->validate($post);
		if (count ($errors)>0){
			$postValidationErrors = $this->view($errors, 400);
			return $this->handleView($postValidationErrors);
		}
		$this->getDoctrine()->getManager()->flush();
		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Post("/posts/{hashId}/votes")
	 * @Rest\RequestParam(name="hashId", description="Post hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function postVoteAction(Request $request, $hashId)
	{
		//if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();

		//$id = $this->container->get('hashids')->decode($hashId);

		$user = $this->getUser();
		$post = $this->getPostObject($hashId);

		$vote = $request->request->get('vote');
		$remove = $request->request->get('remove');


		$okView = $this->view('ok', 204);

		/**
		 * @var PostVote $postVote
		 */
		$postVote = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostVote')->findBy(array(
			'post' => $post,
			'votedBy' => $user
		));
		$em = $this->getDoctrine()->getManager();

		if (empty($postVote)) {
			$postVote = new PostVote();
			$postVote->setPost($post)->setVotedBy($user)->setVote($vote);
			$em->persist($postVote);
			$em->flush();
			return $this->handleView($okView);
		} elseif ($postVote[0] instanceof PostVote) {
			if ($remove) {
				$em->remove($postVote[0]);
				$em->flush();
				return $this->handleView($okView);
			} else {
				$postVote[0]->setVote($vote);
				$em->flush();
				return $this->handleView($okView);
			}
		}
		$koView = $this->view('ko', 400);
		return $this->handleView($koView);
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param $hashId
	 *
	 * @Rest\Delete("/posts/{hashId}/votes")
	 * @Rest\RequestParam(name="hashId", description="Post hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function deleteVoteAction($hashId)
	{
		//if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();

		//$id = $this->container->get('hashids')->decode($hashId);

		$user = $this->getUser();
		$post = $this->getPostObject($hashId);


		/**
		 * @var PostVote $postVote
		 */
		$postVote = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostVote')->findBy(array(
			'post' => $post,
			'votedBy' => $user
		));

		if (!$postVote) {
			throw new NotFoundHttpException;
		}

		$em = $this->getDoctrine()->getManager();

		if ($postVote[0] instanceof PostVote) {
			$em->remove($postVote[0]);
			$em->flush();
			$okView = $this->view('ok', 204);
			return $this->handleView($okView);
		}

		return $koView = $this->view('ko', 400);;
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 *
	 * @param Request $request
	 * @param $hashId
	 *
	 * @Rest\Post("/posts/{hashId}/reports")
	 * @Rest\RequestParam(name="hashId", description="Post hash id")
	 *
	 * @return Response
	 * @throws AccessDeniedException
	 * @throws NotFoundHttpException
	 */
	public function postReportAction(Request $request, $hashId)
	{
		//if (! $request->isXmlHttpRequest()) throw new AccessDeniedException();


		//$id = $this->container->get('hashids')->decode($hashId);

		$user = $this->getUser();
		$post = $this->getPostObject($hashId);

		if ($user === $post->getCreatedBy()) {
			return $this->handleView($this->view('You can\'t report your own post!', 400));
		}

		$postReport = new PostReport();
		$postReport->setPost($post)->setReporter($user);

		$em = $this->getDoctrine()->getManager();
		$em->persist($postReport);
		$em->flush();

		$okView = $this->view('ok', 204);
		return $this->handleView($okView);
	}


    /**
     *
     * @param $hashId
     * @param Request $request
     *
     * @Rest\Get("/posts/{hashId}/votes")
     * @Rest\RequestParam(name="hashId", description="Post's id")
     *
     * @return Response
     */
	public function getPostVotesAction(Request $request, $hashId)
	{
        //ONLY UPVOTES

        $post = $this->getPostObject($hashId);

		$userRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:User");
		$query = $userRepo->findVotedOnPostQ($post, true);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'users');

		$resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 * @param $username
	 * @return Response
	 */
	public function getUserPostsAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");

		$own = ($user == $this->getUser());
		$query = $postRepo->findAllByUserQ($user, !$own);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 * @param $username
	 * @return Response
	 */
	public function getUserPostsUpvotedAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findVotedByUserQ($user, true);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 * @param $username
	 * @return Response
	 */
	public function getUserPostsCommentedAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findCommentedByUserQ($user);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 * @param $username
	 *
	 * @Rest\Get("/users/{username}/posts/stream")
	 * @Rest\RequestParam(name="username", description="username")
	 *
	 * @return Response
	 */
	public function getUserPostsStreamAction(Request $request, $username)
	{
		$user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
		if (!$user) {
			throw new NotFoundHttpException();
		}

		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findAllFollowingQ($user);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);

		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/home")
	 *
	 * @return Response
	 */
	public function getPostsHomeAction(Request $request)
	{
		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		//$posts = $postRepo->findAllFollowing($this->getUser());
		//if (empty($posts)) {
		$query = $postRepo->findFreshPostsQ();
		//}

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/nsfw")
	 *
	 * @return Response
	 */
	public function getPostsNsfwAction(Request $request)
	{
		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		//$posts = $postRepo->findAllFollowing($this->getUser());
		//if (empty($posts)) {
		$query = $postRepo->findNsfwPostsQ();
		//}

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/hot")
	 *
	 * @return Response
	 */
	public function getPostsHotAction(Request $request)
	{
		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findHotPostsQ();

		$pagination = $this->paginate($request, $query);
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/trending")
	 *
	 * @return Response
	 */
	public function getPostsTrendingAction(Request $request)
	{
		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findTrendingPostsQ();

		$pagination = $this->paginate($request, $query);
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/fresh")
	 *
	 * @return Response
	 */
	public function getPostsFreshAction(Request $request)
	{
		$postRepo = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post");
		$query = $postRepo->findFreshPostsQ();

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/category/{slug}")
	 * @Rest\RequestParam(name="slug", description="category slug")
	 *
	 * @return Response
	 */
	public function getPostsCategoryAction(Request $request, $slug)
	{
		$category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(
			array('slug' => $slug)
		);
		if (!$category){
			throw new NotFoundHttpException ();
		}
		$postRepo= $this->getDoctrine()->getRepository('RasenNineGagBundle:Post');
		$query=$postRepo->findFreshPostsByCategoryQ($category);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/category/{slug}/fresh")
	 * @Rest\RequestParam(name="slug", description="category slug")
	 *
	 * @return Response
	 */
	public function getPostsCategoryFreshAction(Request $request, $slug)
	{
		$category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(
			array('slug' => $slug)
		);
		if (!$category){
			throw new NotFoundHttpException ();
		}
		$postRepo= $this->getDoctrine()->getRepository('RasenNineGagBundle:Post');
		$query=$postRepo->findFreshPostsByCategoryQ($category);

		$pagination = $this->get('rasen_ninegag.cursor_paginator')->cursorPaginate($request, $query, 'posts');
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/category/{slug}/hot")
	 * @Rest\RequestParam(name="slug", description="category slug")
	 *
	 * @return Response
	 */
	public function getPostsCategoryHotAction(Request $request, $slug)
	{
		$category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(
			array('slug' => $slug)
		);
		if (!$category){
			throw new NotFoundHttpException ();
		}
		$postRepo= $this->getDoctrine()->getRepository('RasenNineGagBundle:Post');
		$query=$postRepo->findHotPostsByCategoryQ($category);

		$pagination = $this->paginate($request, $query);
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}

	/**
	 * @param Request $request
	 *
	 * @Rest\Get("/posts/category/{slug}/trending")
	 * @Rest\RequestParam(name="slug", description="category slug")
	 *
	 * @return Response
	 */
	public function getPostsCategoryTrendingAction(Request $request, $slug)
	{
		$category = $this->getDoctrine()->getRepository('RasenNineGagBundle:PostCategory')->findOneBy(
			array('slug' => $slug)
		);
		if (!$category){
			throw new NotFoundHttpException ();
		}
		$postRepo= $this->getDoctrine()->getRepository('RasenNineGagBundle:Post');
		$query=$postRepo->findTrendingPostsByCategoryQ($category);

		$pagination = $this->paginate($request, $query);
        $this->dispatchPostViewEvent($pagination->getInline()->getResources(), $request);

		$resp = $this->view($pagination, 200);
		return $this->handleView($resp);
	}


	/**
	 *
	 * @param Request $request
	 * @param $id
	 *
	 * @Rest\Get("/posts/{id}")
	 * @Rest\RequestParam(name="id", description="Post's id")
	 *
	 * @return Response
	 */
	public function getPostAction(Request $request, $id)
	{
		$post = $this->getPostObject($id);
        $this->dispatchPostViewEvent(array($post), $request);
		$resp = $this->view($post, 200);
		return $this->handleView($resp);
	}

	/**
	 * Returns post
	 *
	 * @param $hashId
	 *
	 * @return Post
	 */
	private function getPostObject($hashId)
	{
		$id = $this->container->get('hashids')->decode($hashId);
		/**
		 * @var Post $post
		 */
		$post = $this->getDoctrine()->getRepository('RasenNineGagBundle:Post')->findOneBy(array(
			'id' => $id
		));
		if (!$post) {
			throw new NotFoundHttpException();
		}
		return $post;
	}

    /**
     * @param $posts
     * @param $request
     */
    private function dispatchPostViewEvent($posts, $request)
    {
        /**
         * @var EventDispatcher $dispatcher
         */
        $dispatcher = $this->get('event_dispatcher');
        $postViewEvent = new PostViewEvent($posts, $request);
        $dispatcher->dispatch(PostEvents::POST_VIEW, $postViewEvent);
    }

	/**
	 * @param Request $request
	 * @param $query
	 * @return array
	 */
	private function paginate(Request $request, $query)
	{
		$paginator = $this->get('knp_paginator');
		$page = $request->query->get('page', 1);
		$limit = 25;
		/**
		 * @var SlidingPagination $pagination
		 */
		$pagination = $paginator->paginate(
			$query,
			$page,
			$limit,
			array('distinct' => false)
		);

		/*$result = array(
			'items' => $pagination->getItems(),
			'paginationData' => $pagination->getPaginationData()
		);*/

		$collectionRepresentation = new CollectionRepresentation(
			$pagination->getItems(),
			'posts'
		);
		$paginatedRepresentation = new PaginatedRepresentation(
			$collectionRepresentation,
			$request->get('_route'),
			$request->get('_route_params'),
			$pagination->getPaginationData()['current'],
			$pagination->getPaginationData()['currentItemCount'],
			$pagination->getPaginationData()['pageCount']
		);

		return $paginatedRepresentation;
	}
}
