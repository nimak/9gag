<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 6:15 PM
 */

namespace Rasen\NineGagBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class UpdateCountersCommand
 *
 * @TODO: complete this!
 *
 * @package Rasen\NineGagBundle\Command
 */
class UpdateCountersCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:counters:update')
			->setDescription('Update all counters in all entities')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/**
		 * @var ContainerInterface $container
		 */
		$container = $this->getContainer();

		/**
		 * @var EntityManager $em
		 */
		$em = $container->get('doctrine')->getManager();

		$output->writeln('Updating Comments Counter...');
		$em->getFilters()->disable('softdeleteable');
		$this->updateCommentCounter($em);

		$output->writeln('Updating Posts Counter...');
		$this->updatePostCounter($em);

		$output->writeln('Updating Users Counter...');
		$this->updateUserCounter($em);

		$em->getFilters()->enable('softdeleteable');
		$output->writeln('Done! ;)');
	}

	/**
	 * @param EntityManager $em
	 *
	 * @return mixed
	 */
	private function updatePostCounter(EntityManager $em)
	{
		$postRepo = $em->getRepository('RasenNineGagBundle:Post');
		$commentRepo = $em->getRepository('RasenNineGagBundle:Comment');
		$postReportsRepo = $em->getRepository('RasenNineGagBundle:PostReport');
		$postVoteRepo = $em->getRepository('RasenNineGagBundle:PostVote');

		// ** Post CommentsNo
		$rsm1 = new ResultSetMapping($em);
		$rsm1->addScalarResult('postId','postId');
		$rsm1->addScalarResult('commentCount','commentCount');
		$rsm1->addScalarResult('approvedCommentCount','approvedCommentCount');

		$postsCommentsCountDQL = "SELECT c.post_id AS postId, count(c.id) as commentCount, COUNT( IF(c.approved = 1, 1, null) ) as approvedCommentCount".
		                         " FROM comments c".
			" WHERE c.deleted_at IS NULL".
		                         " GROUP BY c.post_id"
		;
		$postsCommentsCountQ = $em->createNativeQuery($postsCommentsCountDQL, $rsm1);
		$postsCommentsCounts = $postsCommentsCountQ->getScalarResult();
		$postsCommentsCaseQ = "";
		$approvePostsCommentsCaseQ = "";
		foreach ($postsCommentsCounts as $postsCommentsCount) {
			if ($postsCommentsCount['postId'] !== null) {
				$postsCommentsCaseQ .= "WHEN " . $postsCommentsCount['postId'] . " THEN " . $postsCommentsCount['commentCount'] . " ";
				$approvePostsCommentsCaseQ .= "WHEN " . $postsCommentsCount['postId'] . " THEN " . $postsCommentsCount['approvedCommentCount'] . " ";
			}
		}
		// ** Post ReportsNo
		$postsReportsCountQ = $postReportsRepo->createQueryBuilder('pr')
		                                   ->select('p.id AS postId, count(pr.id) as reportCount')
		                                   ->leftJoin('pr.post', 'p')
		                                   ->groupBy('pr.post')
		                                   ->getQuery();
		;
		$postsReportsCount = $postsReportsCountQ->getScalarResult();;
		$postsReportsCaseQ = "";
		foreach ($postsReportsCount as $postReportsCount) {
			if ($postReportsCount['postId'] !== null) {
				$postsReportsCaseQ .= "WHEN " . $postReportsCount['postId'] . " THEN " . $postReportsCount['reportCount'] . " ";
			}
		}

		// ** Post VotesNo (Up / Down)

		$rsm = new ResultSetMapping($em);
		$rsm->addScalarResult('postId','postId');
		$rsm->addScalarResult('downVotesNo','downVotesNo');
		$rsm->addScalarResult('upVotesNo','upVotesNo');

		$postsVotesCountDQL = "SELECT pv.post_id as postId, COUNT(IF(pv.vote = 0, 1, null)) as downVotesNo, COUNT(IF(pv.vote = 1, 1, null)) as upVotesNo ".
		                        "FROM posts_votes pv ".
		                      "GROUP BY pv.post_id"
		;
		$postsVotesCountQ = $em->createNativeQuery($postsVotesCountDQL, $rsm)
		;
		$postsVotesCount = $postsVotesCountQ->getScalarResult();;
		$postsDownVotesCaseQ = "";
		$postsUpVotesCaseQ = "";
		foreach ($postsVotesCount as $postVotesCount) {
			if ($postVotesCount['postId'] !== null) {
				$postsDownVotesCaseQ .= "WHEN " . $postVotesCount['postId'] . " THEN " . $postVotesCount['downVotesNo'] . " ";
				$postsUpVotesCaseQ .= "WHEN " . $postVotesCount['postId'] . " THEN " . $postVotesCount['upVotesNo'] . " ";
			}
		}

		if ($postsCommentsCaseQ !== "" ||
		    $postsReportsCaseQ !== "" ||
		    $postsDownVotesCaseQ !== "" ||
		    $postsUpVotesCaseQ !== "" ||
		    $approvePostsCommentsCaseQ !== "")
		{
			$postUpdateQuery = "UPDATE RasenNineGagBundle:Post p SET ";
			if ($postsCommentsCaseQ !== "") {
				$postUpdateQuery .=
					"p.commentsNo = CASE p.id ".
					$postsCommentsCaseQ .
					" ELSE 0 END "
				;
				if ($postsReportsCaseQ !== "" || $postsDownVotesCaseQ !== "" || $postsUpVotesCaseQ !== "" || $approvePostsCommentsCaseQ !== "") $postUpdateQuery .= ' , ';
			}
			if ($postsReportsCaseQ !== "") {
				$postUpdateQuery .=
					"p.reportsNo = CASE p.id ".
					$postsReportsCaseQ .
					" ELSE 0 END "
				;
				if ($postsDownVotesCaseQ !== "" || $postsUpVotesCaseQ !== "" || $approvePostsCommentsCaseQ !== "") $postUpdateQuery .= ' , ';
			}
			if ($postsDownVotesCaseQ !== "") {
				$postUpdateQuery .=
					"p.downvotesNo = CASE p.id ".
					$postsDownVotesCaseQ .
					" ELSE 0 END "
				;
				if ($postsUpVotesCaseQ !== "" || $approvePostsCommentsCaseQ !== "") $postUpdateQuery .= ' , ';
			}
			if ($postsUpVotesCaseQ !== "") {
				$postUpdateQuery .=
					"p.upvotesNo = CASE p.id ".
					$postsUpVotesCaseQ .
					" ELSE 0 END"
				;
				if ($approvePostsCommentsCaseQ !== "") $postUpdateQuery .= ' , ';
			}
			if ($approvePostsCommentsCaseQ !== "") {
				$postUpdateQuery .=
					"p.approvedCommentsNo = CASE p.id ".
					$approvePostsCommentsCaseQ .
					" ELSE 0 END"
				;
			}
			$updatePostsQ = $em->createQuery($postUpdateQuery);
			$updatePosts = $updatePostsQ->execute();
			return $updatePosts;
		}
		return false;
	}

	/**
	 * @param EntityManager $em
	 *
	 * @return mixed
	 */
	private function updateCommentCounter(EntityManager $em)
	{
		// ** Comment RepliesNo
		$commentsRepo = $em->getRepository('RasenNineGagBundle:Comment');
		$commentsRepliesCountQ = $commentsRepo->createQueryBuilder('cr')
		                                      ->select('c.id AS commentId, count(cr.id) as commentReplyCount')
		                                      ->leftJoin('cr.parent', 'c')
			->where('cr.deletedAt is NULL')
		                                      ->groupBy('cr.parent')
		                                      ->getQuery();
		;
		$commentsRepliesCount = $commentsRepliesCountQ->getScalarResult();;
		$commentsRepliesCaseQ = "";
		foreach ($commentsRepliesCount as $commentRepliesCount) {
			if ($commentRepliesCount['commentId'] !== null) {
				$commentsRepliesCaseQ .= "WHEN " . $commentRepliesCount['commentId'] . " THEN " . $commentRepliesCount['commentReplyCount'] . " ";
			}
		}

		// ** Comment ReportsNo
		$commentsReportsRepo = $em->getRepository('RasenNineGagBundle:CommentReport');
		$commentsReportsCountQ = $commentsReportsRepo->createQueryBuilder('cr')
		                                      ->select('c.id AS commentId, count(cr.id) as reportCount')
		                                      ->leftJoin('cr.comment', 'c')
		                                      ->groupBy('cr.comment')
		                                      ->getQuery();
		;
		$commentsReportsCount = $commentsReportsCountQ->getScalarResult();;
		$commentsReportsCaseQ = "";
		foreach ($commentsReportsCount as $commentReportsCount) {
			if ($commentReportsCount['commentId'] !== null) {
				$commentsReportsCaseQ .= "WHEN " . $commentReportsCount['commentId'] . " THEN " . $commentReportsCount['reportCount'] . " ";
			}
		}

		// ** Comment VotesNo (Up / Down)

		$rsm = new ResultSetMapping($em);
		$rsm->addScalarResult('commentId','commentId');
		$rsm->addScalarResult('downVotesNo','downVotesNo');
		$rsm->addScalarResult('upVotesNo','upVotesNo');

		$commentsVotesCountDQL = "SELECT cv.comment_id as commentId, COUNT(IF(cv.vote = 0, 1, null)) as downVotesNo, COUNT(IF(cv.vote = 1, 1, null)) as upVotesNo".
		                      " FROM comments_votes cv".
		                         " GROUP BY cv.comment_id"
		;
		$commentsVotesCountQ = $em->createNativeQuery($commentsVotesCountDQL, $rsm)
		;
		$commentsVotesCount = $commentsVotesCountQ->getScalarResult();;
		$commentsDownVotesCaseQ = "";
		$commentsUpVotesCaseQ = "";
		foreach ($commentsVotesCount as $commentVotesCount) {
			if ($commentVotesCount['commentId'] !== null) {
				$commentsDownVotesCaseQ .= "WHEN " . $commentVotesCount['commentId'] . " THEN " . $commentVotesCount['downVotesNo'] . " ";
				$commentsUpVotesCaseQ .= "WHEN " . $commentVotesCount['commentId'] . " THEN " . $commentVotesCount['upVotesNo'] . " ";
			}
		}

		if ($commentsReportsCaseQ !== "" ||
		    $commentsDownVotesCaseQ !== "" ||
		    $commentsUpVotesCaseQ !== "" ||
		    $commentsRepliesCaseQ)
		{
			$commentUpdateQuery = "UPDATE RasenNineGagBundle:Comment c SET ";
			if ($commentsReportsCaseQ !== "") {
				$commentUpdateQuery .=
					"c.reportsNo = CASE c.id ".
					$commentsReportsCaseQ .
					" ELSE 0 END "
				;
				if ($commentsDownVotesCaseQ !== "" || $commentsUpVotesCaseQ !== "" || $commentsRepliesCaseQ !== "") $commentUpdateQuery .= ' , ';
			}
			if ($commentsDownVotesCaseQ !== "") {
				$commentUpdateQuery .=
					"c.downvotesNo = CASE c.id ".
					$commentsDownVotesCaseQ .
					" ELSE 0 END "
				;
				if ($commentsUpVotesCaseQ !== "" || $commentsRepliesCaseQ !== "") $commentUpdateQuery .= ' , ';
			}
			if ($commentsUpVotesCaseQ !== "") {
				$commentUpdateQuery .=
					"c.upvotesNo = CASE c.id ".
					$commentsUpVotesCaseQ .
					" ELSE 0 END"
				;
				if ($commentsRepliesCaseQ !== "") $commentUpdateQuery .= ' , ';
			}
			if ($commentsRepliesCaseQ !== "") {
				$commentUpdateQuery .=
					"c.repliesNo = CASE c.id ".
					$commentsRepliesCaseQ .
					" ELSE 0 END"
				;
			}
			$updateCommentsQ = $em->createQuery($commentUpdateQuery);
			$updateComments = $updateCommentsQ->execute();
			return $updateComments;
		}
		return false;
	}

	/**
	 * @param EntityManager $em
	 *
	 * @return mixed
	 */
	private function updateUserCounter(EntityManager $em)
	{
		$commentRepo = $em->getRepository('RasenNineGagBundle:Comment');

		$rsm1 = new ResultSetMapping($em);
		$rsm1->addScalarResult('userId','userId');
		$rsm1->addScalarResult('commentCount','commentCount');
		$rsm1->addScalarResult('approvedCommentCount','approvedCommentCount');

		$usersCommentsCountDQL = "SELECT c.created_by AS userId, count(c.id) as commentCount, COUNT( IF(c.approved = 1, 1, null) ) as approvedCommentCount".
		                         " FROM comments c".
			" WHERE c.deleted_at IS NULL".
		                         " GROUP BY c.created_by"
		;
		$usersCommentsCountQ = $em->createNativeQuery($usersCommentsCountDQL, $rsm1);
		$usersCommentsCount = $usersCommentsCountQ->getScalarResult();
		$usersCommentsCaseQ = "";
		$usersApprovedCommentsCaseQ = "";
		foreach ($usersCommentsCount as $userCommentsCount) {
			if ($userCommentsCount['userId'] !== null) {
				$usersCommentsCaseQ .= "WHEN " . $userCommentsCount['userId'] . " THEN " . $userCommentsCount['commentCount'] . " ";
				$usersApprovedCommentsCaseQ .= "WHEN " . $userCommentsCount['userId'] . " THEN " . $userCommentsCount['approvedCommentCount'] . " ";
			}
		}

		// ** User PostNo
		$postRepo = $em->getRepository('RasenNineGagBundle:Post');

		$rsm2 = new ResultSetMapping($em);
		$rsm2->addScalarResult('userId','userId');
		$rsm2->addScalarResult('postCount','postCount');
		$rsm2->addScalarResult('approvedPostCount','approvedPostCount');

		$postCountDQL = "SELECT p.created_by AS userId, count(p.id) as postCount, COUNT(IF(p.is_approved = 1, 1, null)) as approvedPostCount".
		                         " FROM posts p".
			" WHERE p.deleted_at IS NULL".
		                         " GROUP BY p.created_by"
		;
		$postCountQ = $em->createNativeQuery($postCountDQL, $rsm2);
		$postsCount = $postCountQ->getScalarResult();
		$postsCaseQ = "";
		$approvedPostsCaseQ = "";
		foreach ($postsCount as $postCount) {
			if ($postCount['userId'] !== null) {
				$postsCaseQ .= "WHEN " . $postCount['userId'] . " THEN " . $postCount['postCount'] . " ";
				$approvedPostsCaseQ .= "WHEN " . $postCount['userId'] . " THEN " . $postCount['approvedPostCount'] . " ";
			}
		}


		// ** User PointsNo
		$pointsRepo = $em->getRepository('RasenNineGagBundle:UserPoint');
		$pointsCountQ = $pointsRepo->createQueryBuilder('up')
			->select('u.id AS userId, SUM(up.point) as points')
			->leftJoin('up.user', 'u')
			->groupBy('up.user')
			->getQuery();
		;
		$pointsCount = $pointsCountQ->getScalarResult();;
		$pointsCaseQ = "";
		foreach ($pointsCount as $pointCount) {
			if ($pointCount['userId'] !== null) {
				$pointsCaseQ .= "WHEN " . $pointCount['userId'] . " THEN " . $pointCount['points'] . " ";
			}
		}

		// ** User Followers No
		$userFollowerRepo = $em->getRepository('RasenNineGagBundle:UserFollower');
		$userFollowersCountQ = $userFollowerRepo->createQueryBuilder('uf')
			->select('u.id AS userId, COUNT(uf.id) as followersCount')
			->leftJoin('uf.user', 'u')
			->where('u.deletedAt is NULL')
			->groupBy('uf.user')
			->getQuery();
		;
		$userFollowersCount = $userFollowersCountQ->getScalarResult();;
		$userFollowersCaseQ = "";
		foreach ($userFollowersCount as $userFollowerCount) {
			if ($userFollowerCount['userId'] !== null) {
				$userFollowersCaseQ .= "WHEN " . $userFollowerCount['userId'] . " THEN " . $userFollowerCount['followersCount'] . " ";
			}
		}

		// ** User Following No
		$userFollowingsCountQ = $userFollowerRepo->createQueryBuilder('uf')
			->select('u.id AS userId, COUNT(uf.id) as followingCount')
			->leftJoin('uf.follower', 'u')
			->where('u.deletedAt is NULL')
			->groupBy('uf.follower')
			->getQuery();
		;
		$userFollowingsCount = $userFollowingsCountQ->getScalarResult();;
		$userFollowingsCaseQ = "";
		foreach ($userFollowingsCount as $userFollowingCount) {
			if ($userFollowingCount['userId'] !== null) {
				$userFollowingsCaseQ .= "WHEN " . $userFollowingCount['userId'] . " THEN " . $userFollowingCount['followingCount'] . " ";
			}
		}

		// ** User Reports No
		$userReportRepo = $em->getRepository('RasenNineGagBundle:UserReport');
		$userReportsCountQ = $userReportRepo->createQueryBuilder('ur')
			->select('u.id AS userId, COUNT(ur.id) as reportCount')
			->leftJoin('ur.reporter', 'u')
			->groupBy('ur.reporter')
			->getQuery();
		;
		$userReportsCount = $userReportsCountQ->getScalarResult();;
		$userReportsCaseQ = "";
		foreach ($userReportsCount as $userReportCount) {
			if ($userReportCount['userId'] !== null) {
				$userReportsCaseQ .= "WHEN " . $userReportCount['userId'] . " THEN " . $userReportCount['reportCount'] . " ";
			}
		}

		// ** Post UpVotesNo

		$rsm = new ResultSetMapping($em);
		$rsm->addScalarResult('userId','userId');
		$rsm->addScalarResult('upVotesNo','upVotesNo');

		$postsVotesCountDQL = "SELECT pv.voted_by as userId, COUNT(IF(pv.vote = 1, 1, null)) as upVotesNo ".
		                      "FROM posts_votes pv ".
		                      "GROUP BY pv.voted_by "
		;
		$postsVotesCountQ = $em->createNativeQuery($postsVotesCountDQL, $rsm)
		;
		$postsVotesCount = $postsVotesCountQ->getScalarResult();;
		$postsUpVotesCaseQ = "";
		foreach ($postsVotesCount as $postVotesCount) {
			if ($postVotesCount['userId'] !== null) {
				$postsUpVotesCaseQ .= "WHEN " . $postVotesCount['userId'] . " THEN " . $postVotesCount['upVotesNo'] . " ";
			}
		}

		if ($usersCommentsCaseQ !== "" ||
		    $usersApprovedCommentsCaseQ !== "" ||
		    $postsCaseQ !== "" ||
		    $approvedPostsCaseQ !== "" ||
		    $pointsCaseQ !== "" ||
		    $userFollowersCaseQ !== "" ||
		    $userFollowingsCaseQ !== ""||
		    $userReportsCaseQ !== ""||
		    $postsUpVotesCaseQ !== "")
		{
			$userUpdateQuery = "UPDATE RasenNineGagBundle:User u SET ";
			if ($usersCommentsCaseQ !== "") {
				$userUpdateQuery .=
					"u.commentsNo = CASE u.id ".
					$usersCommentsCaseQ .
					" ELSE 0 END "
				;
				if ($usersApprovedCommentsCaseQ !== "" || $postsCaseQ !== "" || $approvedPostsCaseQ !== "" || $pointsCaseQ !== "" || $userFollowersCaseQ !== "" || $userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($usersApprovedCommentsCaseQ !== "") {
				$userUpdateQuery .=
					"u.approvedCommentsNo = CASE u.id ".
					$usersApprovedCommentsCaseQ .
					" ELSE 0 END "
				;
				if ($postsCaseQ !== "" || $approvedPostsCaseQ !== "" || $pointsCaseQ !== "" || $userFollowersCaseQ !== "" || $userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($postsCaseQ !== "") {
				$userUpdateQuery .=
					"u.postsNo = CASE u.id ".
					$postsCaseQ .
					" ELSE 0 END "
				;
				if ( $approvedPostsCaseQ !== "" || $pointsCaseQ !== "" || $userFollowersCaseQ !== "" || $userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($approvedPostsCaseQ !== "") {
				$userUpdateQuery .=
					"u.approvedPostsNo = CASE u.id ".
					$approvedPostsCaseQ .
					" ELSE 0 END "
				;
				if ($pointsCaseQ !== "" || $userFollowersCaseQ !== "" || $userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($pointsCaseQ !== "") {
				$userUpdateQuery .=
					"u.pointsNo = CASE u.id ".
					$pointsCaseQ .
					" ELSE 0 END "
				;
				if ($userFollowersCaseQ !== "" || $userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($userFollowersCaseQ !== "") {
				$userUpdateQuery .=
					"u.followersNo = CASE u.id ".
					$userFollowersCaseQ .
					" ELSE 0 END "
				;
				if ($userFollowingsCaseQ !== "" || $userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($userFollowingsCaseQ !== "") {
				$userUpdateQuery .=
					"u.followingNo = CASE u.id ".
					$userFollowingsCaseQ .
					" ELSE 0 END"
				;
				if ($userReportsCaseQ !== "" || $postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($userReportsCaseQ !== "") {
				$userUpdateQuery .=
					"u.reportsNo = CASE u.id ".
					$userReportsCaseQ .
					" ELSE 0 END"
				;
				if ($postsUpVotesCaseQ !== "") $userUpdateQuery .= ' , ';
			}
			if ($postsUpVotesCaseQ !== "") {
				$userUpdateQuery .=
					"u.upvotesNo = CASE u.id ".
					$postsUpVotesCaseQ .
					" ELSE 0 END"
				;
			}
			$updateUsersQ = $em->createQuery($userUpdateQuery);
			$updateUsers = $updateUsersQ->execute();
			return $updateUsers;
		}
		return false;
	}
} 