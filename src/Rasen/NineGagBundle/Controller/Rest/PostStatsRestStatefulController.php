<?php

namespace Rasen\NineGagBundle\Controller\Rest;

use JMS\Serializer\SerializationContext;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostCategory;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\PostText;
use Rasen\NineGagBundle\Entity\PostVote;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest,
	FOS\RestBundle\Request\ParamFetcherInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
/**
 * Class PostStatsRestStatefulController
 * @package Rasen\NineGagBundle\Controller\Rest
 */
class PostStatsRestStatefulController extends FOSRestController
{
	/**
     * DISABLED (TO ENABLE ADD Action TO METHOD NAME AND SPECIFY ROUTE)
     *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function postViewCount(Request $request)
	{
		if (!$request->request->has('ids')) {
			throw new BadRequestHttpException();
		}
		$postHashIds = $request->request->get('ids');
		$postHashIds = trim($postHashIds);
		$postHashIds = trim($postHashIds, ',');
		$postHashIds = explode(',', $postHashIds);
		$cookie = null;

	    $cookie = $this->container->get('rasen_ninegag.post_stats_utility')->increaseView($postHashIds, $this->getUser());

		$okView = $this->view('ok', 200);

		$response = $this->handleView($okView);
		if ($cookie instanceof Cookie) {
			$response->headers->setCookie($cookie);
		}

		return $response;
	}

	/**
     *
	 * @param Request $request
	 * @param $hashId
	 *
     * @Rest\Post("/posts/{hashId}/downloads")
     * @Rest\RequestParam(name="hashId", description="Post hash id")
     *
	 * @return Response
	 */
	public function postDownloadCountAction(Request $request, $hashId)
	{
        $post = $this->getPostObject($hashId);

	    $cookie = $this->container->get('rasen_ninegag.post_stats_utility')->increaseDownload($post);

		$okView = $this->view('ok', 200);

		$response = $this->handleView($okView);
		if ($cookie instanceof Cookie) {
			$response->headers->setCookie($cookie);
		}

		return $response;
	}

	/**
     *
	 * @param Request $request
	 * @param $hashId
	 *
     * @Rest\Post("/posts/{hashId}/shares")
     * @Rest\RequestParam(name="hashId", description="Post hash id")
     *
	 * @return Response
	 */
	public function postShareCountAction(Request $request, $hashId)
	{
        $post = $this->getPostObject($hashId);

	    $cookie = $this->container->get('rasen_ninegag.post_stats_utility')->increaseShare($post);

		$okView = $this->view('ok', 200);

		$response = $this->handleView($okView);
		if ($cookie instanceof Cookie) {
			$response->headers->setCookie($cookie);
		}

		return $response;
	}

	/**
	 * Returns post
	 *
	 * @param $hashId
	 *
	 * @return Post
	 */
	private function getPostObject($hashId)
	{
		$id = $this->container->get('hashids')->decode($hashId);
		/**
		 * @var Post $post
		 */
		$post = $this->getDoctrine()->getRepository('RasenNineGagBundle:Post')->findOneBy(array(
			'id' => $id
		));
		if (!$post) {
			throw new NotFoundHttpException();
		}
		return $post;
	}
}
