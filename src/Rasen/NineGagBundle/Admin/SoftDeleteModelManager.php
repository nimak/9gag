<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/13/2015
 * Time: 9:32 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class SoftDeleteModelManager
 *
 * @DI\Service("rasen_ninegag.admin.soft_delete_model_manager")
 *
 * @package Rasen\NineGagBundle\Admin
 */
class SoftDeleteModelManager extends ModelManager
{
    /**
     * {@inheritdoc}
     * @DI\InjectParams({
     *     "registry" = @DI\Inject("doctrine")
     * })
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
        $this->registry->getManager()->getFilters()->disable('softdeleteable');
    }
}