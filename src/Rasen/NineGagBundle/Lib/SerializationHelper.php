<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/19/2015
 * Time: 1:56 PM
 */

namespace Rasen\NineGagBundle\Lib;
use Doctrine\ORM\EntityManager;
use Hashids\Hashids;
use Hateoas\Representation\CollectionRepresentation;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\UserBadge;
use Rasen\NineGagBundle\Hateoas\Representation\OffsetPaginatedRepresentation;
use Symfony\Component\Security\Core\SecurityContext;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SerializationHelper
 *
 * @DI\Service("rasen_ninegag.serialization_helper")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class SerializationHelper {


    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     * @var Hashids
     */
    private $hashids;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *     "securityContext" = @DI\Inject("security.context"),
     *     "hashids" = @DI\Inject("hashids")
     * })
     * @param EntityManager $em
     * @param SecurityContext $securityContext
     * @param Hashids $hashids
     */
    public function __construct(EntityManager $em,
                                SecurityContext $securityContext,
                                Hashids $hashids)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
        $this->hashids = $hashids;
    }

    /**
     * Checks whether the current logged in user can follow the given user or not
     * @param $user
     * @return boolean
     */
    public function canFollow(User $user)
    {
        $currentUser = $this->securityContext->getToken()->getUser();
        if ($user === $currentUser) return false;
        $userFollower = $this->em->getRepository('RasenNineGagBundle:UserFollower')->findBy(array(
            'user' => $user,
            'follower' => $currentUser
        ));
        return !(!empty($userFollower) && $userFollower[0] instanceof UserFollower);
    }

    /**
     * Checks whether the current logged in user can follow the given user or not
     * @param $user
     * @return boolean
     */
    public function canUnfollow(User $user)
    {
        $currentUser = $this->securityContext->getToken()->getUser();
        if ($user === $currentUser) return false;
        return !$this->canFollow($user);
    }

    /**
     * @param Post $post
     * @return array
     */
    public function getCommentsOverview(Post $post)
    {
        $offset = 0;
        $limit = $post->getEmbeddedCommentsLimit();

        $postHashId = $this->hashids->encode($post->getId());

        $currentUser = $this->securityContext->getToken()->getUser();
        $commentRepo = $this->em->getRepository("RasenNineGagBundle:Comment");

        $results = $commentRepo->findAllApprovedByPost($post, $currentUser, $limit, $offset);

        if (count($results['items']) < 1) return array();

        $collectionRepresentation = new CollectionRepresentation(
            $results['items'],
            'comments'
        );
        $offsetPaginatedRepresentation = new OffsetPaginatedRepresentation(
            $collectionRepresentation,
            'ajax_comment_get_post_comments',
            array('postId'=>$postHashId, 'offset'=>$offset, 'limit'=>$limit),
            $offset,
            $results['offset'],
            $limit,
            $results['totalCount']
        );

        return $offsetPaginatedRepresentation;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserBadges(User $user)
    {
        $userBadges = $this->em->getRepository('RasenNineGagBundle:UserBadge')->findBy(array(
            'user' => $user,
            'isClaimed' => true
        ));

        $badges = array();
        foreach ($userBadges as $userBadge) {
            $badges[] = $userBadge->getBadge();
        }
        return $badges;
    }
}