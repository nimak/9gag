/usr/local/openresty/nginx/conf/h5bp:
  file.recurse:
    - source: salt://h5bp-nginx-configs/files/
    - user: root
    - group: root
/usr/local/openresty/nginx/conf/mime.types:
  file.managed:
    - source: salt://h5bp-nginx-configs/files/mime.types
    - user: root
    - group: root
/usr/share/nginx/logs:
  file.directory:
    - user: root
    - group: root