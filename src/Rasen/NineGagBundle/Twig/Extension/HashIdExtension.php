<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/14/14
 * Time: 11:52 AM
 */

namespace Rasen\NineGagBundle\Twig\Extension;


use Hashids\Hashids;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class HashIdExtension
 *
 * @DI\Service("rasen_ninegag.twig.hashids")
 * @DI\Tag("twig.extension")
 *
 * @package Rasen\NineGagBundle\Twig\Extension
 */
class HashIdExtension extends \Twig_Extension
{
	/**
	 * @var Hashids
	 */
	private $hashids;

	/**
	 * Constructor.
	 *
	 * @DI\InjectParams({
	 *     "hashids" = @DI\Inject("hashids")
	 * })
	 *
	 * @param Hashids $hashids
	 */
	public function __construct(Hashids $hashids)
	{
		$this->hashids = $hashids;
	}

	/**
	 * Returns a list of filters.
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('hashid', array($this, 'hashid')),
		);
	}

	/**
	 * Creates hashes from numeric ids
	 *
	 * @param $id
	 *
	 * @return string
	 */
	public function hashid($id) {
		return $this->hashids->encode($id);
	}

	/**
	 * Name of this extension
	 *
	 * @return string
	 */
	public function getName()
	{
		return 'rasen_ninegag.hash_id';
	}
} 