<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Rasen\NineGagBundle\Validator\Constraints as RasenAssert;

/**
 * UserFollower
 *
 * @ORM\Table(name="users_followers", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_follower_id_UNIQUE", columns={"user_id", "follower_id"})}, indexes={@ORM\Index(name="users_followers_user_id_idx", columns={"user_id"}), @ORM\Index(name="users_followers_follower_id_idx", columns={"follower_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"user", "follower"},
 *     errorPath="follower",
 *     message="user.follower.unique"
 * )
 *
 * @RasenAssert\IsUserFollowerNotTheSame(message = "user.user_follower_not_the_same")
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class UserFollower
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="followed_at", type="datetime", nullable=true)
     */
    private $followedAt;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="follower_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $follower;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set followedAt
     *
     * @param \DateTime $followedAt
     * @return UserFollower
     */
    public function setFollowedAt($followedAt)
    {
        $this->followedAt = $followedAt;

        return $this;
    }

    /**
     * Get followedAt
     *
     * @return \DateTime 
     */
    public function getFollowedAt()
    {
        return $this->followedAt;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return UserFollower
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set follower
     *
     * @param \Rasen\NineGagBundle\Entity\User $follower
     * @return UserFollower
     */
    public function setFollower(\Rasen\NineGagBundle\Entity\User $follower = null)
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * Get follower
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getFollower()
    {
        return $this->follower;
    }
}
