<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 11:32 AM
 */

namespace Rasen\NineGagBundle\Lib;

use \Rasen\NineGagBundle\Entity\User;
use \Rasen\NineGagBundle\Entity\Post;
use \Rasen\NineGagBundle\Entity\PostVote;
use \Rasen\NineGagBundle\Entity\PostImage;
use \Rasen\NineGagBundle\Entity\PostText;
use \Rasen\NineGagBundle\Entity\Comment;
use \Rasen\NineGagBundle\Entity\CommentVote;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class BadgeUtility
 *
 *
 * @DI\Service("rasen_ninegag.badge_utility")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class BadgeUtility
{

	/**
	 * @var \Doctrine\Common\Persistence\ObjectManager
	 *
	 * @DI\Inject("doctrine.orm.entity_manager")
	 */
	private $em;

	public function hasBadgeConditions(User $user)
	{

	}

	public function hasMostCommentDaily(User $user)
	{
		$dateFrom = new \DateTime('now');
		$dateTo = new \DateTime('now');
		$dateFrom = $dateFrom->setTime(0, 0, 0);
		$dateTo = $dateTo->setTime(23, 59, 59);

		$commentsTodayCount = $this->em->getRepository('RasenNineGagBundle:Comment')->findCommentsCreatedByCount($user->getId(), $dateFrom, $dateTo);

		if ($commentsTodayCount > 20) {
			return true;
		}
		return false;
	}

	public function hasPopularCommentDaily(User $user)
	{
		$dateFrom = new \DateTime('now');
		$dateTo = new \DateTime('now');
		$dateFrom = $dateFrom->setTime(0, 0, 0);
		$dateTo = $dateTo->setTime(23, 59, 59);

		$repo = $this->em->getRepository('RasenNineGagBundle:Comment');

		/**
		 * @var \Doctrine\ORM\QueryBuilder
		 */
		$q = $repo->createQueryBuilder('c');

	}
} 