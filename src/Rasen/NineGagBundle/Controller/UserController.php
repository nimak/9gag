<?php

namespace Rasen\NineGagBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package Rasen\NineGagBundle\Controller
 */
class UserController extends Controller
{
	/**
	 * @param $username
	 *
	 * @Route("/{username}/")
	 * @Route("/{username}/posts")
	 * @Route("/{username}/upvotes")
	 * @Route("/{username}/comments")
	 * @Route("/{username}/followers")
	 * @Route("/{username}/followings")
	 * @Route("/{username}/stream")
	 *
	 * @return Response
	 */
    public function profileAction($username)
    {
        $user = $this->getDoctrine()->getRepository("RasenNineGagBundle:User")->findOneBy(array("username" => $username ));
	    if (!$user) {
		    throw new NotFoundHttpException();
	    }

	    $isUserFollowed = $this->getDoctrine()->getRepository("RasenNineGagBundle:UserFollower")->findBy(array(
		    'user' => $user,
		    'follower' => $this->getUser()
	    ));

        $badges = $this->container->get('rasen_ninegag.serialization_helper')->getUserBadges($user);

        return $this->render('RasenNineGagBundle:User:profile.html.twig', array(
	        "user" =>$user,
	        "isUserFollowed" =>$isUserFollowed,
            'badges'=>$badges
        ));
    }

	/**
	 * @param $username
	 *
	 * @Route("/{username}/badges")
	 *
	 * @return Response
	 */
    public function viewBadgesAction($username)
    {
        return new Response();
    }

	/**
	 * @param $username
	 *
	 * @Route("/{username}/points")
	 *
	 * @return Response
	 */
    public function viewPointsAction($username)
    {
        return new Response();
    }


}
