var Q = require('q');
var path = require('path');
var extend = require('extend');
var context = require('rabbit.js').createContext('amqp://localhost:5672');
var fs = require('fs-extra');
var glob = require("glob");
var redis = require("redis");

//Set ImageMagick environment variables
process.env['MAGICK_THREAD_LIMIT'] = 1;
process.env['MAGICK_THREADS'] = 1;
process.env['MAGICK_THROTTLE'] = 1;
process.env['OMP_NUM_THREADS'] = 1;

var port = process.argv[2] || 8081;

var THUMBS_DIR_NAME = 'thumbs';
var VIDEOS_DIR_NAME = 'videos';
var THUMB_500_SUFFIX = '_500';
var THUMB_700_SUFFIX = '_700';

var STATUS_NOT_PROCESSED = '0';
var STATUS_PROCESSING = '1';
var STATUS_PROCESSED = '2';

context.on('ready', function() {
    var gm = require('gm');
    var imageMagick = gm.subClass({ imageMagick: true });
    var ffmpeg = require('fluent-ffmpeg');

    redisClient = redis.createClient();

    redisClient.on("error", function (err) {
        console.log("Error " + err);
    });


    var push = context.socket('PUSH', {routing: 'direct', persistent: true});

    var worker = context.socket('WORKER', {routing: 'direct', persistent: true, prefetch: 1});
    worker.setEncoding('utf8');

    var sendFileInfo = function(fileInfo) {
    };

    var setFileStatus = function(filePath, status) {
        var deferred = Q.defer();

        var timeout = 60 * 60 * 24; //1 Day
        var key = filePath+':status';
        redisClient.set(key, status, function(err, reply) {
            redisClient.expire(key, timeout);
            deferred.resolve();
        });

        return deferred.promise;
    };
    // Connect to post.info push socket to update post info in database
    push.connect('post.info.update', function() {
        console.log('push connected!');

        // Connect to the main worker queue to get jobs
        worker.on('data', function(data) {
            var message = JSON.parse(data);
            console.log(message);

            if (message.type == 'process') {
                var filePath = path.normalize(message.filePath);
                var postId = message.postId;
                redisClient.get(message.filePath+':status', function(err, reply) {
                    console.log(reply);
                    if (reply === STATUS_NOT_PROCESSED || reply == null) {
                        setFileStatus(message.filePath, STATUS_PROCESSING).then(function(){
                            var saveToPath = path.normalize(message.saveToPath);
                            fs.exists(filePath, function (exists) {
                                if (exists) {
                                    processFile(filePath, saveToPath).then(function(fileInfo){
                                        fileInfo.postId = postId;

                                        // Send file's info using push socket!
                                        push.write(JSON.stringify(fileInfo));

                                        // Set file's status to PROCESSED if the processing was successful
                                        setFileStatus(message.filePath, STATUS_PROCESSED);
                                        worker.ack();
                                    }, function(){
                                        /*
                                         Set file's status to NOT_PROCESSED if there's been a failure in processing
                                         and re-queue the job
                                        */
                                        setFileStatus(message.filePath, STATUS_NOT_PROCESSED);
                                        worker.requeue();
                                    });
                                } else {
                                    // Discard the job if the file does not exist!
                                    setFileStatus(message.filePath, STATUS_PROCESSED);
                                    worker.discard();
                                }
                            });
                        });
                    } else {
                        // Discard the message if it has been processed before OR is being processed by another worker
                        worker.discard();
                    }
                });

            } else if (message.type == 'purge') {
                var fileName = message.fileName;
                var cachePath = path.normalize(message.cachePath);
                removeProcessedFiles(cachePath, fileName).then(function(){
                    worker.ack();
                }, function(){
                    worker.requeue();
                });
            }
        });
    });

    var processFile = function(filePath, saveToPath)
    {
        var deferred = Q.defer();

        var fileName = path.basename(filePath).replace(path.extname(filePath), "");

        var imageFilePath = filePath;
        if (path.extname(filePath).toLowerCase() == '.gif') {
            imageFilePath = filePath+'[0]';
        }
        var file = imageMagick(imageFilePath);
        var origFile = imageMagick(filePath);

        var fileInfo = null;
        getFileInfo(origFile).then(function(fileData) {
            fileInfo = fileData;
            var thumbsSaveToPath = path.join(saveToPath, THUMBS_DIR_NAME);
            fs.ensureDir(thumbsSaveToPath, function (err) {
                var thumbsSaveToFilePathBase = path.join(thumbsSaveToPath, fileName);
                generateFeatured(file, thumbsSaveToFilePathBase).then(function(file){
                    generateThumbs(file,thumbsSaveToFilePathBase).then(function(file){
                        if (isAnimatedGif(fileInfo)) {
                            console.log('animated');
                            var videosSaveToPath = path.join(saveToPath, VIDEOS_DIR_NAME);
                            fs.ensureDir(videosSaveToPath, function (err) {
                                if (err) {
                                    console.error(err)
                                } else {
                                    var videosSaveToFilePathBase = path.join(videosSaveToPath, fileName);
                                    convertGifToMp4(filePath, videosSaveToFilePathBase).then(function(convertedFilePath){
                                        convertGifToWebm(convertedFilePath, videosSaveToFilePathBase).then(function() {
                                            console.log('done');
                                            deferred.resolve(fileInfo);
                                        }, function(){
                                            deferred.reject();
                                        });
                                    }, function(){
                                        deferred.reject();
                                    });
                                }
                            });
                        } else {
                            deferred.resolve(fileInfo);
                        }
                    }, function(){
                        deferred.reject();
                    });
                }, function(){
                    deferred.reject();
                });
            });
        }, function(){
            deferred.reject();
        });
        return deferred.promise;
    };

    worker.connect('image_processor', function() {
        console.log('connected!');
    });

    var isAnimatedGif = function(fileInfo) {
        console.log(fileInfo.frames);
        return (fileInfo.format.toLowerCase() == 'gif' && fileInfo.frames > 1);
    };

    var getFileInfo = function(file)
    {
        var deferred = Q.defer();
        file.identify('{\"frames\": %n, \"height\": %h, \"width\": %w, \"size\": \"%b\", \"format\": \"%m\"}', function(err, value) {
            var regex = "{([^}]*)}";
            var matches = value.match(regex);
            if (matches.length > 0) {
                deferred.resolve(JSON.parse(matches[0]));
            } else {
                deferred.reject();
            }
        });
        return deferred.promise;
    };

    var generateFeatured = function (file, thumbsSaveToFilePathBase) {
        var deferred = Q.defer();
        //featured Thumb
        var fileExt = '.jpg';
        var saveTo = thumbsSaveToFilePathBase + '_feature' + fileExt;

        fs.exists(saveTo, function(exists) {
            if (exists) {
                deferred.resolve(file);
            } else {
                file.thumb(300, 120, saveTo, 75, function(err){
                    if (!err) {
                        deferred.resolve(file);
                    } else {
                        deferred.reject(err);
                    }
                });
            }
        });
        return deferred.promise;
    };

    var generateThumb = function(file, thumbsSaveToFilePathBase, width, height, quality)
    {
        var deferred = Q.defer();

        var newFile = extend({}, file);
        var fileExt = '.jpg';
        var saveTo = thumbsSaveToFilePathBase + '_' + width + fileExt;

        fs.exists(saveTo, function(exists) {
            if (exists) {
                deferred.resolve(file);
            } else {
                newFile
                    .noProfile()
                    .quality(quality)
                    .resize(width, height)
                    .write(saveTo, function(err){
                        if (!err) {
                            var thumbFile = imageMagick(saveTo);
                            placeWatermark(thumbFile).then(function(file) {
                                file.write(saveTo, function(err){
                                    console.log('-> '+saveTo );
                                    deferred.resolve(file);
                                });
                            });
                        } else {
                            deferred.reject(err);
                        }
                    });
            }
        });


        return deferred.promise;
    };

    var generateThumbs = function(file, thumbsSaveToFilePathBase)
    {
        var deferred = Q.defer();
        //700px Thumb
        generateThumb(file, thumbsSaveToFilePathBase, 700, null, 75)
            .then(function(){
                //500px Thumb
                generateThumb(file, thumbsSaveToFilePathBase, 500, null, 75)
                    .then(function(file){
                        deferred.resolve(file);
                    });
            });
        return deferred.promise;
    };

    var placeWatermark = function(file)
    {
        var deferred = Q.defer();

        var watermarkFilePath = path.resolve(__dirname, './watermark_700.png');
        var watermarkImage = imageMagick(watermarkFilePath);
        file.size(function(err, origSize){
            var origWidth = origSize.width;
            var origHeight = origSize.height;

            watermarkImage.size(function(err, size){
                var wOrigWidth = 97;
                var wOrigHeight = 32;
                if (typeof size !== "undefined") {
                    wOrigWidth = size.width;
                    wOrigHeight = size.height;
                }
                /*var wRatio = wOrigHeight/wOrigWidth;
                var wWidth = origWidth * scale;
                var wHeight = wWidth * wRatio;*/

                var wX = 10;
                var wY = (origHeight - wOrigHeight) - 10;
                file.draw('image Over ' + wX + ',' + wY + ' ' + wOrigWidth + ',' + wOrigHeight + ' \"' + watermarkFilePath + '\"');
                deferred.resolve(file);
            });
        });

        return deferred.promise;
    };

    var convertGifToMp4 = function(gifPath, videosSaveTofilePathBase){
        var deferred = Q.defer();
        console.log(gifPath);
        var mp4Path = videosSaveTofilePathBase + '.mp4';
        fs.exists(mp4Path, function(exists) {
            if (exists) {
                deferred.resolve(mp4Path);
            } else {
                var proc = new ffmpeg({ source: gifPath, nolog: true, niceness: 10 });
                //proc.setFfmpegPath("C:\\Windows\\System32\\ffmpeg\\bin\\ffmpeg.exe");
                proc
                    .input(path.resolve(__dirname, './watermark_500.png'))
                    .withNoAudio()
                    .videoCodec('libx264')
                    .format('mp4')
                    .videoBitrate(500)
                    .addOption('-crf 25')
                    .addOptions(['-cpu-used 1', '-threads 1'])
                    .outputOptions(['-preset slow', '-movflags +faststart'])
                    .complexFilter([
                        {
                            filter: 'scale', options: { w: '500', h: 'trunc(ow/a/2)*2' },
                            outputs: 'scaled'
                        },
                        {
                            filter: 'overlay', options: { x: '10', y: '(H-h) - 10' },
                            inputs: ['scaled']
                        }
                    ])
                    .on('progress', function(progress) {
                        console.log('Processing mp4: ' + progress.percent + '% done');
                    })
                    .on('end', function() {
                        console.log('file has been converted successfully');
                        deferred.resolve(mp4Path);
                    })
                    .on('error', function(err) {
                        console.log('an error happened: ' + err.message);
                        deferred.reject(err);
                    })
                    .saveToFile(mp4Path);
            }
        });


        return deferred.promise;

    };

    var convertGifToWebm = function(filePath, videosSaveTofilePathBase){
        var deferred = Q.defer();
        console.log(filePath);
        var webmPath = videosSaveTofilePathBase + '.webm';
        fs.exists(webmPath, function(exists) {
            if (exists) {
                deferred.resolve(webmPath);
            } else {
                var proc = new ffmpeg({ source: filePath, nolog: true, niceness: 10 });
                //proc.setFfmpegPath("C:\\Windows\\System32\\ffmpeg\\bin\\ffmpeg.exe");
                try {
                proc
                    .withNoAudio()
                    .videoCodec('libvpx')
                    .format('webm')
                    .videoBitrate(500)
                    .addOption('-crf 25')
                    .addOptions(['-quality good', '-cpu-used 1', '-qmin 10', '-qmax 25', '-threads 1'])
                    .on('progress', function(progress) {
                        console.log('Processing webm: ' + progress.percent + '% done');
                    })
                    .on('end', function() {
                        console.log('file has been converted successfully');
                        deferred.resolve(webmPath);
                    })
                    .on('error', function(err) {
                        console.log('an error happened: ' + err.message);
                        deferred.reject(err);
                    })
                    .saveToFile(webmPath);
                } catch(e) {
                    deferred.reject(e);
                    console.log('ERROR!');
                }
            }
        });


        return deferred.promise;

    };

    var removeFile = function(filePath) {
        var deferred = Q.defer();
        console.log('removing ' + filePath);
        fs.remove(filePath, function (err) {
            if (err) {
                deferred.reject(err);
                throw err;
            }
            console.log('successfully deleted ' + filePath);
            deferred.resolve();
        });
        return deferred.promise;
    };
    var removeProcessedFiles = function(postCachePath, fileName) {
        var deferred = Q.defer();

        var cacheDir = path.normalize(postCachePath);
        var videosPath = path.join(cacheDir, VIDEOS_DIR_NAME);
        var thumbsPath = path.join(cacheDir, THUMBS_DIR_NAME);
        console.log('finding files in  ' + thumbsPath);
        glob(fileName+'*', {cwd: thumbsPath, nodir: true}, function (err, files) {
            console.log('begin remove thumbs..');
            if (!err) {
                if (files.length > 0) {
                    console.log('found '+files.length+' thumbs!');
                    files.forEach(function (file) {
                        removeFile(path.join(thumbsPath, file));
                    });
                } else {
                    console.log('no thumbs found!');
                }
                deferred.resolve();
            } else {
                console.log(err);
                deferred.reject(err);
            }

            console.log('finding files in  ' + videosPath);
            glob(fileName+'*', {cwd: videosPath, nodir: true}, function (err, files) {
                console.log('begin remove videos..');
                if (!err) {
                    if (files.length > 0) {
                        console.log('found '+files.length+' videos!');
                        files.forEach(function(file) {
                            removeFile(path.join(videosPath, file));
                        });
                    } else {
                        console.log('no videos found!');
                    }
                    deferred.resolve();
                } else {
                    console.log(err);
                    deferred.reject(err);
                }
            });
        });
        return deferred.promise;
    };
});