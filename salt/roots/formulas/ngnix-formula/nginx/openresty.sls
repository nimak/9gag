{% set nginx = pillar.get('nginx', {}) -%}
{% set home = nginx.get('home', '/var/www') -%}
{% set source = nginx.get('source_root', '/usr/local/src') -%}

{% set openresty = nginx.get('openresty', {}) -%}
{% set openresty_version = openresty.get('version', '1.7.7.2') -%}
{% set openresty_checksum = openresty.get('checksum', 'sha1=837f1d740121ba22db8f9cbae0d142ce7a498c3a') -%}
{% set openresty_package = source + '/openresty-' + openresty_version + '.tar.gz' -%}

get-openresty:
  file.managed:
    - name: {{ openresty_package }}
    - source: http://openresty.org/download/ngx_openresty-{{ openresty_version }}.tar.gz
    - source_hash: {{ openresty_checksum }}
  cmd.wait:
    - cwd: {{ source }}
    - name: tar -zxvf {{ openresty_package }}
    - watch:
      - file: get-openresty

install_openresty:
  cmd.wait:
    - cwd: {{ source }}/ngx_openresty-{{ openresty_version }}
    - names: 
      - ./configure --with-luajit
      - make && make install
    - watch:
      - cmd: get-openresty
