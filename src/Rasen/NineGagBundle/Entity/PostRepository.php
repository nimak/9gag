<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/10/2014
 * Time: 2:22 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class PostRepository
 * @package Rasen\NineGagBundle\Entity
 */
class PostRepository extends EntityRepository
{
	public function findAllByUserQ($user, $onlyApproved = true)
	{
		/*$dql = "SELECT p ".
		       "FROM RasenNineGagBundle:Post p ".
		       "JOIN p.createdBy u ".
		       "LEFT JOIN RasenNineGagBundle:PostVote pv WITH (pv.post = p AND pv.votedBy = u) ".
		       "LEFT JOIN RasenNineGagBundle:PostReport pr WITH (pr.post = p AND pr.reporter = u) ".
		       "WHERE p.createdBy = :user ".
		       "ORDER BY p.createdTime DESC "
		;
		$q = $this->getEntityManager()->createQuery($dql);
		$q =$q->setParameter(':user', $user);*/

		$q = $this->createQueryBuilder('p')
		          ->select('p')
		          ->where('p.createdBy = :user');
		if ($onlyApproved) $q = $q->andWhere('p.isApproved = true');
		$q = $q->orderBy('p.createdTime', 'DESC')
		       ->setParameter(':user', $user);
		return $q->getQuery();
	}

	public function findAllByUser($user, $onlyApproved = true)
	{
		$q = $this->findAllByUserQ($user, $onlyApproved);
		return $q->getResult();
	}

	public function findVotedByUserQ($user, $vote)
	{
		$q = $this->getEntityManager()->createQueryBuilder('p');
		$q->select('p')
			->add('from', 'RasenNineGagBundle:Post p INNER JOIN p.votes pv')
		  //->innerJoin('RasenNineGagBundle:PostVote', 'pv', Expr\Join::WITH, $q->expr()->eq('p', 'pv.post'))
		  ->where('pv.votedBy = :user')
		  ->andWhere('pv.vote = :vote')
		  ->orderBy('pv.votedTime', 'DESC')
		  ->setParameter(':user', $user)
		  ->setParameter(':vote', $vote)
			;
		return $q->getQuery();
	}

	public function findVotedByUser($user, $vote)
	{
		$q = $this->findVotedByUserQ($user, $vote);
		return $q->getResult();
	}

	public function findCommentedByUserQ($user)
	{
		$q = $this->createQueryBuilder('p');
		$q->select('p')
			->add('from', 'RasenNineGagBundle:Post p INNER JOIN p.comments c')
		  //->innerJoin('RasenNineGagBundle:Comment', 'c', Expr\Join::WITH, $q->expr()->eq('p', 'c.post'))
		  ->where('c.createdBy = :user')
		  ->orderBy('c.createdTime', 'DESC')
		  ->setParameter(':user', $user);
		return $q->getQuery();
	}

	public function findCommentedByUser($user)
	{
		$q = $this->findCommentedByUserQ($user);
		return $q->getResult();
	}

	public function findAllFollowingQ($user)
	{
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		  ->leftJoin('RasenNineGagBundle:UserFollower', 'uf', Expr\Join::WITH, $q->expr()->eq('p.createdBy', 'uf.user'))
		  ->where('uf.follower = :user')
		  ->andWhere('p.createdBy = uf.user')
		  ->andWhere('p.isApproved = true')
		  ->andWhere('p.nsfw = false')
		  ->orderBy('p.createdTime', 'DESC')
		  ->setParameter(':user', $user)
		;
		return $q->getQuery();
	}

	public function findAllFollowing($user)
	{
		$q = $this->findAllFollowingQ($user);
		return $q->getResult();
	}

	public function findTopFeaturedQ(){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.featured=true')
		       ->orderBy('p.createdTime','DESC')
		       ->setMaxResults(10)
		       ;
		return $q->getQuery();
	}
	public function findTopFeatured(){
		$q = $this->findTopFeaturedQ();
		return $q->getResult();
	}

	public function findHotPostsQ(){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.upvotesNo','DESC')
		       ->addOrderBy('p.createdTime','DESC')
		       ;
		return $q->getQuery();
	}
	public function findHotPosts(){
		$q = $this->findHotPostsQ();
		return $q->getResult();
	}

	public function findTrendingPostsQ(){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.upvotesNo','DESC')
		       ->addOrderBy('p.createdTime','DESC')
		       ;
		return $q->getQuery();
	}
	public function findTrendingPosts(){
		$q = $this->findTrendingPostsQ();
		return $q->getResult();
	}

	public function findFreshPostsQ(){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.createdTime','DESC')
		       ;
		return $q->getQuery();

        /*$dql = "SELECT p, u, pv, pr ".
            "FROM RasenNineGagBundle:Post p ".
            "LEFT JOIN p.createdBy u ".
            "LEFT JOIN RasenNineGagBundle:PostVote pv WITH pv.post = p AND pv.votedBy = :user ".
            "LEFT JOIN RasenNineGagBundle:PostReport pr WITH pr.post = p AND pr.reporter = :user ".
            "WHERE p.isApproved=true AND p.nsfw = false ".
            "ORDER BY p.id DESC "
        ;
        $q = $this->getEntityManager()->createQuery($dql);
        $q = $q->setParameter('user', $user);

        return $q;*/
	}
	public function findFreshPosts(){
		$q = $this->findFreshPostsQ();
		return $q->getResult();
	}

	public function findNsfwPostsQ(){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
		       ->andWhere('p.nsfw=true')
		       ->orderBy('p.createdTime','DESC')
		       ;
		return $q->getQuery();
	}
	public function findNsfwPosts(){
		$q = $this->findNsfwPostsQ();
		return $q->getResult();
	}

	public function findHotPostsByCategoryQ($category){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
		       ->andWhere('p.category = :category')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.upvotesNo','DESC')
		       ->addOrderBy('p.createdTime','DESC')
		       ->getQuery()
		       ->setParameter('category', $category);
		return $q;
	}
	public function findHotPostsByCategory($category){
		$q = $this->findHotPostsByCategoryQ($category);
		return $q->getResult();
	}

	public function findTrendingPostsByCategoryQ($category){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
		       ->andWhere('p.category = :category')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.upvotesNo','DESC')
		       ->addOrderBy('p.createdTime','DESC')
		       ->getQuery()
		       ->setParameter('category', $category);
		return $q;
	}
	public function findTrendingPostsByCategory($category){
		$q = $this->findTrendingPostsByCategoryQ($category);
		return $q->getResult();
	}

	public function findFreshPostsByCategoryQ($category){
		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
		       ->andWhere('p.category = :category')
			->andWhere('p.nsfw = false')
		       ->orderBy('p.createdTime','DESC')
		       ->getQuery()
		       ->setParameter('category', $category);
		return $q;
	}
	public function findFreshPostsByCategory($category){
		$q = $this->findFreshPostsByCategoryQ($category);
		return $q->getResult();
	}

	public function findNextPostByPost(Post $post)
	{
		$createdTime = $post->getCreatedTime();

		$q = $this->createQueryBuilder('p');
		$q = $q->select('p')
		       ->where('p.isApproved=true')
		       ->andWhere('p.createdTime < :createdTime')
			->andWhere('p.category = :category')
		       ->orderBy('p.createdTime','DESC')
		       ->getQuery()
			->setMaxResults(1)
		       ->setParameter('category', $post->getCategory())
		       ->setParameter('createdTime', $createdTime);
		return $q->getResult();
	}

}