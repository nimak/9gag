<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/29/2014
 * Time: 1:20 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
/**
 * Class ApprovePostSubscriber
 *
 * @DI\Service("event_listener.approve_post_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class ApprovePostSubscriber implements EventSubscriber
{

	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'prePersist'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();
		if ($entity instanceof Post)
		{
			/**
			 * Approve the post automatically if the user is a "Trusted User"
			 */
			if ($entity->getIsApproved() == null) {
				$entity->setIsApproved($entity->getCreatedBy()->getIsTrustedUser());
			}
		}
	}
} 