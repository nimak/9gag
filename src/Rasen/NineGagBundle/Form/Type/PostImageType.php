<?php
namespace Rasen\NineGagBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('imageFile', 'file', array(
            'required'  => true,
        ));
        $builder->add('postTitle','text', array(
            'required'  => false,
        ));
        $builder->add('category');
        $builder->add('nsfw', 'checkbox', array(
            'label'     => 'list.label_nsfw',
            'required'  => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Rasen\NineGagBundle\Entity\PostImage',
            'translation_domain' => 'RasenNineGagBundle',
            ));
    }

    public function getName()
    {
        return 'post_image';
    }
}