<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/28/2014
 * Time: 6:28 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Rasen\NineGagBundle\Lib\JsonProducer;
use JMS\DiExtraBundle\Annotation as DI;
use Snc\RedisBundle\Client\Phpredis\Client;

/**
 * Class ImageProcessor
 *
 * @DI\Service("rasen_ninegag.image_processor")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class ImageProcessor
{
	const MESSAGE_TYPE_PROCESS = 'process';
	const MESSAGE_TYPE_PURGE = 'purge';

	const FILTER_MP4 = 'mp4';
	const FILTER_WEBM = 'webm';
	const FILTER_THUMB_700 = 'thumb700';
	const FILTER_THUMBS_500 = 'thumb500';
	const FILTER_THUMBS_FEATURE = 'feature';

	const STATUS_NOT_PROCESSED = '0';
	const STATUS_PROCESSING = '1';
	const STATUS_PROCESSED = '2';

	/**
	 * @var JsonProducer
	 */
	private $imageProcessorProducer;

	private $redis;

	/**
	 * * @DI\InjectParams({
	 *     "imageProcessorProducer" = @DI\Inject("old_sound_rabbit_mq.image_processor_producer"),
	 *     "redis" = @DI\Inject("snc_redis.default"),
	 * })
	 * @param JsonProducer $imageProcessorProducer
	 * @param $redis
	 */
	public function __construct(JsonProducer $imageProcessorProducer, $redis)
	{
		$this->imageProcessorProducer = $imageProcessorProducer;
		$this->redis = $redis;
	}

	public function process($postImageId, $imagePath, $saveToPath)
	{
		$this->setFileStatusInRedis($imagePath, self::STATUS_NOT_PROCESSED);
		$msg = array(
			'type' => self::MESSAGE_TYPE_PROCESS,
			'filePath' => $imagePath,
			'saveToPath' => $saveToPath,
			'postId' => $postImageId
		);
		$this->imageProcessorProducer->publish($msg);
	}

	public function purge($cachePath, $imageName)
	{
		$msg = array(
			'type' => self::MESSAGE_TYPE_PURGE,
			'cachePath' => $cachePath,
			'fileName' => $imageName
		);
		$this->imageProcessorProducer->publish($msg);
	}

	private function setFileStatusInRedis($imagePath, $status)
	{
		$timeout = 60 * 60 * 24; //1 Day
		$this->redis->set($imagePath.':status', $status, $timeout);
	}

	private function getFileStatusInRedis($imagePath)
	{
		return $this->redis->get($imagePath.':status');
	}
} 