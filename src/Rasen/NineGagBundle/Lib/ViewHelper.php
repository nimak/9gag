<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/1/2014
 * Time: 9:43 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Notice;

/**
 * Class ViewHelper
 * Some stuff to help out rendering the view / frontend.
 *
 * @DI\Service("rasen_ninegag.view_helper")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class ViewHelper {

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager")
	 * })
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}


	/**
	 * Returns all categories stored in database as an array.
	 *
	 * @return mixed
	 */
	public function getAllCategories()
	{
		$allCategories = $this->em->getRepository('RasenNineGagBundle:PostCategory')->findAllArray();
		return $allCategories;
	}
	/**
	 * Returns all categories that are main menu
	 *
	 * @return mixed
	 */
	public function getAllCategoriesClassified()
	{
		$allCategories = $this->getAllCategories();
		$categories = array('main' => array(), 'more'=>array());
		foreach ($allCategories as $category) {
			if ($category['mainMenu']) {
				$categories['main'][] = $category;
			} else {
				$categories['more'][] = $category;
			}
		}
		return $categories;
	}


	/**
	 * Returns unread notifications count
	 * @param $userId
	 * @return mixed
	 */
	public function getUnreadNotificationsCount($userId)
	{
		$unreadCount = $this->em->getRepository('RasenNineGagBundle:Notification')->countAllUnreadByUserId($userId);
		return $unreadCount;
	}

    public function getFeaturedPosts(){
        $featuredPosts= $this->em->getRepository('RasenNineGagBundle:Post')->findTopFeatured();
        return $featuredPosts;

    }

    /**
     * Return the latest active notice
     *
     * @return Notice|bool
     */
    public function getActiveNotice(){
        /**
         * @var Notice $activeNotice
         */
        $activeNotice = $this->em->getRepository('RasenNineGagBundle:Notice')->findBy(array(
            'active' => true
        ), array(
            'id' => 'DESC'
        ));
        return (!empty($activeNotice) ? $activeNotice[0] : false);
    }

	public function getProfileColor($username)
	{
		$usernameMd5 = md5($username);
		$range = array(65,219);
		$numbers = join('', array_map(function ($n) { return sprintf('%03d', $n); }, unpack('C*', $usernameMd5)));

		$rS = str_split($numbers,32)[0];
		$gS = str_split($numbers,32)[1];
		$bS = str_split($numbers,32)[2];

		$rSS = 0;
		foreach (str_split($rS,3) as $rI) {
			$rSS += intval($rI);
		}
		$gSS = 0;
		foreach (str_split($gS,3) as $gI) {
			$gSS += intval($gI);
		}
		$bSS = 0;
		foreach (str_split($bS,3) as $bI) {
			$bSS += intval($bI);
		}

		$rangeDiff = $range[1] - $range[0];
		$r = floor($range[0] + ((intval($rSS) / intval('1'.str_repeat(0, strlen($rSS)))) * $rangeDiff));
		$g = floor($range[0] + ((intval($gSS) / intval('1'.str_repeat(0, strlen($gSS)))) * $rangeDiff));
		$b = floor($range[0] + ((intval($bSS) / intval('1'.str_repeat(0, strlen($bSS)))) * $rangeDiff));
		return $this->RGBToHex($r, $g, $b);
	}

	private function RGBToHex($r, $g, $b) {
		//String padding bug found and the solution put forth by Pete Williams (http://snipplr.com/users/PeteW)
		$hex = "#";
		$hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
		$hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
		$hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);

		return $hex;
	}
} 