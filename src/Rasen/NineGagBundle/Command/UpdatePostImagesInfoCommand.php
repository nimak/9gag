<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 3/13/2015
 * Time: 9:42 PM
 */

namespace Rasen\NineGagBundle\Command;

use Doctrine\ORM\EntityManager;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Lib\ImageInformationLib;
use Rasen\NineGagBundle\Lib\PostImageProcessor;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class GenerateHashIdsCommand
 * @package Rasen\NineGagBundle\Command
 */
class UpdatePostImagesInfoCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('rasen:post:update-info')
			->setDescription('Updates posts\' images information (height, width, size) in database.')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'force update all of the posts, instead of just the ones with no information stored.'
            )
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
        /**
         * $container ContainerInterface
         */
        $container = $this->getContainer();


        /**
         * @var EntityManager $em
         */
        $em = $container->get('doctrine')->getManager();

        /**
         * @var ImageInformationLib $imageInformation
         */
        $imageInformation = $container->get('rasen_ninegag.image_information');

        $selectDQL = "SELECT p FROM RasenNineGagBundle:PostImage p";
        if (!$input->hasOption('force') || !$input->getOption('force')) {
            $selectDQL .= " WHERE p.height IS NULL OR p.width IS NULL OR p.size IS NULL";
        }
        $selectQ = $em->createQuery($selectDQL);
        $results = $selectQ->iterate();
        $i = 1;
        $n = 0;
        $batchSize = 20;
        $output->writeln('Updating posts\' images information...');
        foreach ($results as $row) {
            $post = $row[0];
            if ($post instanceof PostImage)
            {
                if (empty($post->getImageUrl())) {
                    continue;
                }

                $postImagePath = $this->getPostImagePath($post);

                $post->setIsAnimated($imageInformation->isAnimated($postImagePath));

                list($width, $height, $type, $attr) = $imageInformation->getImageDimensions($postImagePath);
                $post->setWidth($width)->setHeight($height);

                $imageSize = $imageInformation->getFileSize($postImagePath);
                $post->setSize($imageSize);

                $n++;

                if (($i % $batchSize) === 0) {
                    $em->flush(); // Executes all updates.
                    $em->clear(); // Detaches all objects from Doctrine!
                }
            }
            ++$i;
        }
        $em->flush(); // Executes all updates.
        if ($n > 0) {
            $output->writeln('Updated '.$n.' posts!');
        } else {
            $output->writeln('Nothing to update! (use --force if you want to update ALL posts)');
        }
        $output->writeln('Done :)');
	}

    private function getPostImagePath(PostImage $post)
    {
        $uploadPath = $this->getContainer()->getParameter('kernel.root_dir').PostImageProcessor::POST_UPLOAD_DIR;
        return realpath(PostImageProcessor::join_paths($uploadPath, $post->getImageUrl()));
    }
} 