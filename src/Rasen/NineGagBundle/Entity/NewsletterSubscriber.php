<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * NewsletterSubscriber
 *
 * @ORM\Table(name="newsletter_subscribers", uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="newsletter_subscriber.email.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class NewsletterSubscriber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank(message = "newsletter_subscriber.email.not_blank")
     * @Assert\Email(
     *     message = "newsletter_subscriber.email.email",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(name="subscribed_time", type="datetime", nullable=true)
     */
    private $subscribedTime;

    /**
     * Whether or not the email has an active subscription to the newsletter
     *
     * If the user opts out or unsubscribes from the newsletter this value will become **false**,
     * therefore the user will not receive any more newsletters.
     *
     * @var boolean
     *
     * @ORM\Column(name="is_active_subscription", type="boolean", nullable=false)
     */
    private $isActiveSubscription;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(name="last_modified_time", type="datetime", nullable=false)
     */
    private $lastModifiedTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return NewsletterSubscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set subscribedTime
     *
     * @param \DateTime $subscribedTime
     * @return NewsletterSubscriber
     */
    public function setSubscribedTime($subscribedTime)
    {
        $this->subscribedTime = $subscribedTime;

        return $this;
    }

    /**
     * Get subscribedTime
     *
     * @return \DateTime 
     */
    public function getSubscribedTime()
    {
        return $this->subscribedTime;
    }

    /**
     * Set isActiveSubscription
     *
     * @param boolean $isActiveSubscription
     * @return NewsletterSubscriber
     */
    public function setIsActiveSubscription($isActiveSubscription)
    {
        $this->isActiveSubscription = $isActiveSubscription;

        return $this;
    }

    /**
     * Get isActiveSubscription
     *
     * @return boolean 
     */
    public function getIsActiveSubscription()
    {
        return $this->isActiveSubscription;
    }

    /**
     * Set lastModifiedTime
     *
     * @param \DateTime $lastModifiedTime
     * @return NewsletterSubscriber
     */
    public function setLastModifiedTime($lastModifiedTime)
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * Get lastModifiedTime
     *
     * @return \DateTime
     */
    public function getLastModifiedTime()
    {
        return $this->lastModifiedTime;
    }
}
