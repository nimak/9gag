<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/21/2014
 * Time: 7:49 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class UserAdmin
 * @package Rasen\NineGagBundle\Admin
 */
class UserAdmin extends BaseUserAdmin
{
	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'username'
	);

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'id',
            'username',
            'email',
            'firstName',
            'lastName',
            'gender',
            'createdTime',
            'lastLogin',
            'phone',
            'profilePic',
            'profileBio',
            'birthday',
            'isTrustedUser',
            'postsNo',
            'approvedPostsNo',
            'commentsNo',
            'approvedCommentsNo',
            'followersNo',
            'followingNo',
            'pointsNo',
            'reportsNo',
        );
    }

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'user';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->add('username', 'text')
			->add('email', 'text')
			->add('firstName', 'text')
			->add('lastName', 'text')
			->add('createdTime', 'date')
			->add('phone', 'text')
			->add('profilePic', 'file')
			->add('profileBio', 'text')
			->add('birthday', 'date')
			->add('postsNo')
			->add('approvedPostsNo')
			->add('commentsNo')
			->add('approvedCommentsNo')
			->add('followersNo')
			->add('followingNo')
			->add('pointsNo')
			->end()
			// .. more info
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{

		$subject = $this->getSubject();
		$profilePicFieldOptions = array('required' => false);

		// Show image preview as helper
		if ($subject && ($webPath = $subject->getProfilePicName())) {
			// get the container so the full path to the image can be set
			$container = $this->getConfigurationPool()->getContainer();
			$helper = $container->get('vich_uploader.templating.helper.uploader_helper');
			$path = $helper->asset($subject, 'profile_pics');

			// add a 'help' option containing the preview's img tag
			$profilePicFieldOptions['help'] = '<img src="'.$path.'" class="admin-preview" />';
		}

		$formMapper
			->with('users',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('username', 'text')
			->add('email', 'text')
			->add('plainPassword', 'text', array('required' => false))
			->add('isTrustedUser', 'checkbox', array('required'=>false, 'help'=>'form.description.user.is_trusted_user'))
			->add('firstName', 'text', array('required'=>false))
			->add('lastName', 'text', array('required'=>false))
			->add('gender', 'choice',
				array(
					'choices' =>
						array(
						      '0' => 'form.gender.not_specified',
						      '1' => 'form.gender.male',
						      '2' => 'form.gender.female'
						),
					'translation_domain' => 'RasenNineGagBundle'
				)
			)
			->add('phone', 'text', array('required'=>false))
			->add('profilePic', 'file', $profilePicFieldOptions)
			->add('profileBio', 'text', array('required'=>false, 'help'=>'form.description.user.profile_bio'))
			->add('birthday', 'birthday', array('required'=>false))
			->end()
		;

		if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
			$formMapper
				->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_EDIT_POSTS' => 'ROLE_EDIT_POSTS',
                        'ROLE_DELETE_POSTS' => 'ROLE_DELETE_POSTS',
                        'ROLE_EDIT_COMMENTS' => 'ROLE_EDIT_COMMENTS',
                        'ROLE_DELETE_COMMENTS' => 'ROLE_DELETE_COMMENTS',
                        'ROLE_MODERATOR' => 'ROLE_MODERATOR',
                        'ROLE_USER' => 'ROLE_USER',
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
                        'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
                    ),
					'expanded' => true,
					'multiple' => true,
					'required' => false,
					'translation_domain' => 'RasenNineGagBundle'
				))
				->add('locked', 'checkbox', array('required' => false))
				->add('expired', 'checkbox', array('required' => false))
				->add('enabled', 'checkbox', array('required' => false))
				->add('credentialsExpired', 'checkbox', array('required' => false))
				->end()
			;
		}
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $filterMapper)
	{
		$filterMapper
			->add('id')
			->add('username')
			->add('isTrustedUser')
			->add('firstName')
			->add('lastName')
			->add('gender')
            ->add('createdTime')
			->add('phone')
			->add('birthday')
			->add('isDeleted', 'doctrine_orm_callback', array(
//                'callback'   => array($this, 'getWithOpenCommentFilter'),
				'callback' => function($queryBuilder, $alias, $field, $value) {
					if (!$value['value']) {
						return;
					}
					if ($value['value'] == 1) {
						$queryBuilder->andWhere($alias.'.deletedAt is NOT NULL');
					} else {
						$queryBuilder->andWhere($alias.'.deletedAt is NULL');
					}

					return true;
				},
				'field_type' => 'sonata_type_boolean'
			))
			->add('postsNo')
			->add('approvedPostsNo')
			->add('commentsNo')
			->add('approvedCommentsNo')
			->add('upvotesNo')
			->add('followersNo')
			->add('followingNo')
			->add('pointsNo')
		;
	}
	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('username')
			->add('email')
			->add('enabled')
			->add('locked')
			->add('isTrustedUser')
			->add('firstName')
			->add('lastName')
			->add('gender')
            ->add('createdTime')
			->add('phone')
			->add('birthday')
			->add('postsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_post_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('approvedPostsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_post_list',
					'parameters' => array('filter[isApproved][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('commentsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_comment_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('approvedCommentsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_comment_list',
					'parameters' => array('filter[approved][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('upvotesNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_postvote_list',
					'parameters' => array('filter[vote][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('followersNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_userfollower_list',
					'parameters' => array('type'=>'followers'),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('followingNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_userfollower_list',
					'parameters' => array('type'=>'following'),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('pointsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_user_userpoint_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
		;

		if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
			$listMapper
				->add('impersonating', 'string', array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'))
			;
		}
	}
}