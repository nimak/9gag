{% set imagemagick = pillar.get('imagemagick', {}) -%}
{% set source = imagemagick.get('source', 'ImageMagick.tar.gz') -%}
{% set version = imagemagick.get('version', '6.9.0-7') -%}

git_packages:
  pkg.installed:
    - names:
      - libssl-dev
      - git
      - pkg-config
      - build-essential
      - curl
      - gcc
      - g++
      - checkinstall
  cmd.run:
    - name: apt-get build-dep imagemagick -y
    - user: root

get-imagick:
  file.managed:
    - name: /usr/src/{{ source }}
    - source: salt://imagemagick/files/{{ source }}
    - require:
      - pkg: git_packages
  cmd.wait:
    - cwd: /usr/src
    - names:
      - tar -zxvf {{ source }}
    - watch:
      - file: /usr/src/{{ source }}
    - unless: command -v identify

make-imagick:
  cmd.run:
    - cwd: /usr/src/ImageMagick-{{ version }}
    - names:
      - ./configure
      - make
      - checkinstall --install=yes --pkgname=imagemagick --pkgversion "{{ version }}" --default
    - unless: command -v identify

register-libs:
  cmd.run:
    - cwd: /usr/src/ImageMagick-{{ version }}
    - name: ldconfig /usr/local/lib
    - onlyif: command -v identify