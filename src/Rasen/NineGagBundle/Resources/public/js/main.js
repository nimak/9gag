if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

function secureJsonConverter(text) {
    text = text.replace(")]}',\n", '');
    return $.parseJSON(text);
}

$(document).ready(function() {
    var arrowDropdown = $('.arrow-dropdown');
    $('.arrow-dropdown > a.dropdown-toggle').on('click', function (e) {
        var dropdown = $(this).parent();
        arrowDropdown.filter('.open').not(dropdown).removeClass("open");
        dropdown.toggleClass("open");
    });
    $('body').on('click', function (e) {
        if (!arrowDropdown.is(e.target) && arrowDropdown.has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
            arrowDropdown.removeClass('open');
        }
    });

    $('a.dropdown-toggle').on('click', function(e){
        e.preventDefault();
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip({container: "body", placement: "top"})
    });
    var topOffset = $('#profileTabs').outerHeight(true) + $('.navbar-fixed-top').height() +10;
    $('#promoBox').css('top', topOffset);
    $('#promoBox').affix({
        offset: {
            top: function () {
                var height = ($('#mainSidebar').offset().top + $('#mainSidebar').outerHeight(true)) - topOffset - $('#promoBox').outerHeight(true);
                return (this.top = height);
            }
        }
    });
    $('.newsletter-subscribe-form').on('submit', function(e){
        e.preventDefault();
        var form = $(e.target);
        var btnSubscribe = form.find('.btn-subscribe');
        var emailInput = form.find('.subscribe-email');
        var errorContainer = form.find('.form-errors-container');
        $.ajax({
            type: "POST",
            url: Routing.generate('ajax_newsletter_post_newsletter_subscriber', [], true),
            cache: false,
            converters: {
                "text json": secureJsonConverter
            },
            headers: {"X-XSRF-TOKEN": docCookies.getItem('XSRF-TOKEN')},
            data: { email: emailInput.val(),_token: form.find('input.token').val() },
            beforeSend: function() {
                emailInput.prop('disabled', true);
                btnSubscribe.prop('disabled', true);
                btnSubscribe.find('.btn-label').hide();
                btnSubscribe.find('.loading').show();
            }
        }).success(function(){
            form.hide();
            form.parent().find('.success-msg').show();
        }).error(function(xhr, status, error){
            var errors = JSON.parse(xhr.responseText)
            for (var i = 0; i<errors.length; i++) {
                errorContainer.html('');
                errorContainer.append('<li class="form-error">'+errors[i].message+'</li>');
            }
        }).complete(function(){
            emailInput.prop('disabled', false);
            btnSubscribe.prop('disabled', false);
            btnSubscribe.find('.btn-label').show();
            btnSubscribe.find('.loading').hide();
        });
    });
    $('#openRegisterModal').on('click', function(){
        $('#loginModal').modal('hide');
    });
    jQuery.fn.showLoading = function (){
        $(this).prop('disabled', true);
        $(this).html('<i class="ion-loading-c"></i>');
        return this;
    };
    $('form.submit-once').each(function(i, el){
        var $el = $(el);
        $el.on('submit', function(e){
            if ($el.data('submitted') === true) {
                e.preventDefault();
            } else {
                $el.data('submitted', true);
                $el.find('button[type="submit"]').showLoading();
                $el.find('input[type="submit"]').showLoading();
            }
        });
    });
    if ($('#noticeDropdown').length) {
        var noticeDropdownBtn = $('#noticeDropdownBtn');
        var noticeIcon = noticeDropdownBtn.find('.notice-icon');
        var noticeContent = $('#noticeContent');
        var noticeId = noticeContent.data('id');
        var noticedBefore = function(){
            return docCookies.hasItem('nvid') && docCookies.getItem('nvid') == noticeId;
        };
        if (!noticedBefore()) {
            noticeIcon.removeClass('animation-off');
        }
        noticeDropdownBtn.on('click', function(e){
            if (!noticedBefore()) {
                ga('send', 'event', 'notice-view', 'click', noticeContent.find('.notice-title').text(), 1);
            }
            noticeIcon.addClass('animation-off');
            docCookies.setItem('nvid', noticeId);
        });
        noticeContent.perfectScrollbar({
            suppressScrollX: true
        });
    }

    if (!docCookies.hasItem('f')) {
        var fingerprint = new Fingerprint().get();
        docCookies.setItem('f', fingerprint, Infinity, '/');
    }

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        $(this).parents('.upload-input').find('.file-name').text(label);
    });
});
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});