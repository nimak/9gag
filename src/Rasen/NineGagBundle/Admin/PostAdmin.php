<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/17/2014
 * Time: 3:39 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostImage;
use Rasen\NineGagBundle\Entity\PostText;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class PostAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class PostAdmin extends Admin
{
	protected $parentAssociationMapping = 'createdBy';

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'post';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('restore', $this->getRouterIdParameter().'/restore');
		$collection->add('approve', $this->getRouterIdParameter().'/approve');
		$collection->add('disapprove', $this->getRouterIdParameter().'/disapprove');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBatchActions()
	{
		// retrieve the default batch actions
		$actions = parent::getBatchActions();

		if (
			$this->hasRoute('delete') && $this->isGranted('DELETE')
		) {
			$actions['restore'] = array(
				'label' => $this->trans('action_restore', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);
		}
		if (
			$this->hasRoute('edit') && $this->isGranted('EDIT')
		) {
			$actions['approve'] = array(
				'label' => $this->trans('action_approve', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);
			$actions['disapprove'] = array(
				'label' => $this->trans('action_disapprove', array(), 'RasenNineGagBundle'),
				'ask_confirmation' => true
			);

		}

		return $actions;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->with('post',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('isApproved')
			->add('nsfw')
			->add('featured')
			->add('category')
			->add('commentsNo')
			->add('approvedCommentsNo')
			->add('viewsNo')
			->add('downloadsNo')
			->add('sharesNo')
			->add('createdTime')
			->add('createdBy')
			->add('approvedTime')
			->add('approvedBy')
			->end()
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$subject = $this->getSubject();
		$imageFileFieldOptions = array('required' => false);

		// Show image preview as helper
		if ($subject && ($subject instanceof PostImage)  && ($webPath = $subject->getImageUrl())) {
			// get the container so the full path to the image can be set
			$container = $this->getConfigurationPool()->getContainer();
			$helper = $container->get('vich_uploader.templating.helper.uploader_helper');
			$path = $helper->asset($subject, 'posts');
            $cachedFilterUrl = $container->get('liip_imagine.cache.manager')->getBrowserPath($path, 'posts_full');
			//$fullPath = $container->get('request')->getBasePath().'/uploads/posts/'.$webPath;

			// add a 'help' option containing the preview's img tag
			$imageFileFieldOptions['help'] = '<img src="'.$cachedFilterUrl.'" class="admin-preview" />';
		}

		if ($subject instanceof PostImage) {
			$formMapper
				->add('postTitle', 'text', array('required' => false))
				->add('imageFile', 'file', $imageFileFieldOptions)
			;
		}
		elseif ($subject instanceof PostText) {
			$formMapper
				->add('content', 'textarea')
			;
		}

		$formMapper
			->add('category', 'entity', array('class' => 'Rasen\NineGagBundle\Entity\PostCategory', 'property'=>'name'))
			->add('isApproved', 'checkbox', array('required'=>false))
			->add('nsfw', 'checkbox', array('required'=>false))
            ->add('featured', 'checkbox', array('required'=>false))

        ;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('isApproved')
			->add('nsfw')
			->add('isDeleted', 'doctrine_orm_callback', array(
//                'callback'   => array($this, 'getWithOpenCommentFilter'),
				'callback' => function($queryBuilder, $alias, $field, $value) {
					if (!$value['value']) {
						return;
					}
					if ($value['value'] == 1) {
						$queryBuilder->andWhere($alias.'.deletedAt is NOT NULL');
					} else {
						$queryBuilder->andWhere($alias.'.deletedAt is NULL');
					}

					return true;
				},
				'field_type' => 'sonata_type_boolean'
			))
            ->add('featured')
			->add('category')
			->add('commentsNo')
			->add('approvedCommentsNo')
			->add('viewsNo')
            ->add('downloadsNo')
            ->add('sharesNo')
			->add('createdTime')
			->add('createdBy.id')
			->add('approvedTime')
			->add('approvedBy.id')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$subject = $this->getSubject();
		$listMapper
			->addIdentifier('id')
			->add('content', 'string', array('template'=>'RasenNineGagBundle:Admin/CRUD:list_post_content.html.twig'))
			->add('isApproved', 'boolean', array('editable'=>true))
			->add('nsfw', 'boolean', array('editable'=>true))
            ->add('featured', 'boolean', array('editable'=>true))
			->add('category')
			->add('commentsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_post_comment_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('approvedCommentsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_post_comment_list',
					'parameters' => array('filter[approved][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('viewsNo')
			->add('downvotesNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_post_postvote_list',
					'parameters' => array('filter[vote][value]' => 2),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('upvotesNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_post_postvote_list',
					'parameters' => array('filter[vote][value]' => 1),
					'identifier_parameter_name' => 'id'
				)
			))
			->add('reportsNo', 'url', array(
				'route' => array(
					'name' => 'admin_rasen_ninegag_post_postreport_list',
					'parameters' => array(),
					'identifier_parameter_name' => 'id'
				)
			))
            ->add('downloadsNo')
            ->add('sharesNo')
			->add('createdTime')
			->add('createdBy')
			->add('approvedTime')
			->add('approvedBy')
			->add('_action', 'actions', array(
				'actions' => array(
					'approve' => array('template'=>'RasenNineGagBundle:Admin/CRUD:list__action_approve.html.twig'),
					'disapprove' => array('template'=>'RasenNineGagBundle:Admin/CRUD:list__action_disapprove.html.twig'),
				)
			))
		;
	}


	/**
	 * @param Post $post
	 *
	 * @return mixed|void
	 */
	public function preUpdate($post)
	{
		//$post->getCreatedBy()->updateApprovedPostsNo();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNewInstance()
	{
		if (!$this->getRequest()->query->has('subclass')) $this->getRequest()->query->set('subclass', 'text');
		$object = $this->getModelManager()->getModelInstance($this->getClass());
		foreach ($this->getExtensions() as $extension) {
			$extension->alterNewInstance($this, $object);
		}

		return $object;
	}
} 