<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 2/18/2015
 * Time: 11:44 PM
 */

namespace Rasen\NineGagBundle\Hateoas\Representation;

use Hateoas\Configuration\Annotation as Hateoas;
use Hateoas\Representation\RouteAwareRepresentation;
use JMS\Serializer\Annotation as Serializer;
use Rasen\NineGagBundle\Lib\CursorPagination\Cursor;

/**
 * Class CursorPaginatedRepresentation
 * @package Rasen\NineGagBundle\Hateoas\Representation
 *
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\XmlRoot("collection")
 *
 * @Hateoas\Relation(
 *      "next",
 *      href = "expr(object.getNextUrl())",
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(not object.hasNext())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "previous",
 *      href = "expr(object.getPreviousUrl())",
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(not object.hasPrevious())"
 *      )
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class CursorPaginatedRepresentation extends RouteAwareRepresentation
{
    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\XmlAttribute
     */
    private $limit;

    /**
     * @var Cursor
     *
     * @Serializer\Expose
     * @Serializer\XmlAttribute
     */
    private $cursors;

    /**
     * @var array
     */
    private $paginationData;

    public function __construct(
        $inline,
        $route,
        array $parameters        = array(),
        $paginationData,
        $absolute                = false,
        $total                   = null
    ) {
        parent::__construct($inline, $route, $parameters, $absolute);

        $this->paginationData = $paginationData;
        $this->limit = $paginationData['limit'];
        $this->cursors = $paginationData['cursors'];
    }

    /**
     * @return array
     */
    public function getPaginationData()
    {
        return $this->paginationData;
    }

    /**
     * @return boolean
     */
    public function hasNext()
    {
        return array_key_exists('next', $this->paginationData);
    }

    /**
     * @return string
     */
    public function getNextUrl()
    {
        return array_key_exists('next', $this->paginationData) ? $this->paginationData['next'] : null;
    }

    /**
     * @return boolean
     */
    public function hasPrevious()
    {
        return array_key_exists('previous', $this->paginationData);
    }

    /**
     * @return string
     */
    public function getPreviousUrl()
    {
        return array_key_exists('previous', $this->paginationData) ? $this->paginationData['previous'] : null;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }
}