<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 11/03/2014
 * Time: 12:05 PM
 */

namespace Rasen\NineGagBundle\Lib;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Hashids\Hashids;
use JMS\Serializer\SerializerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Rasen\NineGagBundle\Entity\Badge;
use Rasen\NineGagBundle\Entity\NotificationMeta;
use \Rasen\NineGagBundle\Entity\User;
use \Rasen\NineGagBundle\Entity\Post;
use \Rasen\NineGagBundle\Entity\PostVote;
use \Rasen\NineGagBundle\Entity\PostImage;
use \Rasen\NineGagBundle\Entity\PostText;
use \Rasen\NineGagBundle\Entity\Comment;
use \Rasen\NineGagBundle\Entity\CommentVote;
use \Rasen\NineGagBundle\Entity\Notification;
use \Rasen\NineGagBundle\Lib\SendNotificationProducer;

use \Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\UserBadge;
use Rasen\NineGagBundle\Entity\UserPoint;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\LoggingTranslator;
use Symfony\Component\Translation\Translator;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class NotificationUtility
 *
 *
 * @DI\Service("rasen_ninegag.notification_utility")
 *
 * @package Rasen\NineGagBundle\Lib
 */
class NotificationUtility
{

	/**
	 * @var EntityManager
	 */
	protected $em;

	protected $translator;

	/**
	 * @var Router
	 */
	protected $router;

	/**
	 * @var SendNotificationProducer
	 */
	protected $sendNotificationProducer;

	/**
	 * @var SerializerInterface
	 */
	protected $serializer;

	/**
	 * @var Hashids
	 */
	private $hashids;

	/**
	 * @var UrlSlugGenerator $urlSlugGenerator
	 */
	protected $urlSlugGenerator;

    /**
     * @var UploaderHelper
     */
    protected $uploaderHelper;

    /**
     * @var CacheManager
     */
    protected $liipImagine;

	/**
	 * @DI\InjectParams({
	 *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
	 *     "translator" = @DI\Inject("translator"),
	 *     "router" = @DI\Inject("router"),
	 *     "sendNotificationProducer" = @DI\Inject("old_sound_rabbit_mq.send_notification_producer"),
	 *     "serializer" = @DI\Inject("jms_serializer"),
	 *     "hashids" = @DI\Inject("hashids"),
	 *     "urlSlugGenerator" = @DI\Inject("rasen_ninegag.url_slug_generator"),
     *     "uploaderHelper" = @DI\Inject("vich_uploader.templating.helper.uploader_helper"),
     *     "liipImagine" = @DI\Inject("liip_imagine.cache.manager")
	 * })
	 * @param EntityManager $em
	 * @param $translator
	 * @param Router $router
	 * @param SendNotificationProducer $sendNotificationProducer
	 * @param SerializerInterface $serializer
	 * @param Hashids $hashids
	 * @param UrlSlugGenerator $urlSlugGenerator
     * @param UploaderHelper $uploaderHelper
     * @param CacheManager $liipImagine
	 */
	public function __construct(EntityManager $em,
		$translator,
		Router $router,
		SendNotificationProducer $sendNotificationProducer,
		SerializerInterface $serializer,
		Hashids $hashids,
		UrlSlugGenerator $urlSlugGenerator,
        UploaderHelper $uploaderHelper,
		CacheManager $liipImagine)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->router = $router;
		$this->sendNotificationProducer = $sendNotificationProducer;
		$this->serializer = $serializer;
		$this->hashids = $hashids;
		$this->urlSlugGenerator = $urlSlugGenerator;
        $this->uploaderHelper = $uploaderHelper;
        $this->liipImagine = $liipImagine;
	}

	/**
	 * Returns the corresponding class name for a object type
	 *
	 * @param $objectType
	 *
	 * @return string
	 */
	private function getObjectTypeClassName($objectType)
	{
		switch ($objectType)
		{
			case Notification::OBJECT_TYPE_POST:
				return 'RasenNineGagBundle:Post';
				break;
			case Notification::OBJECT_TYPE_COMMENT:
				return 'RasenNineGagBundle:Comment';
				break;
			case Notification::OBJECT_TYPE_USER:
				return 'RasenNineGagBundle:User';
				break;
			case Notification::OBJECT_TYPE_BADGE:
				return 'RasenNineGagBundle:UserBadge';
				break;
			case Notification::OBJECT_TYPE_POINT:
				return 'RasenNineGagBundle:UserPoint';
				break;
			default:
				return false;
		}
	}


	/**
	 * Get the object instance from database.
	 *
	 * @param $objectType
	 * @param $objectId
	 *
	 * @return null|object
	 */
	private function getObject($objectType, $objectId)
	{
		$objectClassName = $this->getObjectTypeClassName($objectType);
		$object = $this->em->getRepository($objectClassName)->findOneBy(array('id'=>$objectId));
		return $object;
	}

	/**
	 * Returns the corresponding view url for a object type
	 *
	 * @param $object
	 * @param $verb
	 * @param array $meta
	 *
	 * @return string
	 */
	private function getObjectViewUrl($object, $verb, $meta=array())
	{
        if (!$object) return false;
        if ($object instanceof Post)
        {
            $postViewUrl = $this->urlSlugGenerator->generateUrl($object);
            if ($verb == Notification::VERB_COMMENTED) {
                //die(var_dump($meta));
                if ($meta['comment_id'] !== null) {
                    //$hashCommentId = $this->hashids->encode($meta['comment_id']);
                    $commentLink = '#c'.$meta['comment_id'];
                } else {
                    $commentLink = '';
                }
                return $postViewUrl.$commentLink;
            }
            return $postViewUrl;
        }
        elseif ($object instanceof Comment)
        {
            $post = $object->getPost();
            if (!$post) return false;
            $postViewUrl = $this->urlSlugGenerator->generateUrl($post);
            if ($verb == Notification::VERB_REPLIED) {
                if ($meta['reply_id'] !== null) {
                    $commentLink = '#c'.$meta['reply_id'];
                } else {
                    $commentLink = '';
                }
                return $postViewUrl.$commentLink;
            }
            $hashCommentId = $this->hashids->encode($object->getId());
            return $postViewUrl.'#c'.$hashCommentId;
        }
        elseif ($object instanceof User)
        {
            return $this->router->generate('rasen_ninegag_user_profile', array('username'=>$object->getUsername()));
        }
        elseif ($object instanceof UserBadge)
        {
            return $this->router->generate('rasen_ninegag_user_viewbadges', array('username'=>$object->getUser()->getUsername()));
        }
        elseif ($object instanceof UserPoint)
        {
            return $this->router->generate('rasen_ninegag_user_viewpoints', array('username'=>$object->getUser()->getUsername()));
        }
        else
        {
            return false;
        }
	}

	/**
	 * Returns the corresponding view image for a notification
	 *
	 * @param $object
	 * @param User $actor
	 * @param $verb
	 *
	 * @return string
	 */
	private function getNotifImage($object, User $actor, $verb)
	{

        if ($this->uploaderHelper->asset($actor, 'profile_pics')) {
            $url = $this->uploaderHelper->asset($actor, 'profile_pics');
        } else {
            $url = '/bundles/rasenninegag/images/empty_user.png';
        }
        $cached48Url = $this->liipImagine->getBrowserPath($url, 'square_48');
        $imgTag = '<img class="thumb" width="48" height="48" src="'.$cached48Url.'">';

        if ($object instanceof Post) {
            if ($object instanceof PostImage)
            {
                return $imgTag;
            } else {
                $iconTag = '<i class="glyphicon glyphicon-th-large"></i>';
                return $iconTag;
            }
        }
        elseif ($object instanceof Comment)
        {
            return $imgTag;
        }
        elseif ($object instanceof User)
        {
            return $imgTag;
        }
        elseif ($object instanceof UserBadge)
        {
            return '<i class="fa fa-trophy"></i>';
        }
        elseif ($object instanceof UserPoint)
        {
            return '<i class="fa fa-asterisk"></i>';
        }
        else
        {
            return false;
        }
	}

	/**
	 * Returns the corresponding view icon for a object type
	 *
	 * @param $object
	 * @param $verb
	 *
	 * @return string
	 */
	private function getObjectViewIcon($object, $verb)
	{
        if ($object instanceof Post) {
            if ($object instanceof PostImage)
            {
                if ($verb == Notification::VERB_COMMENTED) {
                    $iconTag = '<i class="ion-chatboxes"></i>';
                } elseif ($verb == Notification::VERB_DOWNVOTED) {
                    $iconTag = '<i class="fa fa-thumbs-o-down"></i>';
                } elseif ($verb == Notification::VERB_UPVOTED) {
                    $iconTag = '<i class="fa fa-thumbs-o-up"></i>';
                } else {
                    $iconTag = '<i class="glyphicon glyphicon-th-large"></i>';
                }
                return $iconTag;
            } else {
                $iconTag = '<i class="glyphicon glyphicon-th-large"></i>';
                return $iconTag;
            }
        }
        elseif ($object instanceof Comment)
        {
            if ($verb == Notification::VERB_DOWNVOTED) {
                $iconTag = '<i class="ion-arrow-down-a"></i>';
            } elseif ($verb == Notification::VERB_UPVOTED) {
                $iconTag = '<i class="ion-arrow-up-a"></i>';
            } elseif ($verb == Notification::VERB_REPLIED) {
                $iconTag = '<i class="ion-chatboxes"></i>';
            } else {
                $iconTag = '<i class="ion-chatboxes"></i>';
            }
            return $iconTag;
        }
        elseif ($object instanceof User)
        {
            return '<i class="fa fa-user"></i>';
        }
        elseif ($object instanceof UserBadge)
        {
            return '<i class="fa fa-trophy"></i>';
        }
        elseif ($object instanceof UserPoint)
        {
            return '<i class="fa fa-asterisk"></i>';
        }
        else
        {
            return false;
        }
	}

	/**
	 * Returns the profile view url for a actor
	 *
	 * @param string $actorUsername
	 *
	 * @return string
	 */
	private function getActorViewUrl($actorUsername)
	{
		return $this->router->generate('rasen_ninegag_user_profile', array('username'=>$actorUsername));
	}

	/**
	 * Return the appropriate message id (translation id) for the combination of verb and object type.
	 *
	 * @param string $verb
	 * @param string $objectType
	 *
	 * @return string
	 */
	private function getVerbObjectMessageId($verb, $objectType)
	{
		$prefix = 'notification.messages';
		$messageId = $prefix . '.' . $objectType . '.' . $verb;

		return $messageId;
	}

	/**
	 * Returns all notifications for a given user
	 *
	 * The result is an array containing these keys:
	 *
	 * * (Array) messages : Contains all unread notification messages (grouped together).
	 *
	 *     Each message is an array of the following format:
	 *
	 *     * content : Notification message content.
	 *     * link : Notification message link. (usually a link to view the object that the notification is about)
	 *
	 * * (Integer) totalUnreadCount : Number of all unread notifications (not grouped)
	 *
	 * @param integer $userId
	 * @param boolean $onlyUnread
	 * @param integer $limit
	 *
	 * @return array
	 */
	public function getAllNotificationMessages($userId, $onlyUnread = true, $limit = null)
	{
		$result = array('messages', 'totalUnreadCount');
		$notificationRepo = $this->em->getRepository('RasenNineGagBundle:Notification');

		$notifs = $notificationRepo->findAllByUserId($userId, $onlyUnread, $limit);
		$messages = array();
		foreach ($notifs as $notif) {
            $msg = $this->buildMessage($notif);
            // false means it's no longer valid (referencing object got deleted)
            if ($msg === false) {
                // Notifications should be deleted
                $notificationIds = $notif['notificationIds'];
                $this->removeNotifications($notificationIds);
                continue;
            }
			$messages[] = $msg;
		}
		$result['messages'] = $messages;

		$unreadCount = $notificationRepo->countAllUnreadByUserId($userId);
		$result['totalUnreadCount'] = $unreadCount;

		// Mark all as read after fetching from db.
		//$this->markAllAsRead($userId);

		return $result;
	}

	/**
	 * Get unread notifications count (grouped)
	 *
	 * @param $notificationsQueryResult
	 */
	public function getUnreadCount($notificationsQueryResult)
	{
		count($notificationsQueryResult);
	}

	/**
	 * Returns necessary meta data for building a notification message.
	 *
	 * @param Notification $notification
	 * @param $object
	 *
	 * @return array
	 */
	private function getMetas(Notification $notification, $object) {
		$metas = array();

		if (!$notification->getNotificationMeta()->isEmpty()) {
			$notificationMetas = $notification->getNotificationMeta();
			/**
			 * @var NotificationMeta $notificationMeta
			 */
			foreach ($notificationMetas as $notificationMeta) {
				$metas[$notificationMeta->getName()] = $notificationMeta->getValue();
			}
        }

        if ($object instanceof UserBadge) {
            $metas['username'] = $object->getUser()->getUsername();
            $metas['badgeName'] = $object->getBadge()->getName();
        } elseif ($object instanceof UserPoint) {
            $metas['username'] = $object->getUser()->getUsername();
            $metas['pointsNo'] = $object->getPoint();
            $metas['pointDescription'] = $object->getDescription();
        } elseif ($object instanceof User) {
            $metas['username'] = $object->getUsername();
        } elseif ($object instanceof Comment) {
            $metas['postId'] = $object->getPost()->getId();
        }
        return $metas;
    }

	/**
	 * Marks all notifications as read
	 *
	 * @param $userId
	 */
	public function markAllAsRead($userId)
	{
		$notificationRepo = $this->em->getRepository('RasenNineGagBundle:Notification');
		return $notificationRepo->markAllAsRead($userId);
	}

    /**
     * Removes notifications
     *
     * @param string $notificationIds Comma separated ids
     */
    private function removeNotifications($notificationIds)
    {
        $sql = "DELETE FROM notifications WHERE id IN (:ids)";
        $params = array('ids' => $notificationIds);
        $this->em->getConnection()->executeUpdate($sql, $params);
    }

	/**
	 * Builds a notification message.
	 *
	 * The message is an array containing:
	 *
	 * * content : Notification message content.
	 * * link : Notification message link. (usually a link to view the object that the notification is about)
	 *
	 * @param array $notificationsQueryResult
	 *
	 * @return string
	 */
	private function buildMessage($notificationsQueryResult)
	{
		$message = array('content'=>null, 'link'=> null);

		/**
		 * @var Notification $sampleNotif
		 */
		$sampleNotif = $notificationsQueryResult[0];

		$lastNotification = $this->em->getRepository('RasenNineGagBundle:Notification')->findOneBy(array(
			'id' => $notificationsQueryResult['lastNotificationId']
		));
		$actorName = $lastNotification->getActor()->getName();

		$lastCreatedTime = $notificationsQueryResult['lastCreatedTime'];

		$objectType = $sampleNotif->getObjectType();
		$verb = $sampleNotif->getVerb();
		$objectId = $sampleNotif->getObjectId();
		$actorsNo = $notificationsQueryResult['actorCount'] - 1;

        $object = $this->getObject($objectType, $objectId);
        if (!$object) return false;

        $metas = $this->getMetas($sampleNotif, $object);
        $notifViewUrl = $this->getObjectViewUrl($object, $verb, $metas);

        // Check if notification related object is deleted (it's done in the getObjectViewUrl function)
        if ($notifViewUrl === false) {
            return false;
        }
        $message['link'] = $notifViewUrl;

		$transParams = array(
			'%actor%' => $actorName,
			'%totalActorsNo%' => $actorsNo
		);

		foreach ($metas as $meta=>$value) {
			$transParams['%'.$meta.'%'] = $value;
		}
		$actorUsername = $lastNotification->getActor()->getUsername();
		$actorProfileUrl = $this->getActorViewUrl($actorUsername);
		$transParams['%actorProfileUrl%'] = $actorProfileUrl;

		$messageId = $this->getVerbObjectMessageId($verb, $objectType);

		// Attach to message id to get the appropriate translated message for single or multiple grouped notifications
		if ($actorsNo > 0) {
			$messageId .= '.group';
		} else {
			$messageId .= '.single';
		}

		$message['content'] = $this->translator->trans($messageId, $transParams, 'RasenNineGagBundle');

		$notifViewIcon = $this->getObjectViewIcon($object, $verb);
		$message['icon'] = $notifViewIcon;

		$notifViewImage = $this->getNotifImage($object, $lastNotification->getActor(), $verb);
		$message['image'] = $notifViewImage;

		$message['timestamp'] = new \DateTime($lastCreatedTime, new \DateTimeZone('UTC'));

		$message['id'] = $notificationsQueryResult['lastNotificationId'];

		$message['read'] = $sampleNotif->getReadStatus();

		return $message;
	}

	/**
	 * Builds a notification message for a single notification.
	 *
	 * The message is an array containing:
	 *
	 * * content : Notification message content.
	 * * link : Notification message link. (usually a link to view the object that the notification is about)
	 * * id : Notification entity id
	 *
	 * @param Notification $notification
	 *
	 * @return string
	 */
	public function buildSingleMessage(Notification $notification)
	{
		$message = array('content'=>null, 'link'=> null, 'id'=> null);

		/**
		 * @var Notification $notification
		 */
		$actorName = $notification->getActor()->getName();
		$objectType = $notification->getObjectType();
		$verb = $notification->getVerb();
		$objectId = $notification->getObjectId();

		$transParams = array(
			'%actor%' => $actorName
		);

        $object = $this->getObject($objectType, $objectId);
        if (!$object) return false;

		$metas = $this->getMetas($notification, $object);

		foreach ($metas as $meta=>$value) {
			$transParams['%'.$meta.'%'] = $value;
		}
		$actorUsername = $notification->getActor()->getUsername();
		$actorProfileUrl = $this->getActorViewUrl($actorUsername);
		$transParams['%actorProfileUrl%'] = $actorProfileUrl;

		$messageId = $this->getVerbObjectMessageId($verb, $objectType);

		// Attach to message id to get the appropriate translated message for single or multiple grouped notifications
		$messageId .= '.single';


		$message['content'] = $this->translator->trans($messageId, $transParams, 'RasenNineGagBundle');

		$notifViewUrl = $this->getObjectViewUrl($object, $verb, $metas);
		$message['link'] = $notifViewUrl;

		$notifViewIcon = $this->getObjectViewIcon($object, $verb);
		$message['icon'] = $notifViewIcon;

        $notifViewImage = $this->getNotifImage($object, $notification->getActor(), $verb);
        $message['image'] = $notifViewImage;

		$message['timestamp'] = $notification->getCreatedTime();

		$message['id'] = $notification->getId();

		$message['read'] = $notification->getReadStatus();

		$message['username'] = $notification->getUser()->getUsername();

		$message['data_type'] = 'notification';

		return $this->serializer->serialize($message, 'json');
	}

	/**
	 * Builds a routing key for each notification
	 *
	 * @param Notification $notification
	 *
	 * @return string
	 */
	public function getNotificationRoutingKey(Notification $notification)
	{
		return 'notification.'.$notification->getUser()->getUsername();
	}

	/**
	 * Publishes the notification message to consumers (Through RabbitMQ)
	 *
	 * @param Notification $notification
	 */
	public function publishNotification(Notification $notification)
	{
		$routingKey = $this->getNotificationRoutingKey($notification);
		$msg = $this->buildSingleMessage($notification);
		$this->sendNotificationProducer->publish($msg, $routingKey);
	}
}