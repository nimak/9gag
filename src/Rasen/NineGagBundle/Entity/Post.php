<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Rasen\NineGagBundle\Model\VotableReportable;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
/**
 * Post
 *
 * A Post is either a Text or an Image.
 *
 * @ORM\Table(name="posts", indexes={@ORM\Index(name="posts_category_id_idx", columns={"category_id"}), @ORM\Index(name="posts_created_by_idx", columns={"created_by"}), @ORM\Index(name="posts_last_modified_by_idx", columns={"last_modified_by"}), @ORM\Index(name="posts_approved_by_idx", columns={"approved_by"})})
 * @ORM\Entity(repositoryClass="Rasen\NineGagBundle\Entity\PostRepository") @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap( {"text" = "PostText", "image" = "PostImage"} )
 *
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "ajax_post_get_post",
 *          parameters = { "id" = "expr(service('hashids').encode(object.getId()))" }
 *      )
 * )
 * @Hateoas\Relation(
 *      "comments",
 *      href = @Hateoas\Route(
 *          "ajax_comment_get_post_comments",
 *          parameters = { "postId" = "expr(service('hashids').encode(object.getId()))" }
 *      ),
 *      embedded = "expr(service('rasen_ninegag.serialization_helper').getCommentsOverview(object))",
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 * @Hateoas\Relation(
 *      "votes",
 *      href = @Hateoas\Route(
 *          "ajax_post_post_vote",
 *          parameters = { "hashId" = "expr(service('hashids').encode(object.getId()))" }
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(object.isDeleted())"
 *      )
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
abstract class Post extends VotableReportable
{
    /**
     * @var integer
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	protected $id;

    /**
     * Whether this post has been approved by an administrator to appear on the stream.
     *
     * All posts need to be approved by an administrator before they appear on the stream, unless the user is
     * an **ActiveUser**.
     * If a user is an **ActiveUser**, then his/her posts would not need any moderation and this flag will automatically be set to true.
     *
     * @var boolean
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="is_approved", type="boolean", nullable=true)
     */
    protected $isApproved;

    /**
     * Featured Post in sidebar
     *
     *
     * @var boolean
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="featured", type="boolean", nullable=true)
     */
    protected $featured;

    /**
     * "Not Safe For Work" flag.
     *
     * If a post has content or material inappropriate for work environment (e.g. nudity or profanity)
     *
     * @var boolean
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="nsfw", type="boolean", nullable=false)
     */
	protected $nsfw;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="created_time", type="datetime", nullable=false)
     */
	protected $createdTime;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="change", field="isApproved", value="1")
     *
     * @Serializer\Exclude
     *
     * @ORM\Column(name="approved_time", type="datetime", nullable=true)
     */
	protected $approvedTime;

    /**
     * Stores this post's comments count
     *
     * @var integer
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="comments_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
     */
	protected $commentsNo;

	/**
	 * Stores the post's total approved comments count.
	 *
	 * @var integer
	 *
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @ORM\Column(name="approved_comments_no", type="integer", nullable=true, options={"unsigned":true, "default":0})
	 */
	protected $approvedCommentsNo;

    /**
     * Stores this post's total views count
     *
     * @var integer
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="views_no", type="bigint", nullable=true, options={"unsigned":true, "default":0})
     */
	protected $viewsNo;

    /**
     * Stores this post's total downloads count
     *
     * @var integer
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="downloads_no", type="bigint", nullable=true, options={"unsigned":true, "default":0})
     */
	protected $downloadsNo;

    /**
     * Stores this post's total shares count
     *
     * @var integer
     *
     * @Serializer\Groups({"default", "own"})
     *
     * @ORM\Column(name="shares_no", type="bigint", nullable=true, options={"unsigned":true, "default":0})
     */
	protected $sharesNo;

    /**
     * @var \Rasen\NineGagBundle\Entity\PostCategory
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\PostCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "own"})
     *
     */
	protected $category;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="create")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     * })
     *
     * @Serializer\Groups({"default", "own"})
     *
     */
	protected $createdBy;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @Gedmo\Blameable(on="change", field="isApproved", value="1")
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approved_by", referencedColumnName="id")
     * })
     *
     * @Serializer\Exclude
     *
     */
	protected $approvedBy;


	/**
	 * @ORM\OneToMany(targetEntity="Comment", mappedBy="post", cascade={"persist", "remove", "merge"}, orphanRemoval=true, fetch="EXTRA_LAZY")
	 *
	 * @Serializer\Exclude
	 *
	 */
	protected $comments;

	/**
	 * @ORM\OneToMany(targetEntity="PostVote", mappedBy="post", cascade={"persist", "remove", "merge"}, orphanRemoval=true, fetch="EXTRA_LAZY")
	 *
	 * @Serializer\Exclude
	 *
	 */
	protected $votes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    protected $embeddedCommentsLimit;

    /**
     * Constructor
     */
    public function __construct()
    {
	    $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
	    $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
	    $this->resetCounters();
    }

    /**
     * Get embeddedCommentsLimit
     *
     * @return integer 
     */
    public function getEmbeddedCommentsLimit()
    {
        return $this->embeddedCommentsLimit ? $this->embeddedCommentsLimit : 3;
    }

    /**
     * Set embeddedCommentsLimit
     * @param $embeddedCommentsLimit
     * @return Post
     */
    public function setEmbeddedCommentsLimit($embeddedCommentsLimit)
    {
        $this->embeddedCommentsLimit = $embeddedCommentsLimit;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @Serializer\VirtualProperty
	 * @Serializer\Groups({"default", "own"})
	 *
	 * @return string
	 */
	public function getType()
	{
		if ($this instanceof PostImage) {
			return 'image';
		} else {
			return 'text';
		}
	}

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     * @return Post
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean 
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * Set nsfw
     *
     * @param boolean $nsfw
     * @return Post
     */
    public function setNsfw($nsfw)
    {
        $this->nsfw = $nsfw;

        return $this;
    }

    /**
     * Get nsfw
     *
     * @return boolean 
     */
    public function getNsfw()
    {
        return $this->nsfw;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return Post
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime 
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set approvedTime
     *
     * @param \DateTime $approvedTime
     * @return Post
     */
    public function setApprovedTime($approvedTime)
    {
        $this->approvedTime = $approvedTime;

        return $this;
    }

    /**
     * Get approvedTime
     *
     * @return \DateTime 
     */
    public function getApprovedTime()
    {
        return $this->approvedTime;
    }


    /**
     * Set featured
     *
     * @param boolean $featured
     * @return Post
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return boolean
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set commentsNo
     *
     * @param integer $commentsNo
     * @return Post
     */
    public function setCommentsNo($commentsNo)
    {
        $this->commentsNo = $commentsNo;

        return $this;
    }

    /**
     * Get commentsNo
     *
     * @return integer 
     */
    public function getCommentsNo()
    {
        return $this->commentsNo;
    }

	/**
	 * Set approvedCommentsNo
	 *
	 * @param integer $approvedCommentsNo
	 * @return Post
	 */
	public function setApprovedCommentsNo($approvedCommentsNo)
	{
		$this->approvedCommentsNo = $approvedCommentsNo;

		return $this;
	}

	/**
	 * Get approvedCommentsNo
	 *
	 * @return integer
	 */
	public function getApprovedCommentsNo()
	{
		return $this->approvedCommentsNo;
	}

	/**
	 * Update commentsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return VotableReportable
	 */
	public function updateCommentsNo($decrease = false)
	{
		if ($this->commentsNo == null) $this->commentsNo = 0;
		$this->commentsNo = $decrease ? $this->commentsNo - 1 : $this->commentsNo + 1;

		return $this;
	}

    /**
     * Set viewsNo
     *
     * @param integer $viewsNo
     * @return Post
     */
    public function setViewsNo($viewsNo)
    {
        $this->viewsNo = $viewsNo;

        return $this;
    }

    /**
     * Get viewsNo
     *
     * @return integer 
     */
    public function getViewsNo()
    {
        return $this->viewsNo;
    }

	/**
	 * Increase viewsNo
	 *
	 * Increase viewsNo one step
	 *
	 * @return VotableReportable
	 */
	public function increaseViewsNo()
	{
		if ($this->viewsNo == null) $this->viewsNo = 0;
		$this->viewsNo = $this->viewsNo + 1;

		return $this;
	}

    /**
     * Set downloadsNo
     *
     * @param integer $downloadsNo
     * @return Post
     */
    public function setDownloadsNo($downloadsNo)
    {
        $this->downloadsNo = $downloadsNo;

        return $this;
    }

    /**
     * Get downloadsNo
     *
     * @return integer
     */
    public function getDownloadsNo()
    {
        return $this->downloadsNo;
    }

	/**
	 * Increase downloadsNo
	 *
	 * Increase downloadsNo one step
	 *
	 * @return VotableReportable
	 */
	public function increaseDownloadsNo()
	{
		if ($this->downloadsNo == null) $this->downloadsNo = 0;
		$this->downloadsNo = $this->downloadsNo + 1;

		return $this;
	}

    /**
     * Set sharesNo
     *
     * @param integer $sharesNo
     * @return Post
     */
    public function setSharesNo($sharesNo)
    {
        $this->sharesNo = $sharesNo;

        return $this;
    }

    /**
     * Get sharesNo
     *
     * @return integer
     */
    public function getSharesNo()
    {
        return $this->sharesNo;
    }

	/**
	 * Increase sharesNo
	 *
	 * Increase sharesNo one step
	 *
	 * @return VotableReportable
	 */
	public function increaseSharesNo()
	{
		if ($this->sharesNo == null) $this->sharesNo = 0;
		$this->sharesNo = $this->sharesNo + 1;

		return $this;
	}

    /**
     * Set category
     *
     * @param \Rasen\NineGagBundle\Entity\PostCategory $category
     * @return Post
     */
    public function setCategory(\Rasen\NineGagBundle\Entity\PostCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Rasen\NineGagBundle\Entity\PostCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $createdBy
     * @return Post
     */
    public function setCreatedBy(\Rasen\NineGagBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set approvedBy
     *
     * @param \Rasen\NineGagBundle\Entity\User $approvedBy
     * @return Post
     */
    public function setApprovedBy(\Rasen\NineGagBundle\Entity\User $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

	/**
	 * Add comment
	 *
	 * @param \Rasen\NineGagBundle\Entity\Comment $comment
	 * @return Post
	 */
	public function addComment(\Rasen\NineGagBundle\Entity\Comment $comment)
	{
		$this->reports[] = $comment;

		return $this;
	}

	/**
	 * Remove comment
	 *
	 * @param \Rasen\NineGagBundle\Entity\Comment $comment
	 */
	public function removeComment(\Rasen\NineGagBundle\Entity\Comment $comment)
	{
		$this->reports->removeElement($comment);
	}

	/**
	 * Get comments
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getComments()
	{
		return $this->comments;
	}

	/**
	 * Set comments
	 *
	 * @param $comments
	 *
	 * @return Post
	 */
	public function setComments($comments)
	{
		$this->comments = $comments;

		return $this;
	}

	/**
	 * Add votes
	 *
	 * @param \Rasen\NineGagBundle\Entity\PostVote $votes
	 * @return Post
	 */
	public function addVote(\Rasen\NineGagBundle\Entity\PostVote $votes)
	{
		$this->votes[] = $votes;

		return $this;
	}

	/**
	 * Remove votes
	 *
	 * @param \Rasen\NineGagBundle\Entity\PostVote $votes
	 */
	public function removeVote(\Rasen\NineGagBundle\Entity\PostVote $votes)
	{
		$this->votes->removeElement($votes);
	}

	/**
	 * Get votes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getVotes()
	{
		return $this->votes;
	}

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Post
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return ($this->deletedAt !== null);
    }

	/**
	 * Reset counters
	 */
	public function resetCounters()
	{
		$this->commentsNo = $this->approvedCommentsNo = $this->viewsNo = $this->downloadsNo = $this->sharesNo = $this->downvotesNo = $this->upvotesNo = $this->reportsNo = 0;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->getId();
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function removeApproveBlame()
	{
		if ($this->isApproved == null) {
			$this->approvedBy = $this->approvedTime = null;
		}
	}


	/**
	 * Update approvedCommentsNo
	 *
	 * Increase or Decrease one step
	 *
	 * @param bool $decrease
	 * @return User
	 */
	public function updateApprovedCommentsNo($decrease = false)
	{
		if ($this->approvedCommentsNo == null) $this->approvedCommentsNo = 0;
		$this->approvedCommentsNo = $decrease ? $this->approvedCommentsNo - 1 : $this->approvedCommentsNo + 1;

		return $this;
	}

	/**
	 * @param $approvedBefore
	 * @param $approvedAfter
	 */
	public function changeApprovedCommentsNoOnUpdate( $approvedBefore, $approvedAfter )
	{
		if ( $approvedBefore && ! $approvedAfter ) {
			$this->updateApprovedCommentsNo( true ); //approvedCommentsNo--
		} elseif ( ! $approvedBefore && $approvedAfter ) {
			$this->updateApprovedCommentsNo( false ); //approvedCommentsNo++
		}

	}
}
