<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/14/2014
 * Time: 12:05 PM
 */

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class UserRepository
 * @package Rasen\NineGagBundle\Entity
 */
class UserRepository extends EntityRepository
{
	public function findAllFollowersQ($user)
	{
		$dql = "SELECT u ".
			"FROM RasenNineGagBundle:User u ".
			"INNER JOIN RasenNineGagBundle:UserFollower uf WITH uf.follower = u ".
			"WHERE uf.user = :user ".
            "ORDER BY u.id ";
		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('user', $user);

		return $q;
	}
	public function findAllFollowers($user)
	{
		$q = $this->findAllFollowersQ($user);
		return $q->getResult();
	}

	public function findAllFollowingsQ($user)
	{
		$dql = "SELECT u ".
			"FROM RasenNineGagBundle:User u ".
			"INNER JOIN RasenNineGagBundle:UserFollower uf WITH uf.user = u ".
			"WHERE uf.follower = :user ".
			"ORDER BY u.id ";
		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('user', $user);

		return $q;
	}

	public function findAllFollowings($user)
	{
        $q = $this->findAllFollowingsQ($user);
		return $q->getResult();
	}

	public function findAllUsersMentioned($user)
	{
		$dql = "SELECT u ".
			"FROM RasenNineGagBundle:User u ".
			"LEFT JOIN RasenNineGagBundle:CommentMention cm WITH cm.user = u ".
			"LEFT JOIN cm.comment c ".
			"WHERE c.createdBy = :user ";
		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('user', $user);

		return $q->getResult();
	}

	public function findAllCommentedOnPost($post)
	{
		$dql = "SELECT u ".
			"FROM RasenNineGagBundle:User u ".
			"LEFT JOIN RasenNineGagBundle:Comment c WITH c.createdBy = u ".
			"WHERE c.post = :post AND c.parent IS NULL ";
		$q = $this->getEntityManager()->createQuery($dql);

		$q->setParameter('post', $post);

		return $q->getResult();
	}

	public function findByQuery($query)
	{
		$qs = preg_split("/(\+)/", $query);

		$rsm = $this->createResultSetMappingBuilder('u');
		$rsm->addRootEntityFromClassMetadata('RasenNineGagBundle:User', 'u');
		//$rsm->addScalarResult('relevance', 'relevance');
		$selectClause = $rsm->generateSelectClause(array(
			'u' => 'u'
		));

		$params = array();
		$sortC0 = " ( LEFT(u.username, ". strlen($qs[0]) .") LIKE :q0r) AS relevance";
		$sql = "SELECT DISTINCT " . $selectClause . ',' . $sortC0 ." FROM users u ";
		$c = count($qs);
		for($i = 0; $i < $c; ++$i) {
			$qry = trim($qs[$i]);
			$placeholder = ":q". $i;
			$params[$placeholder] = $qry;

			$sortC = " ( LEFT(u.username, ". strlen($qry) .") LIKE ".$placeholder."r) AS relevance";

			if ($i>0) {
				$sql .= " INNER JOIN (";
				$sql .= "SELECT DISTINCT " . $selectClause . ", ".$sortC." FROM users u " .
					" WHERE u.username = ". $placeholder .
					" OR u.first_name like ". $placeholder .
					" OR u.last_name like ". $placeholder .
					" ORDER BY relevance DESC".
					") t" . $i . " ON (u.id = t" . $i . ".id0)"
				;
			}
			if ($i==$c-1) {
				$sql .=  " WHERE u.username like :q". 0 .
					" OR u.first_name like :q". 0 .
					" OR u.last_name like :q". 0 .
					" ORDER BY relevance DESC"
				;
			}
		}
		//die(var_dump($sql));
		$em = $this->getEntityManager();
		$foundUsersQ = $em->createNativeQuery($sql, $rsm);

		foreach ($params as $key=>$qry) {
			$foundUsersQ->setParameter($key, '%'. $qry .'%');
			$foundUsersQ->setParameter($key.'r', $qry .'%');
		}

		return $foundUsersQ->getResult();
	}

    public function findVotedOnPostQ($post, $vote)
    {
        $q = $this->getEntityManager()->createQueryBuilder('u');
        $q->select('u')
            ->add('from', 'RasenNineGagBundle:User u')
            ->innerJoin('RasenNineGagBundle:PostVote', 'pv','WITH', $q->expr()->eq('u', 'pv.votedBy'))
            ->where('pv.post = :post')
            ->andWhere('pv.vote = :vote')
            ->orderBy('pv.votedTime', 'DESC')
            ->setParameter(':post', $post)
            ->setParameter(':vote', $vote)
        ;
        return $q->getQuery();
    }

    public function findVotedOnPost($post, $vote)
    {
        $q = $this->findVotedOnPostQ($post, $vote);
        return $q->getResult();
    }

    public function findVotedOnCommentQ($comment, $vote)
    {
        $q = $this->getEntityManager()->createQueryBuilder('u');
        $q->select('u')
            ->add('from', 'RasenNineGagBundle:User u')
            ->innerJoin('RasenNineGagBundle:CommentVote', 'cv','WITH', $q->expr()->eq('u', 'cv.votedBy'))
            ->where('cv.comment = :comment')
            ->andWhere('cv.vote = :vote')
            ->orderBy('cv.votedTime', 'DESC')
            ->setParameter(':comment', $comment)
            ->setParameter(':vote', $vote)
        ;
        return $q->getQuery();
    }

    public function findVotedOnComment($comment, $vote)
    {
        $q = $this->findVotedOnCommentQ($comment, $vote);
        return $q->getResult();
    }
} 