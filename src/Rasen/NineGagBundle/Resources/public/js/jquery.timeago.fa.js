﻿jQuery.timeago.settings.strings = {
    prefixAgo: null,
    prefixFromNow: null,
    suffixAgo: "پیش",
    suffixFromNow: "از حال",
    seconds: "چند ثانیه",
    minute: "یک دقیقه",
    minutes: "%d دقیقه",
    hour: "یک ساعت",
    hours: "%d ساعت",
    day: "یک روز",
    days: "%d روز",
    month: "یک ماه",
    months: "%d ماه",
    year: "یک سال",
    years: "%d سال",
    wordSeparator: " "
};