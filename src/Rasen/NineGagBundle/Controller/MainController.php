<?php

namespace Rasen\NineGagBundle\Controller;

use Rasen\NineGagBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class MainController
 * @package Rasen\NineGagBundle\Controller
 */
class MainController extends Controller
{
    public function hotAction()
    {
        return $this->render('RasenNineGagBundle:Post:hot.html.twig');
    }

    public function trendingAction()
    {
        return $this->render('RasenNineGagBundle:Post:trending.html.twig');
    }

    public function freshAction()
    {
        return $this->render('RasenNineGagBundle:Post:fresh.html.twig');
    }

	/**
	 * @param Request $request
	 *
	 * @Route("/")
	 * @return array
	 */
	public function homeAction(Request $request)
	{
		//if (!$this->getUser()) {
			return $this->forward('RasenNineGagBundle:Main:fresh');
		//}
		//$posts = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post")->findAllFollowing($this->getUser());
		//return $this->render('RasenNineGagBundle:Post:home.html.twig', array(
		//	'posts' => $posts
		//));
	}

	/**
	 * @param Request $request
	 *
	 * @Route("/nsfw")
	 * @return array
	 */
	public function nsfwAction(Request $request)
	{

		//return $this->forward('RasenNineGagBundle:Main:fresh');
		//$posts = $this->getDoctrine()->getRepository("RasenNineGagBundle:Post")->findAllNsfw($this->getUser());
		return $this->render('RasenNineGagBundle:Post:nsfw.html.twig', array(
			//'posts' => $posts
		));
	}

    /**
     * @Security("has_role('ROLE_USER')")
     *
     * @Route("/settings")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function settingAction(Request $request)
    {
        $user=$this->getUser();

        /** @var $um \FOS\UserBundle\Model\UserManagerInterface */
        $um = $this->container->get('fos_user.user_manager');

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $um->updateUser($user, true);
                return $this->render(
                    'RasenNineGagBundle:User:setting.html.twig',
                    array('form' => $form->createView(), 'success' => 'forms.user.edit.successfully' , 'username'=>$user->getUsername())

                );
            }
        }

        return $this->render('RasenNineGagBundle:User:setting.html.twig', array('form' => $form->createView() , 'username'=>$user->getUsername()));
    }
}
