<?php

namespace Rasen\NineGagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Settings
 *
 * Stores the application settings for each user
 *
 * @ORM\Table(name="settings", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_name_UNIQUE", columns={"user_id", "name"})}, indexes={@ORM\Index(name="settings_user_id_idx", columns={"user_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"user", "name"},
 *     errorPath="name",
 *     message="settings.user_name.unique"
 * )
 *
 * @author Nima Karimi <nima.k68@gmail.com>
 */
class Settings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank(message = "settings.name.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "settings.name.length_min",
     *      maxMessage = "settings.name.length_max"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank(message = "settings.value.not_blank")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "settings.value.length_min",
     *      maxMessage = "settings.value.length_max"
     * )
     */
    private $value;

    /**
     * @var \Rasen\NineGagBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Rasen\NineGagBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Settings
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Settings
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user
     *
     * @param \Rasen\NineGagBundle\Entity\User $user
     * @return Settings
     */
    public function setUser(\Rasen\NineGagBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rasen\NineGagBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
