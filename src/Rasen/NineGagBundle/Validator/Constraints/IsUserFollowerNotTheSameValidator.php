<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/10/2014
 * Time: 1:25 PM
 */

namespace Rasen\NineGagBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation as DI;
use Rasen\NineGagBundle\Entity\Notification;
/**
 * Class IsUserFollowerNotTheSameValidator
 *
 * Checks if a user and it's follower are not the same.
 *
 * @package Rasen\NineGagBundle\Validator\Constraints
 *
 * @DI\Validator("is_user_follower_not_the_same")
 */
class IsUserFollowerNotTheSameValidator extends ConstraintValidator
{
	/**
	 * @param \Rasen\NineGagBundle\Entity\UserFollower $userFollower
	 * @param Constraint $constraint
	 */
	public function validate($userFollower, Constraint $constraint)
	{
		if ($userFollower->getUser() === $userFollower->getFollower()) {
			$this->context->addViolationAt(
				'user',
				$constraint->message,
				array(),
				null
			);
		}
	}
}