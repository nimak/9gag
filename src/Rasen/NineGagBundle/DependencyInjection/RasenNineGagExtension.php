<?php

namespace Rasen\NineGagBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class RasenNineGagExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

	    if (!isset($config['post_cache_dir'])) {
		    throw new \InvalidArgumentException('The "post_cache_dir" option must be set');
	    }
	    $container->setParameter('rasen_ninegag.post_cache_dir', $config['post_cache_dir']);

	    if (!isset($config['post_cache_url'])) {
		    throw new \InvalidArgumentException('The "post_cache_url" option must be set');
	    }
	    $container->setParameter('rasen_ninegag.post_cache_url', $config['post_cache_url']);
    }

	public function getAlias()
	{
		return 'rasen_nine_gag';
	}
}
