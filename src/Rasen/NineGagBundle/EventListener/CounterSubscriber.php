<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/7/2014
 * Time: 10:23 PM
 */

namespace Rasen\NineGagBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Rasen\NineGagBundle\Entity\Comment;
use Rasen\NineGagBundle\Entity\CommentVote;
use Rasen\NineGagBundle\Entity\CommentReport;
use Rasen\NineGagBundle\Entity\Post;
use Rasen\NineGagBundle\Entity\PostVote;
use Rasen\NineGagBundle\Entity\PostReport;
use Rasen\NineGagBundle\Entity\User;
use Rasen\NineGagBundle\Entity\UserFollower;
use Rasen\NineGagBundle\Entity\UserPoint;
use Rasen\NineGagBundle\Entity\UserReport;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CounterSubscriber
 *
 * This class handles updating counter fields in entities such as followersNo, upvotesNo, etc.
 *
 * @DI\Service("event_listener.counter_subscriber")
 * @DI\Tag("doctrine.event_subscriber", attributes = {"connection" = "default"})
 *
 * @package Rasen\NineGagBundle\EventListener
 */
class CounterSubscriber implements EventSubscriber
{
	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return array(
			'prePersist',
			'preRemove',
			'onFlush'
		);
	}

	function flatten(array $array) {
		$return = array();
		array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
		return $return;
	}

	/**
	 * @param OnFlushEventArgs $args
	 */
	public function onFlush(OnFlushEventArgs $args)
	{
		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach ($uow->getScheduledEntityUpdates() as $entity) {
			$changeSet = $uow->getEntityChangeSet($entity);

			$updatedEntities = array();

			if ($entity instanceof Post)
			{
				if ( array_key_exists('isApproved', $changeSet) ) {
					$valueBefore = $changeSet['isApproved'][0];
					$valueAfter = $changeSet['isApproved'][1];
					$entity->getCreatedBy()->changeApprovedPostsNoOnUpdate($valueBefore, $valueAfter);
					$updatedEntities[] = $entity->getCreatedBy();
				}
				if ( array_key_exists('deletedAt', $changeSet) ) {
					$valueBefore = $changeSet['deletedAt'][0];
					$valueAfter = $changeSet['deletedAt'][1];
					if ( $valueBefore && ! $valueAfter ) {
						$updatedEntities[] = $this->count($entity, false); //PostNo++
					} elseif ( ! $valueBefore && $valueAfter ) {
						//$this->count($entity, true); //PostNo--
					}
					$uow->computeChangeSets();
				}
			} elseif ($entity instanceof PostVote)
			{
				if ( array_key_exists('post', $changeSet) || array_key_exists('vote', $changeSet) || array_key_exists('votedBy', $changeSet) ) {
					$objBefore = $objAfter = $entity->getPost();
					$userBefore = $userAfter = $entity->getVotedBy();
					$valueBefore = $valueAfter = $entity->getVote();
					if (array_key_exists('vote', $changeSet)) {
						$valueBefore = $changeSet['vote'][0];
						$valueAfter = $changeSet['vote'][1];
					}
					if (array_key_exists('post', $changeSet)) {
						$objBefore = $changeSet['post'][0];
						$objAfter = $changeSet['post'][1];
					}
					if (array_key_exists('votedBy', $changeSet)) {
						$userBefore = $changeSet['votedBy'][0];
						$userAfter = $changeSet['votedBy'][1];
					}
					$entity->getPost()->changeVotesNoOnUpdate($objBefore, $objAfter, $valueBefore, $valueAfter);
					$entity->getVotedBy()->changeVotesNoOnUpdate($userBefore, $userAfter, $valueBefore, $valueAfter);
					$updatedEntities[] =$entity->getPost();
					$updatedEntities[] =$entity->getVotedBy();
				}
			} elseif ($entity instanceof PostReport)
			{
				if (array_key_exists('post', $changeSet)) {
					$objBefore = $changeSet['post'][0];
					$objAfter = $changeSet['post'][1];
					$entity->getPost()->changeReportsNoOnUpdate($objBefore, $objAfter);
					$updatedEntities[] =$entity->getPost();
				}
			} elseif ($entity instanceof Comment)
			{
				if ( array_key_exists('approved', $changeSet) ) {
					$valueBefore = $changeSet['approved'][0];
					$valueAfter = $changeSet['approved'][1];
					$entity->getCreatedBy()->changeApprovedCommentsNoOnUpdate($valueBefore, $valueAfter);
					$entity->getPost()->changeApprovedCommentsNoOnUpdate($valueBefore, $valueAfter);
					$updatedEntities[] =$entity->getCreatedBy();
					$updatedEntities[] = $entity->getPost();
				}
				if ( array_key_exists('deletedAt', $changeSet) ) {
					$valueBefore = $changeSet['deletedAt'][0];
					$valueAfter = $changeSet['deletedAt'][1];
					if ( $valueBefore && ! $valueAfter ) {
                        $entity->getCreatedBy()->updateApprovedCommentsNo(false);
                        $entity->getPost()->updateApprovedCommentsNo(false);
                        $updatedEntities[] =$entity->getCreatedBy();
                        $updatedEntities[] = $entity->getPost();
					} elseif ( ! $valueBefore && $valueAfter ) {
						//$this->count($entity, true); //commentsNo-- ||| taken care of in onPreRemove section
					}
				}
			} elseif ($entity instanceof CommentVote)
			{
				if ( array_key_exists('comment', $changeSet) || array_key_exists('vote', $changeSet) ) {
					$objBefore = $objAfter = $entity->getComment();
					$valueBefore = $valueAfter = $entity->getVote();
					if (array_key_exists('vote', $changeSet)) {
						$valueBefore = $changeSet['vote'][0];
						$valueAfter = $changeSet['vote'][1];
					}
					if (array_key_exists('comment', $changeSet)) {
						$objBefore = $changeSet['comment'][0];
						$objAfter = $changeSet['comment'][1];
					}
					$entity->getComment()->changeVotesNoOnUpdate($objBefore, $objAfter, $valueBefore, $valueAfter);
					$updatedEntities[] = $entity->getComment();
				}
			} elseif ($entity instanceof CommentReport)
			{
				if (array_key_exists('comment', $changeSet)) {
					$objBefore = $changeSet['comment'][0];
					$objAfter = $changeSet['comment'][1];
					$entity->getComment()->changeReportsNoOnUpdate($objBefore, $objAfter);
					$updatedEntities[] = $entity->getComment();
				}
			} elseif ($entity instanceof UserReport)
			{
				if (array_key_exists('user', $changeSet)) {
					$objBefore = $changeSet['user'][0];
					$objAfter = $changeSet['user'][1];
					$entity->getUser()->changeReportsNoOnUpdate($objBefore, $objAfter);
					$updatedEntities[] = $entity->getUser();
				}
			} elseif ($entity instanceof UserPoint)
			{
				if ( array_key_exists('user', $changeSet) || array_key_exists('point', $changeSet) ) {
					$objBefore = $objAfter = $entity->getUser();
					$valueBefore = $valueAfter = $entity->getPoint();
					if (array_key_exists('point', $changeSet)) {
						$valueBefore = $changeSet['point'][0];
						$valueAfter = $changeSet['point'][1];
					}
					if (array_key_exists('user', $changeSet)) {
						$objBefore = $changeSet['user'][0];
						$objAfter = $changeSet['user'][1];
					}
					$entity->getUser()->changePointsNoOnUpdate($objBefore, $objAfter, $valueBefore, $valueAfter);
					$updatedEntities[] = $entity->getUser();
				}
			} elseif ($entity instanceof UserFollower)
			{
				if ( array_key_exists('user', $changeSet) || array_key_exists('follower', $changeSet) ) {
					$objBefore = $objAfter = $entity->getUser();
					$followerBefore = $followerAfter = $entity->getFollower();;
					if (array_key_exists('follower', $changeSet)) {
						$followerBefore = $changeSet['follower'][0];
						$followerAfter = $changeSet['follower'][1];
					}
					if (array_key_exists('user', $changeSet)) {
						$objBefore = $changeSet['user'][0];
						$objAfter = $changeSet['user'][1];
					}
					$entity->getUser()->changeFollowersNoOnUpdate($objBefore, $objAfter, $followerBefore, $followerAfter);
					$updatedEntities[] = $entity->getUser();
				}
			} elseif ($entity instanceof User)
			{
				if ( array_key_exists('deletedAt', $changeSet) ) {
					$valueBefore = $changeSet['deletedAt'][0];
					$valueAfter = $changeSet['deletedAt'][1];
					if ( $valueBefore && ! $valueAfter ) {
						//$this->count($entity, false); //followersNo++/followingsNo++
					} elseif ( ! $valueBefore && $valueAfter ) {
						//$this->count($entity, true); //followersNo--/followingsNo--
					}
					//$uow->computeChangeSets();
				}
			}

			$updatedEntities = $this->flatten($updatedEntities);
			foreach ($updatedEntities as $updatedEntity) {
				/*echo "------------>>";
                var_dump(get_class($updatedEntity));
                echo "<<-------------";*/
				$classMetadata = $em->getClassMetadata(get_class($updatedEntity));
				$uow->recomputeSingleEntityChangeSet($classMetadata, $updatedEntity);
			}
		}


		/*foreach ($uow->getScheduledEntityInsertions() as $entity) {

		}
		foreach ($uow->getScheduledEntityDeletions() as $entity) {

		}

		foreach ($uow->getScheduledCollectionDeletions() as $col) {

		}

		foreach ($uow->getScheduledCollectionUpdates() as $col) {

		}*/
	}

	/**
	 * {@inheritdoc}
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();
		$this->count($entity);
	}

	/**
	 * {@inheritdoc}
	 */
	public function preRemove(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();
		if ($entity instanceof Comment || $entity instanceof Post || $entity instanceof User) {
			if ($entity->isDeleted()) return false;
		}
		$this->count($entity, true);
	}

	/**
	 * Count Votes
	 *
	 * @param object $entity
	 * @param bool $decrease
	 * @return array
	 */
	public function countVote($entity, $decrease = false)
	{
		$updatedEntities = array();

		if ($entity instanceof PostVote)
		{
			/**
			 * $post Post
			 */
			$post = $entity->getPost();
			$post->updateVotesNo($entity->getVote(), $decrease);
			$updatedEntities[] = $post;

			/**
			 * $user User
			 */
			$user = $entity->getVotedBy();
			$user->updateVotesNo($entity->getVote(), $decrease);
			$updatedEntities[] = $user;
		}
		elseif ($entity instanceof CommentVote)
		{
			/**
			 * $comment Comment
			 */
			$comment = $entity->getComment();
			$comment->updateVotesNo($entity->getVote(), $decrease);
			$updatedEntities[] = $comment;
		}
		$updatedEntities = $this->flatten($updatedEntities);
		return $updatedEntities;
	}

	/**
	 * Count Approved
	 *
	 * @param $entity
	 * @param bool $decrease
	 * @return array
	 */
	public function countApproved($entity, $decrease)
	{
		$updatedEntities = array();

		if ($entity instanceof Post)
		{
			$user = $entity->getCreatedBy();
			$user->updateApprovedPostsNo($decrease);
			$updatedEntities[] = $user;

		}
		elseif ($entity instanceof Comment)
		{

			$user = $entity->getCreatedBy();
			$user->updateApprovedCommentsNo($decrease);
			$updatedEntities[] = $user;

			$post = $entity->getPost();
			$post->updateApprovedCommentsNo($decrease);
			$updatedEntities[] = $post;

		}
		$updatedEntities = $this->flatten($updatedEntities);
		return $updatedEntities;
	}

	/**
	 * Count User Point
	 *
	 * @param $entity
	 * @param bool $decrease
	 * @return array
	 */
	public function countPoint($entity, $decrease = false)
	{
		$updatedEntities = array();

		if ( $entity instanceof UserPoint ) {
			/**
			 * $user User
			 */
			$user = $entity->getUser();

			$point = $entity->getPoint();

			$user->updatePointsNo( $point, $decrease );
			$updatedEntities[] = $user;
		}
		$updatedEntities = $this->flatten($updatedEntities);
		return $updatedEntities;
	}

	/**
	 * Increase/Decrease # of counters in entities.
	 *
	 * @param $entity
	 * @param bool $decrease Whether to decrease the counter or increase it.
	 * @return array
	 */
	public function count($entity, $decrease = false)
	{
		$updatedEntities = array();

		$updatedEntities[] = $this->countVote($entity, $decrease);

		$updatedEntities[] = $this->countPoint($entity, $decrease);

		if ($entity instanceof Comment)
		{
			/**
			 * $post Post
			 */
			$post = $entity->getPost();
			//if ($decrease OR $entity->getId() == null) {
				$post->updateCommentsNo($decrease);
			//}
			$updatedEntities[] = $post;

			$parent = $entity->getParent();
			if ($parent && $parent instanceof Comment) {
				$parent->updateRepliesNo($decrease);
				$updatedEntities[] = $parent;
			}

			/**
			 * $user User
			 */
			$user = $entity->getCreatedBy();
			if ($decrease OR $entity->getId() == null) {
				$user->updateCommentsNo($decrease);
				$updatedEntities[] = $user;
			}
			if ( $entity->getApproved() ) {
                $post->updateApprovedCommentsNo( $decrease );

				$user->updateApprovedCommentsNo( $decrease );
				$updatedEntities[] = $user;
			}
		}
		elseif ($entity instanceof UserFollower)
		{
			if ($decrease OR $entity->getId() == null) {

				/**
				 * $user User
				 */
				$user = $entity->getUser();
				$user->updateFollowersNo($decrease);
				$updatedEntities[] = $user;

				/**
				 * $follower User
				 */
				$follower = $entity->getFollower();
				$follower->updateFollowingNo($decrease);
				$updatedEntities[] = $follower;
			}
		}
		elseif ($entity instanceof Post)
		{
			/**
			 * $user User
			 */
			$user = $entity->getCreatedBy();
			$user->updatePostsNo($decrease);
			if ($entity->getIsApproved()) {
				$user->updateApprovedPostsNo($decrease);
			}
			$updatedEntities[] = $user;
		}
		elseif ($entity instanceof CommentReport)
		{
			/**
			 * $comment Comment
			 */
			$comment = $entity->getComment();
			$comment->updateReportsNo($decrease);
			$updatedEntities[] = $comment;
		}
		elseif ($entity instanceof PostReport)
		{
			/**
			 * $post Post
			 */
			$post = $entity->getPost();
			$post->updateReportsNo($decrease);
			$updatedEntities[] = $post;
		}
		elseif ($entity instanceof UserReport)
		{
			/**
			 * $user User
			 */
			$user = $entity->getUser();
			$user->updateReportsNo($decrease);
			$updatedEntities[] = $user;
		}

		$updatedEntities = $this->flatten($updatedEntities);
		return $updatedEntities;
	}
} 