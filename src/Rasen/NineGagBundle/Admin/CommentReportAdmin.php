<?php
/**
 * Created by PhpStorm.
 * User: Nima
 * Date: 10/24/2014
 * Time: 12:29 PM
 */

namespace Rasen\NineGagBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
/**
 * Class CommentReportAdmin
 *
 * @package Rasen\NineGagBundle\Admin
 */
class CommentReportAdmin extends Admin
{
	protected $parentAssociationMapping = 'comment';

	/**
	 * {@inheritdoc}
	 */
	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'reportedTime'
	);

	/**
	 * {@inheritdoc}
	 */
	public function getBaseRoutePattern()
	{
		if (!$this->baseRoutePattern) {
			$baseRoute = 'comment-report';
			if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
				$this->baseRoutePattern = sprintf('%s/{id}/%s',
					$this->getParent()->getBaseRoutePattern(),
					$baseRoute
				);
			} else {
				$this->baseRoutePattern = $baseRoute;
			}
		}
		return $this->baseRoutePattern;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->with('comment_report',
				array(
					'class' => 'col-md-8 col-md-offset-2'
				))
			->add('comment', 'sonata_type_model_list')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
            ->add('comment.id')
			->add('reporter.id')
			->add('reportedTime')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('id')
			->add('comment')
			->add('reporter')
			->add('reportedTime')
		;
	}
} 