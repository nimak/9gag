function get_cdn_host(fileName)
    local servers = {'i1%.statics','i2%.statics','i3%.statics'}
    local fileNameNoExt = string.gsub(fileName, '%_', '')
    local fileNameNoExt = string.gsub(fileNameNoExt, '%-', '')
    local fileNameNoExt = string.gsub(fileNameNoExt, '%.', '')
    local charPos = (math.ceil(string.len(fileNameNoExt)/2))+1
    local middleChar = string.sub(fileNameNoExt, charPos, charPos)
    local charCode = 0.1
    if tonumber(middleChar, 16) ~= nil then
        charCode = tonumber(middleChar, 16) / 15
    else
        charCode = string.byte(middleChar) / 128
    end
    local i = math.ceil(charCode * 3)
    i = math.min(i, 3)
    i = math.max(i, 1)
    return servers[i]
end
local host = ngx.var.host
if string.find(host, "i[0-9]%.statics") then
    local fileName = string.match(ngx.var.request_filename, '([^/]+)%..+$')
    local cdnHost = get_cdn_host(fileName)
    if string.find(host, cdnHost) == nil then
        ngx.exec("@404")
    end
end